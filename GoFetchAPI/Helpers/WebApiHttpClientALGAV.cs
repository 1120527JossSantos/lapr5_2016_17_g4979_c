﻿using GoFetchAPI.DTO;
using GoFetchAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace GoFetchAPI.Helpers
{
    public class WebApiHttpClientALGAV
    {
        public const string WebApiBaseAddress = "http://10.8.11.53:5001/";

        public static HttpClient GetClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(WebApiBaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
        public static async Task<Itinerary> sendToAlgavForRegularItinerary(ALGAVItineraryRequest dto, DateTime startDate)
        {
            var client = GetClient();
          
            Itinerary itinerary=null;
            string dataJSON = JsonConvert.SerializeObject(dto);
            HttpContent content = new StringContent(dataJSON, System.Text.Encoding.UTF8, "application/json");
            var response = await client.PostAsync(WebApiBaseAddress + "registerCaminho", content);
            DateTime start = new DateTime(startDate.Ticks);
            string responseJson2 = await response.Content.ReadAsStringAsync();
            if (await response.Content.ReadAsStringAsync() != "False\r\n")
            {
                itinerary = new Itinerary();
                itinerary.StartTime = start;
                itinerary.VisitList = new List<Visit>();
                foreach (int poivisit in dto.pois)
                {
                    itinerary.VisitList.Add(new Visit { POIID = poivisit });
                }
                
                string responseJson = await response.Content.ReadAsStringAsync();
                transformToItinerary(responseJson,itinerary,dto.transporte,start);
            }
            return itinerary;
        }
        public static async Task<Boolean> sendToAlgavToRegisterPOI()
        {

            return true;
        }

        public static async Task<Itinerary> sendToAlgavForTimedItinerary(ALGAVItineraryRequest dto, DateTime startDate)
        {
            var client = GetClient();
            Itinerary itinerary = null;
            ALGAVItineraryRequestWithDuration dto2 = (ALGAVItineraryRequestWithDuration)dto;
            string dataJSON = JsonConvert.SerializeObject(dto2);
            HttpContent content = new StringContent(dataJSON, System.Text.Encoding.UTF8, "application/json");
            var response = await client.PostAsync(WebApiBaseAddress + "register_caminho_com_horario", content);
            DateTime start = new DateTime(startDate.Ticks);
            if (await response.Content.ReadAsStringAsync() != "False\r\n")
            {
                itinerary = new Itinerary();
                itinerary.StartTime = start;
                itinerary.VisitList = new List<Visit>();
                foreach (int poivisit in dto.pois)
                {
                    itinerary.VisitList.Add(new Visit { POIID = poivisit });
                }

                string responseJson = await response.Content.ReadAsStringAsync();
                transformToItinerary(responseJson, itinerary, dto.transporte, start);
            }
            return itinerary;
        }

       public static void transformToItinerary(string response, Itinerary itinerary, int transport,DateTime start)
        {
            
            //string responseString = response.Replace("[\r\n", "");
            string responseString = response.Replace(" ", "");
            responseString = responseString.Replace("\r\n", "");
            responseString = responseString.Replace("],", "");
            responseString = responseString.Replace("]", "");
            string[] split = responseString.Split('[');
            string[] visitaPOI = split[2].Split(',');
            string[] horasDeChegada = split[split.Length - 1].Split(',');
            foreach(Visit vis in itinerary.VisitList)
            {
                for(int i =0;i<visitaPOI.Length;i++)
                {
                    if (Int32.Parse(visitaPOI[i]) == vis.POIID)
                    {
                        int block=Int32.Parse(horasDeChegada[i]);
                        DateTime visitTime = new DateTime(1, 1, 1);
                        visitTime = visitTime.AddHours(8.0);
                        for (int j = 0; j < block; j++) visitTime = visitTime.AddMinutes(15.0);
                        vis.StartTime = visitTime;
                    }
                }
            }
           
            string[] dispo = split[3].Split(',');
            int startBlock = Int32.Parse(dispo[0]);
            int endBlock = Int32.Parse(dispo[dispo.Length - 1]);
            
            start = start.AddHours(8.0);
            for (int i = 0; i < startBlock; i++)
                start = start.AddMinutes(15.0);
            string[] contentString;
            itinerary.StartTime = start;

            itinerary.travels = new List<POITravel>();

            if(transport == 3)
            {
                for (int i = 5; i < split.Length - 1; i++)
                {
                    contentString = split[i].Split(',');
                    itinerary.travels.Add(new POITravel { POI1ID = Int32.Parse(contentString[0]), POI2ID = Int32.Parse(contentString[1]), typeOfTravel = Int32.Parse(contentString[2]) });
                }
            } else
            {
                for(int j = 0; j < visitaPOI.Length - 1; j++)
                {
                    itinerary.travels.Add(new POITravel { POI1ID = Int32.Parse(visitaPOI[j]), POI2ID = Int32.Parse(visitaPOI[j + 1]), typeOfTravel = transport});
                }
            }
            
        }

    }
}