﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoFetchAPI.Models;

namespace GoFetchAPI.ViewModels
{
    public class UserAlertsViewModel
    {
        public ApplicationUser User { get; set; }

        public int AlertID { get; set; }

        public List<Alert> Alerts { get; set; }
    }
}