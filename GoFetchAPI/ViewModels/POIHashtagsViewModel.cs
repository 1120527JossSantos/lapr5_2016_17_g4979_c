﻿using GoFetchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoFetchAPI.ViewModels
{
    public class POIHashtagsViewModel
    {
        public POI POI { get; set; }

        public string Label { get; set; }

        public List<Hashtag> Hashtags { get; set; }
    }
}