﻿using GoFetchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoFetchAPI.ViewModels
{
    // ViewModel to be used during the POI acception process
    public class POIAcceptionViewModel
    {
        public POI POI { get; set; }
        
        public List<POIConnection> Connections { get; set; }

        public List<Schedule> WorkSchedule { get; set; }
        public List<Restriction> RestrictionList { get; set; }

        public POIAcceptionViewModel()
        {
            POI = new POI();
            this.Connections = new List<POIConnection>();
            this.WorkSchedule = new List<Schedule>();

            // create a new position for each day
            for(int i = 0; i < 7; i++)
            {
                Schedule sch = new Schedule();
                sch.Day = (i + 1);
                this.WorkSchedule.Add(sch);
            }
        }
    }
}