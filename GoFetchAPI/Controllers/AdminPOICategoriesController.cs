﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoFetchAPI.DAL.Repositories;
using GoFetchAPI.DAL.Repository_Interfaces;
using GoFetchAPI.Models;

namespace GoFetchAPI.Controllers
{
    public class AdminPOICategoriesController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IPOICategoryRepository poiCategoryRepository = new POICategoryRepository();

        // GET: AdminPOICategories
        public async Task<ActionResult> Index()
        {
            return View(poiCategoryRepository.GetData().ToList());
        }

        // GET: AdminPOICategories/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //POICategory pOICategory = await db.POICategories.FindAsync(id);
            POICategory pOICategory = poiCategoryRepository.GetData((int) id);
            if (pOICategory == null)
            {
                return HttpNotFound();
            }
            return View(pOICategory);
        }

        // GET: AdminPOICategories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminPOICategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Description")] POICategory pOICategory)
        {
            if (ModelState.IsValid)
            {
                //db.POICategories.Add(pOICategory);
                //await db.SaveChangesAsync();
                poiCategoryRepository.Create(pOICategory);
                return RedirectToAction("Index");
            }

            return View(pOICategory);
        }

        // GET: AdminPOICategories/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //POICategory pOICategory = await db.POICategories.FindAsync(id);
            POICategory pOICategory = poiCategoryRepository.GetData((int) id);
            if (pOICategory == null)
            {
                return HttpNotFound();
            }
            return View(pOICategory);
        }

        // POST: AdminPOICategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Description")] POICategory pOICategory)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(pOICategory).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                poiCategoryRepository.UpdatePOICategory(pOICategory);
                return RedirectToAction("Index");
            }
            return View(pOICategory);
        }

        // GET: AdminPOICategories/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //POICategory pOICategory = await db.POICategories.FindAsync(id);
            POICategory pOICategory = poiCategoryRepository.GetData((int) id);
            if (pOICategory == null)
            {
                return HttpNotFound();
            }
            return View(pOICategory);
        }

        // POST: AdminPOICategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            //POICategory pOICategory = await db.POICategories.FindAsync(id);
            //db.POICategories.Remove(pOICategory);
            //await db.SaveChangesAsync();
            POICategory poiCategory = poiCategoryRepository.GetData(id);
            poiCategoryRepository.DeletePOICategory(poiCategory);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
