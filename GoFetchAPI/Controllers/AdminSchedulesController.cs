﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.Models;

namespace GoFetchAPI.Controllers
{
    public class AdminSchedulesController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IScheduleRepository scheduleRepository = new ScheduleRepository();

        // GET: AdminSchedules
        public async Task<ActionResult> Index()
        {
            var schedules = scheduleRepository.GetData();
            return View(schedules.ToList());
        }

        // GET: AdminSchedules/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Schedule schedule = await db.Schedules.FindAsync(id);
            Schedule schedule = scheduleRepository.GetData((int) id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            return View(schedule);
        }

        // GET: AdminSchedules/Create
        public ActionResult Create()
        {
            //ViewBag.POIID = new SelectList(db.POIs, "ID", "Name");
            ViewBag.POIID = new SelectList(scheduleRepository.GetData(), "ID", "Name");
            return View();
        }

        // POST: AdminSchedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Day,POIID,Open,Close")] Schedule schedule)
        {
            if (ModelState.IsValid)
            {
                //db.Schedules.Add(schedule);
                //await db.SaveChangesAsync();
                scheduleRepository.Create(schedule);
                return RedirectToAction("Index");
            }

            //ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", schedule.POIID);
            ViewBag.POIID = new SelectList(scheduleRepository.GetData(), "ID", "Name", schedule.POIID);
            return View(schedule);
        }

        // GET: AdminSchedules/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Schedule schedule = await db.Schedules.FindAsync(id);
            Schedule schedule = scheduleRepository.GetData((int) id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            //ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", schedule.POIID);
            ViewBag.POIID = new SelectList(scheduleRepository.GetData(), "ID", "Name", schedule.POIID);
            return View(schedule);
        }

        // POST: AdminSchedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Day,POIID,Open,Close")] Schedule schedule)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(schedule).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                scheduleRepository.UpdateSchedule(schedule);
                return RedirectToAction("Index");
            }
            //ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", schedule.POIID);
            ViewBag.POIID = new SelectList(scheduleRepository.GetData(), "ID", "Name", schedule.POIID);
            return View(schedule);
        }

        // GET: AdminSchedules/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Schedule schedule = await db.Schedules.FindAsync(id);
            Schedule schedule = scheduleRepository.GetData((int) id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            return View(schedule);
        }

        // POST: AdminSchedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            //Schedule schedule = await db.Schedules.FindAsync(id);
            //db.Schedules.Remove(schedule);
            //await db.SaveChangesAsync();
            Schedule schedule = scheduleRepository.GetData(id);
            scheduleRepository.DeleteSchedule(schedule);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
