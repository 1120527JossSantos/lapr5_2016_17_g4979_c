﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.Models;

namespace GoFetchAPI.Controllers
{
    public class AdminProfilesController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IProfileRepository profileRepository = new ProfileRepository();

        // GET: AdminProfiles
        public async Task<ActionResult> Index()
        {
            return View(profileRepository.GetData().ToList());
        }

        // GET: AdminProfiles/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Profile profile = await db.Profiles.FindAsync(id);
            Profile profile = profileRepository.GetData((int) id);
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }

        // GET: AdminProfiles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminProfiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name,BirthDate")] Profile profile)
        {
            if (ModelState.IsValid)
            {
                //db.Profiles.Add(profile);
                //await db.SaveChangesAsync();
                profileRepository.Create(profile);
                return RedirectToAction("Index");
            }

            return View(profile);
        }

        // GET: AdminProfiles/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Profile profile = await db.Profiles.FindAsync(id);
            Profile profile = profileRepository.GetData((int) id);
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }

        // POST: AdminProfiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,BirthDate")] Profile profile)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(profile).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                profileRepository.Update(profile.ID, profile);
                return RedirectToAction("Index");
            }
            return View(profile);
        }

        // GET: AdminProfiles/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Profile profile = await db.Profiles.FindAsync(id);
            Profile profile = profileRepository.GetData((int) id);
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }

        // POST: AdminProfiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            //Profile profile = await db.Profiles.FindAsync(id);
            //db.Profiles.Remove(profile);
            //await db.SaveChangesAsync();
            profileRepository.Delete(id);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
