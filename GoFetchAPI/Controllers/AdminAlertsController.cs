﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.DAL.Repositories;
using GoFetchAPI.DAL.Repository_Interfaces;
using GoFetchAPI.Models;

namespace GoFetchAPI.Controllers
{
    public class AdminAlertsController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IAlertRepository alertsRepository = new AlertRepository();
        private IAlertTypeRepository alertTypesRepository = new AlertTypeRepository();

        // GET: AdminAlerts
        public async Task<ActionResult> Index()
        {
            //var alerts = db.Alerts.Include(a => a.AlertType);
            var alerts = alertsRepository.GetData();
            return View(alerts.ToList());
        }

        // GET: AdminAlerts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Alert alert = await db.Alerts.FindAsync(id);
            Alert alert = alertsRepository.GetData((int)id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            return View(alert);
        }

        // GET: AdminAlerts/Create
        public ActionResult Create()
        {
            //ViewBag.AlertTypeID = new SelectList(db.AlertTypes, "ID", "Description");
            ViewBag.AlertTypeID = new SelectList(alertTypesRepository.GetData(), "ID", "Description");
            return View();
        }

        // POST: AdminAlerts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,AlertTypeID,Message")] Alert alert)
        {
            if (ModelState.IsValid)
            {
                //db.Alerts.Add(alert);
                //await db.SaveChangesAsync();
                alertsRepository.Create(alert);
                return RedirectToAction("Index");
            }

            //ViewBag.AlertTypeID = new SelectList(db.AlertTypes, "ID", "Description", alert.AlertTypeID);
            ViewBag.AlertTypeID = new SelectList(alertTypesRepository.GetData(), "ID", "Description");
            return View(alert);
        }

        // GET: AdminAlerts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Alert alert = await db.Alerts.FindAsync(id);
            Alert alert = alertsRepository.GetData((int)id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            //ViewBag.AlertTypeID = new SelectList(db.AlertTypes, "ID", "Description", alert.AlertTypeID);
            ViewBag.AlertTypeID = new SelectList(alertTypesRepository.GetData(), "ID", "Description", alert.AlertTypeID);
            return View(alert);
        }

        // POST: AdminAlerts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,AlertTypeID,Message")] Alert alert)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(alert).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                alertsRepository.Update(alert.ID, alert);
                return RedirectToAction("Index");
            }
            //ViewBag.AlertTypeID = new SelectList(db.AlertTypes, "ID", "Description", alert.AlertTypeID);
            ViewBag.AlertTypeID = new SelectList(alertTypesRepository.GetData(), "ID", "Description", alert.AlertTypeID);
            return View(alert);
        }

        // GET: AdminAlerts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Alert alert = await db.Alerts.FindAsync(id);
            Alert alert = alertsRepository.GetData((int)id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            return View(alert);
        }

        // POST: AdminAlerts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            //Alert alert = await db.Alerts.FindAsync(id);
            //db.Alerts.Remove(alert);
            //await db.SaveChangesAsync();
            alertsRepository.Delete(id);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
