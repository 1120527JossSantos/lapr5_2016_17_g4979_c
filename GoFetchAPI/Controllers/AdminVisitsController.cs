﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.Models;

namespace GoFetchAPI.Controllers
{
    public class AdminVisitsController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IVisitRepository visitRepository = new VisitRepository();

        // GET: AdminVisits
        public async Task<ActionResult> Index()
        {
            //var visits = db.Visits.Include(v => v.POI);
            var visits = visitRepository.GetData();
            return View(visits.ToList());
        }

        // GET: AdminVisits/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Visit visit = await db.Visits.FindAsync(id);
            Visit visit = visitRepository.GetData((int) id);
            if (visit == null)
            {
                return HttpNotFound();
            }
            return View(visit);
        }

        // GET: AdminVisits/Create
        public ActionResult Create()
        {
            //ViewBag.POIID = new SelectList(db.POIs, "ID", "Name");
            ViewBag.POIID = new SelectList(visitRepository.GetData(), "ID", "Name");
            return View();
        }

        // POST: AdminVisits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,POIID,StartTime")] Visit visit)
        {
            if (ModelState.IsValid)
            {
                //db.Visits.Add(visit);
                //await db.SaveChangesAsync();
                visitRepository.Create(visit);
                return RedirectToAction("Index");
            }

            //ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", visit.POIID);
            ViewBag.POIID = new SelectList(visitRepository.GetData(), "ID", "Name", visit.POIID);
            return View(visit);
        }

        // GET: AdminVisits/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Visit visit = await db.Visits.FindAsync(id);
            Visit visit = visitRepository.GetData((int) id);
            if (visit == null)
            {
                return HttpNotFound();
            }
            //ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", visit.POIID);
            ViewBag.POIID = new SelectList(visitRepository.GetData(), "ID", "Name", visit.POIID);
            return View(visit);
        }

        // POST: AdminVisits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,POIID,StartTime")] Visit visit)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(visit).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                visitRepository.UpdateVisit(visit);
                return RedirectToAction("Index");
            }
            //ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", visit.POIID);
            ViewBag.POIID = new SelectList(visitRepository.GetData(), "ID", "Name", visit.POIID);
            return View(visit);
        }

        // GET: AdminVisits/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Visit visit = await db.Visits.FindAsync(id);
            Visit visit = visitRepository.GetData((int) id);
            if (visit == null)
            {
                return HttpNotFound();
            }
            return View(visit);
        }

        // POST: AdminVisits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            //Visit visit = await db.Visits.FindAsync(id);
            //db.Visits.Remove(visit);
            //await db.SaveChangesAsync();
            Visit visit = visitRepository.GetData(id);
            visitRepository.DeleteVisit(visit);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
