﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using GoFetchAPI.DAL.Repositories;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.Controllers
{
    public class POICategoryController : ApiController
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        POICategoryRepository poiCategoryRepository = new POICategoryRepository();

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/POICategory/All")]
        public IHttpActionResult GetAllPOICategory()
        {
            List<POICategory> POICategoryList = poiCategoryRepository.GetData();
            if (POICategoryList == null)
            {
                return NotFound();
            }

            return Ok(poiCategoryRepository.ConvertModelListToDTO(POICategoryList));
        }

        [ResponseType(typeof(POICategory))]
        public IHttpActionResult GetPOICategory(int id)
        {
            POICategory poiCategory = poiCategoryRepository.GetData(id);
            if (poiCategory == null)
            {
                return BadRequest();
            }

            return Ok(poiCategory);
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/POICategory/PutPOICategory")]
        [ResponseType(typeof(POICategory))]
        public IHttpActionResult PutPOICategory(int id, POICategoryDTO poiCategoryDTO)
        {
            POICategory poiCategory = poiCategoryRepository.ConvertDTOToModel(poiCategoryDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != poiCategory.ID)
            {
                return BadRequest();
            }

            poiCategoryRepository.UpdatePOICategory(poiCategory);

            return StatusCode(HttpStatusCode.NoContent);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/POICategory/PostPOICategory")]
        [ResponseType(typeof(POICategory))]
        public IHttpActionResult PostPOICategory(POICategoryDTO poiCategoryDTO)
        {
            POICategory poiCategory = poiCategoryRepository.ConvertDTOToModel(poiCategoryDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            poiCategoryRepository.InsertPOICategory(poiCategory);

            return CreatedAtRoute("DefaultApi", new { id = poiCategory.ID }, poiCategory);
        }

        [ResponseType(typeof(POICategory))]
        public IHttpActionResult DeletePOICategory(int id)
        {
            POICategory poiCategory = poiCategoryRepository.GetData(id);
            if (poiCategory == null)
            {
                return NotFound();
            }

            poiCategoryRepository.DeletePOICategory(poiCategory);

            return Ok(poiCategoryRepository.ConvertModelToDTO(poiCategory));
        }

        //// GET: POICategory
        //public ActionResult Index()
        //{
        //    return View(db.POICategories.ToList());
        //}

        //// GET: POICategory/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    POICategory pOICategory = db.POICategories.Find(id);
        //    if (pOICategory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(pOICategory);
        //}

        //// GET: POICategory/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: POICategory/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,Description")] POICategory pOICategory)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.POICategories.Add(pOICategory);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(pOICategory);
        //}

        //// GET: POICategory/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    POICategory pOICategory = db.POICategories.Find(id);
        //    if (pOICategory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(pOICategory);
        //}

        //// POST: POICategory/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ID,Description")] POICategory pOICategory)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(pOICategory).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(pOICategory);
        //}

        //// GET: POICategory/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    POICategory pOICategory = db.POICategories.Find(id);
        //    if (pOICategory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(pOICategory);
        //}

        //// POST: POICategory/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    POICategory pOICategory = db.POICategories.Find(id);
        //    db.POICategories.Remove(pOICategory);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
