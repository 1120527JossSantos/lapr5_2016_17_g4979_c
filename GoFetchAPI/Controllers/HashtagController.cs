﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using GoFetchAPI.DAL;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.Controllers
{
    public class HashtagController : ApiController
    {
        private HashtagRepository hashtagRepository = new HashtagRepository();

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Hashtag/All")]
        public IHttpActionResult GetAllHashtag()
        {
            List<Hashtag> HashtagList = hashtagRepository.GetData();
            if (HashtagList == null)
            {
                return NotFound();
            }

            return Ok(hashtagRepository.ConvertModelListToDTO(HashtagList));
        }

        [ResponseType(typeof(Hashtag))]
        public IHttpActionResult GetHashtag(int label)
        {
            Hashtag hashtag = hashtagRepository.GetData(label);
            if (hashtag == null)
            {
                return BadRequest();
            }

            return Ok(hashtag);
        }

        [ResponseType(typeof (Hashtag))]
        public IHttpActionResult PutHashtag(string label, HashtagDTO hashtagDTO)
        {
            Hashtag hashtag = hashtagRepository.ConvertDTOToModel(hashtagDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (label != hashtag.Label)
            {
                return BadRequest();
            }

            hashtagRepository.UpdateHashtag(hashtag);

            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(Hashtag))]
        public IHttpActionResult PostHashtag(HashtagDTO hashtagDTO)
        {
            Hashtag hashtag = hashtagRepository.ConvertDTOToModel(hashtagDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            hashtagRepository.InsertHashtag(hashtag);

            return CreatedAtRoute("DefaultApi", new {label = hashtag.Label}, hashtag);
        }

        [ResponseType(typeof(Hashtag))]
        public IHttpActionResult DeleteHashtag(int label)
        {
            Hashtag hashtag = hashtagRepository.GetData(label);
            if (hashtag == null)
            {
                return NotFound();
            }

            hashtagRepository.DeleteHashtag(hashtag);

            return Ok(hashtagRepository.ConvertModelToDTO(hashtag));
        }


        //private ApplicationDbContext db = new ApplicationDbContext();

        //// GET: Hashtag
        //public async Task<ActionResult> Index()
        //{
        //    return View(await db.HashTags.ToListAsync());
        //}

        //// GET: Hashtag/Details/5
        //public async Task<ActionResult> Details(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Hashtag hashtag = await db.HashTags.FindAsync(id);
        //    if (hashtag == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(hashtag);
        //}

        //// GET: Hashtag/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Hashtag/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[System.Web.Mvc.HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "Label,IsTentative")] Hashtag hashtag)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.HashTags.Add(hashtag);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    return View(hashtag);
        //}

        //// GET: Hashtag/Edit/5
        //public async Task<ActionResult> Edit(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Hashtag hashtag = await db.HashTags.FindAsync(id);
        //    if (hashtag == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(hashtag);
        //}

        //// POST: Hashtag/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[System.Web.Mvc.HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "Label,IsTentative")] Hashtag hashtag)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(hashtag).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(hashtag);
        //}

        //// GET: Hashtag/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Hashtag hashtag = await db.HashTags.FindAsync(id);
        //    if (hashtag == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(hashtag);
        //}

        //// POST: Hashtag/Delete/5
        //[System.Web.Mvc.HttpPost, System.Web.Mvc.ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    Hashtag hashtag = await db.HashTags.FindAsync(id);
        //    db.HashTags.Remove(hashtag);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
