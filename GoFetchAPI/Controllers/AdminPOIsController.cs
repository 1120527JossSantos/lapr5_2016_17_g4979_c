﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoFetchAPI.Models;
using GoFetchAPI.DAL;
using GoFetchAPI.Helpers;
using System.Net.Http;
using GoFetchAPI.ViewModels;
using GoFetchAPI.DAL.Repositories;
using Microsoft.AspNet.Identity;

namespace GoFetchAPI.Controllers
{
    public class AdminPOIsController : Controller
    {
        // delete
        //private ApplicationDbContext db = new ApplicationDbContext();

        private POIRepository repository = new POIRepository();
        private POICategoryRepository categories = new POICategoryRepository();
        private ScheduleRepository schedules = new ScheduleRepository();
        private AlertRepository alertRepository = new AlertRepository();
        private RestrictionRepository restrictionsRepository = new RestrictionRepository();
        private POIConnectionRepository connectionsRepository = new POIConnectionRepository();


        // GET: AdminPOIs
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index()
        {
            //var pOIs = db.POIs.Include(p => p.Category);
            var pOIs = repository.GetData(null);
            return View(pOIs.ToList());
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> IndexTentative()
        {
            //var pOIs = db.POIs.Include(p => p.Category);
            var pOIs = repository.GetTentative();
            return View("Tentative", pOIs.ToList());
        }

        // GET: AdminPOIs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //POI pOI = await db.POIs.FindAsync(id);
            List <POIConnection> from =connectionsRepository.GetDataFrom((int)id);
            List<String> fromPOI = new List<String>();
            foreach(POIConnection pc in from) { fromPOI.Add(pc.POI2.Name); }
            ViewBag.FROMList = fromPOI;
            List<POIConnection> to = connectionsRepository.GetDataTo((int)id);
            List<String> toPOI = new List<String>();
            foreach (POIConnection pc in to) { toPOI.Add(pc.POI1.Name); }
            ViewBag.TOList = toPOI;
            POI pOI = repository.GetData((int)id);
            if (pOI == null)
            {
                return HttpNotFound();
            }
            return View(pOI);
        }

        // GET: AdminPOIs/Create
        public ActionResult Create()
        {
            //ViewBag.CategoryID = new SelectList(db.POICategories, "ID", "Description");
            ViewBag.CategoryID = new SelectList(categories.GetData(), "ID", "Description");
            return View();
        }

        // POST: AdminPOIs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name,Description,VisitTime,IsTentative,Latitude,Longitude,CategoryID")] POI pOI)
        {
            if (ModelState.IsValid)
            {
                //db.POIs.Add(pOI);
                //await db.SaveChangesAsync();
                pOI.ProposerID = User.Identity.GetUserId();
                repository.Create(pOI);
                return RedirectToAction("Index");
            }

            //ViewBag.CategoryID = new SelectList(db.POICategories, "ID", "Description", pOI.CategoryID);
            ViewBag.CategoryID = new SelectList(categories.GetData(), "ID", "Description", pOI.CategoryID);
            return View(pOI);
        }

        // GET: AdminPOIs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //POI pOI = await db.POIs.FindAsync(id);
            POI pOI = repository.GetData((int)id);
            if (pOI == null)
            {
                return HttpNotFound();
            }
            //ViewBag.CategoryID = new SelectList(db.POICategories, "ID", "Description", pOI.CategoryID);
            ViewBag.CategoryID = new SelectList(categories.GetData(), "ID", "Description", pOI.CategoryID);
            return View(pOI);
        }

        // POST: AdminPOIs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,Description,VisitTime,IsTentative,Latitude,Longitude,Altitude,CategoryID")] POI pOI)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(pOI).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                repository.UpdatePOI(pOI.ID, pOI);
                return RedirectToAction("Index");
            }

            //ViewBag.CategoryID = new SelectList(db.POICategories, "ID", "Description", pOI.CategoryID);
            ViewBag.CategoryID = new SelectList(categories.GetData(), "ID", "Description", pOI.CategoryID);
            return View(pOI);
        }

        // GET: AdminPOIs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //POI pOI = await db.POIs.FindAsync(id);
            POI pOI = repository.GetData((int)id);
            if (pOI == null)
            {
                return HttpNotFound();
            }
            return View(pOI);
        }

        // POST: AdminPOIs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            //POI pOI = await db.POIs.FindAsync(id);
            //db.POIs.Remove(pOI);
            //await db.SaveChangesAsync();
            POI poi = repository.GetData(id);
            repository.DeletePOI(poi);
            RejectPOI(poi);
            return RedirectToAction("Index");
        }

        // GET: AdminPOIs/Accept/5
        [HttpGet]
        public async Task<ActionResult> Accept(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            POI pOI = repository.GetData((int)id);
            if (pOI == null)
            {
                return HttpNotFound();
            }

            var pOIs = repository.GetData(null);
            if (pOIs == null)
            {
                return HttpNotFound();
            }

            POIAcceptionViewModel poiReturn = new POIAcceptionViewModel();
            poiReturn.POI = pOI;

            //ViewBag.CategoryID = new SelectList(db.POICategories, "ID", "Description", pOI.CategoryID);
            ViewBag.CategoryID = new SelectList(categories.GetData(), "ID", "Description", pOI.CategoryID);

            ViewBag.Restrictions = restrictionsRepository.GetData();

                //new MultiSelectList(restrictionsRepository.GetData(), "Code", "Description");

            //ViewBag.POIsID = new SelectList(repository.GetData(), "ID", "Name");
            return View(poiReturn);
        }

        // POST: AdminPOIs/Accept/5
        [HttpPost]

        public async Task<ActionResult> AcceptPOIAction(object[] info, object[] restrictions, object[] from, object[] to, int[] distance)
        {
           
            int id = Convert.ToInt32(info[0]);
          
            POIAcceptionViewModel vm = new POIAcceptionViewModel();
           
            vm.POI.Name = Convert.ToString(info[1]);
            vm.POI.Description = Convert.ToString(info[2]);
            vm.POI.VisitTime = Convert.ToInt32(info[3]);
            vm.POI.Latitude = Convert.ToString(info[4]);
            vm.POI.Longitude = Convert.ToString(info[5]);
            vm.POI.Altitude = Convert.ToString(info[6]);
            vm.POI.CategoryID = Convert.ToInt32(info[7]);
            var p = 1;
            Schedule sh = new Schedule();
            sh.Day = 1;
            sh.POIID = id;
            sh.Open = Convert.ToString(info[8]);
            sh.Close = Convert.ToString(info[9]);

            Schedule sh1 = new Schedule();
            sh1.Day = 2;
            sh1.POIID = id;
            sh1.Open = Convert.ToString(info[10]);
            sh1.Close = Convert.ToString(info[11]);

            Schedule sh2 = new Schedule();
            sh2.Day = 3;
            sh2.POIID = id;
            sh2.Open = Convert.ToString(info[12]);
            sh2.Close = Convert.ToString(info[13]);

            Schedule sh3 = new Schedule();
            sh3.Day = 4;
            sh3.POIID = id;
            sh3.Open = Convert.ToString(info[14]);
            sh3.Close = Convert.ToString(info[15]);

            Schedule sh4 = new Schedule();
            sh4.Day = 5;
            sh4.POIID = id;
            sh4.Open = Convert.ToString(info[16]);
            sh4.Close = Convert.ToString(info[17]);

            Schedule sh5 = new Schedule();
            sh5.Day = 6;
            sh5.POIID = id;
            sh5.Open = Convert.ToString(info[18]);
            sh5.Close = Convert.ToString(info[19]);

            Schedule sh6 = new Schedule();
            sh6.Day = 7;
            sh6.POIID = id;
            sh6.Open = Convert.ToString(info[20]);
            sh6.Close = Convert.ToString(info[21]);
            

            List<Schedule> workschedule = new List<Schedule>();
            workschedule.Add(sh);
            workschedule.Add(sh1);
            workschedule.Add(sh2);
            workschedule.Add(sh3);
            workschedule.Add(sh4);
            workschedule.Add(sh5);
            workschedule.Add(sh6);

            vm.WorkSchedule = workschedule;
            vm.POI.WorkSchedule = workschedule;
            
            List<POIConnection> connections = new List<POIConnection>();

            for (var i = 0; i < distance.Length; i++)
            {
                POIConnection con = new POIConnection();
                string s = Convert.ToString(from[i]);
                if (s.Equals("X"))
                {
                    con.POI1ID = id;
                }
                else
                {
                    con.POI1ID = Convert.ToInt32(from[i]);
                }

                string t = Convert.ToString(to[i]);
                if (t.Equals("X"))
                {
                    con.POI2ID = id;
                }
                else
                {
                    con.POI2ID = Convert.ToInt32(to[i]);
                }

                con.Distance = Convert.ToInt32(distance[i]);

                connections.Add(con);
            }

            List<Restriction> restrictionsPOI = new List<Restriction>();

            for (var i = 0; i < restrictions.Length; i++)
            {
                string restrictionCode = Convert.ToString(restrictions[i]);
                Restriction res = restrictionsRepository.GetData(restrictionCode);
                restrictionsPOI.Add(res);
            }
            vm.RestrictionList = restrictionsPOI;
            vm.POI.RestrictionList = restrictionsPOI;
            vm.WorkSchedule = workschedule;
            

            var j = 1;
            if (ModelState.IsValid)
            {
                int v = 1;
                
                // update the POI status
                repository.Update(id, vm.POI);
                repository.Accept(id);
                POI poi = repository.GetData(id);           
                AcceptPOI(poi);

                // save the POI's connections
                
                connectionsRepository.CreateMultipleConnections(connections);

                // save the POI's work schedule
                repository.AddSchedule(id, vm.WorkSchedule);

                // register to ALGAV app
                vm.Connections = connections;
                vm.POI.ID = poi.ID;
                var DTO = repository.ConvertModelToALGAVDTO(vm);
                var client = WebApiHttpClientALGAV.GetClient();
                 HttpResponseMessage response = await client.PostAsJsonAsync("register_poi", DTO);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
               

    }

            return RedirectToAction("Index");
        }

        // POST: AdminPOIs/Accept/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Accept([Bind(Include = "POI,Connections,WorkSchedule")] POIAcceptionViewModel vm)
        {
            if (ModelState.IsValid)
            {
                // update the POI status
                //repository.Update(vm.POI.ID, vm.POI);
                repository.Accept(vm.POI.ID);
                
                AcceptPOI(vm.POI);

                // save the POI's connections
                //connections.CreateMultiple(vm.Connections);

                // save the POI's work schedule
                repository.AddSchedule(vm.POI.ID, vm.WorkSchedule);
               // int v = 1;
                // register to ALGAV app
                var DTO = repository.ConvertModelToALGAVDTO(vm);
                var client = WebApiHttpClientALGAV.GetClient();
                HttpResponseMessage response = await client.PostAsJsonAsync("register_poi", DTO);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }


        public void AcceptPOI(POI poi)
        {
            AcceptRejectPOIAlert(poi, true);
        }

        public void RejectPOI(POI poi)
        {
            AcceptRejectPOIAlert(poi, false);
        }

        public void AcceptRejectPOIAlert(POI poi, bool acceptOrReject)
        {
            string message = null;
            if (!poi.IsTentative && acceptOrReject)
            {
                message = "Your point of interest suggestion \""+ poi.Description +"\" has been accepted!";
            }

            if (!acceptOrReject)
            {
                message = "Your point of interest suggestion \"" + poi.Description + "\" has been rejected.";
            }

            alertRepository.AddUserMessageAlert(poi.Proposer, message, 1);
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
