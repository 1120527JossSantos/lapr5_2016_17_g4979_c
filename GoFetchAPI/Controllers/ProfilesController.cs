﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.Models;
using GoFetchAPI.ViewModels;

namespace GoFetchAPI.Controllers
{
    public class ProfilesController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IAlertRepository alertsRepository = new AlertRepository();
        private IProfileRepository userRepository = new ProfileRepository();

        // GET: Profiles
        public async Task<ActionResult> Index()
        {
            //return View(await db.Profiles.ToListAsync());
            return View(userRepository.GetData().ToList());
        }

        // GET: Profiles/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Profile profile = await db.Profiles.FindAsync(id);
            Profile profile = userRepository.GetData((int) id);
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }

        // GET: Profiles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Profiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name,BirthDate")] Profile profile)
        {
            if (ModelState.IsValid)
            {
                //db.Profiles.Add(profile);
                //await db.SaveChangesAsync();
                userRepository.Create(profile);
                return RedirectToAction("Index");
            }

            return View(profile);
        }

        // GET: Profiles/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Profile profile = await db.Profiles.FindAsync(id);
            Profile profile = userRepository.GetData((int) id);
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }

        // POST: Profiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,BirthDate")] Profile profile)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(profile).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                userRepository.Update(profile.ID,profile);
                return RedirectToAction("Index");
            }
            return View(profile);
        }

        // GET: Profiles/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Profile profile = await db.Profiles.FindAsync(id);
            Profile profile = userRepository.GetData((int) id);
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }

        // POST: Profiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            //Profile profile = await db.Profiles.FindAsync(id);
            //db.Profiles.Remove(profile);
            //await db.SaveChangesAsync();
            userRepository.Delete(id);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
