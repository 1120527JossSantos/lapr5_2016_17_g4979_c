﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.Models;

namespace GoFetchAPI.Controllers
{
    public class AdminRestrictionsController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IRestrictionRepository restrictionRepository = new RestrictionRepository();

        // GET: AdminRestrictions
        public async Task<ActionResult> Index()
        {
            return View(restrictionRepository.GetData().ToList());
        }

        // GET: AdminRestrictions/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Restriction restriction = await db.Restrictions.FindAsync(id);
            Restriction restriction = restrictionRepository.GetData(id);
            if (restriction == null)
            {
                return HttpNotFound();
            }
            return View(restriction);
        }

        // GET: AdminRestrictions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminRestrictions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Code,Description")] Restriction restriction)
        {
            if (ModelState.IsValid)
            {
                //db.Restrictions.Add(restriction);
                //await db.SaveChangesAsync();
                restrictionRepository.Create(restriction);
                return RedirectToAction("Index");
            }

            return View(restriction);
        }

        // GET: AdminRestrictions/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Restriction restriction = await db.Restrictions.FindAsync(id);
            Restriction restriction = restrictionRepository.GetData(id);
            if (restriction == null)
            {
                return HttpNotFound();
            }
            return View(restriction);
        }

        // POST: AdminRestrictions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Code,Description")] Restriction restriction)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(restriction).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                restrictionRepository.Update(restriction.Code, restriction);
                return RedirectToAction("Index");
            }
            return View(restriction);
        }

        // GET: AdminRestrictions/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Restriction restriction = await db.Restrictions.FindAsync(id);
            Restriction restriction = restrictionRepository.GetData(id);
            if (restriction == null)
            {
                return HttpNotFound();
            }
            return View(restriction);
        }

        // POST: AdminRestrictions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            //Restriction restriction = await db.Restrictions.FindAsync(id);
            //db.Restrictions.Remove(restriction);
            //await db.SaveChangesAsync();
            restrictionRepository.Delete(id);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
