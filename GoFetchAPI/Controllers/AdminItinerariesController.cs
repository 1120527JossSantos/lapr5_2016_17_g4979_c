﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.Models;
using Microsoft.AspNet.Identity;
using GoFetchAPI.DTO;
using GoFetchAPI.Helpers;

namespace GoFetchAPI.Controllers
{
    public class AdminItinerariesController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private ItineraryRepository itineraryRepository = new ItineraryRepository();
        private POIRepository poiRepository = new POIRepository();

        // GET: AdminItineraries
        public async Task<ActionResult> Index()
        {
            List<Itinerary> list = itineraryRepository.GetData().ToList();
            ViewBag.NumberPOI = list.Count;
            return View(list);
        }

        // GET: AdminItineraries/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Itinerary itinerary = await db.Itineraries.FindAsync(id);
            Itinerary itinerary = itineraryRepository.GetData((int) id);
            if (itinerary == null)
            {
                return HttpNotFound();
            }
            return View(itinerary);
        }

        // GET: AdminItineraries/Create
        public ActionResult Create()
        {
            ViewBag.POIs = new SelectList(poiRepository.GetData(null), "Name", "Description");
            return View();
        }


        // POST: AdminItineraries/Create
        [HttpPost]
        public async Task<ActionResult> CreateItineraryAction(object[] listPOIS,string startTime,int type, int wayoftransportation)
        {
            //usar a seguinte função para guardar apenas a data e nao horas
            // Convert.ToDateTime(m.Data_de_leitura).Date
            ALGAVItineraryRequest dto = new ALGAVItineraryRequest();
            List<int> dispo = new List<int>();
            for (int i = 1; i < 64; i++) { dispo.Add(i); }
            
            dto.disponibilidadeTurista = dispo;
            List<int> POIIDs = new List<int>();
            for (int i = 0; i < listPOIS.Length; i++)
            {
                POIIDs.Add(Convert.ToInt32(listPOIS[i]));
            }
                dto.pois = POIIDs;
           
            string[] words = startTime.Split('-');
            DateTime date = new DateTime(Convert.ToInt32(words[0]), Convert.ToInt32(words[1]), Convert.ToInt32(words[2]));
            dto.diadasemana = ((int)date.DayOfWeek) + 1;
            dto.transporte = wayoftransportation;
            string d = User.Identity.GetUserId();
            Itinerary itinerary = null;
            switch (type)
            {
                case 1:
                case 2:
                    itinerary = await WebApiHttpClientALGAV.sendToAlgavForTimedItinerary(dto, date);
                    break;
                case 3:
                    itinerary = await WebApiHttpClientALGAV.sendToAlgavForRegularItinerary(dto, date);
                    break;
            }
            if (itinerary != null)
            {
                itineraryRepository.Create(itinerary);
                return RedirectToAction("Index");
            }
            ViewBag.Error = "The itenerary you tried to create wasn't possible, try again with some others parameters";
            return RedirectToAction("Index");
        }

        // POST: AdminItineraries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,StartTime,Type,POIs")] Itinerary itinerary)
        {
            if (ModelState.IsValid)
            {
                //db.Itineraries.Add(itinerary);
                //await db.SaveChangesAsync();
                itineraryRepository.Create(itinerary);
                return RedirectToAction("Index");
            }
            
            return View(itinerary);
        }

        // GET: AdminItineraries/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Itinerary itinerary = await db.Itineraries.FindAsync(id);
            Itinerary itinerary = itineraryRepository.GetData((int) id);
            if (itinerary == null)
            {
                return HttpNotFound();
            }
            return View(itinerary);
        }

        // POST: AdminItineraries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,StartTime,Type")] Itinerary itinerary)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(itinerary).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                itineraryRepository.UpdateItinerary(itinerary);
                return RedirectToAction("Index");
            }
            return View(itinerary);
        }

        // GET: AdminItineraries/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Itinerary itinerary = await db.Itineraries.FindAsync(id);
            Itinerary itinerary = itineraryRepository.GetData((int) id);
            if (itinerary == null)
            {
                return HttpNotFound();
            }
            return View(itinerary);
        }

        // POST: AdminItineraries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            //Itinerary itinerary = await db.Itineraries.FindAsync(id);
            //db.Itineraries.Remove(itinerary);
            //await db.SaveChangesAsync();
            Itinerary itinerary = itineraryRepository.GetData(id);
            itineraryRepository.DeleteItinerary(itinerary);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
