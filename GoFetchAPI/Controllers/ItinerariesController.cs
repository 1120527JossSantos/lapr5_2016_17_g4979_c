﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;
using Microsoft.AspNet.Identity;
using GoFetchAPI.Helpers;
using Newtonsoft.Json;
using System.Threading;
using System.Globalization;

namespace GoFetchAPI.Controllers
{
    public class ItinerariesController : ApiController
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private ItineraryRepository itineraryRepository = new ItineraryRepository();
        private AlertRepository alerts = new AlertRepository();

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Itinerary/All")]
        public IHttpActionResult GetAllItinerary()
        {
            List<Itinerary> ItineraryList = itineraryRepository.GetData();
            if (ItineraryList == null)
            {
                return NotFound();
            }

            return Ok(itineraryRepository.ConvertModelListToDTO(ItineraryList));
        }

        /**
         * Returns all itineraries from an authenticated user.
         * 
         */
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Itinerary/User")]
        [System.Web.Http.Authorize(Roles="Tourist")]
        public IHttpActionResult GetUserItineraries()
        {
            string userID = HttpContext.Current.User.Identity.GetUserId();
            List<Itinerary> ItineraryList = itineraryRepository.ByUser(userID);
            
            if(ItineraryList == null)
            {
                return NotFound();
            }

            return Ok(itineraryRepository.ConvertModelListToDTO(ItineraryList));
        }

        /**
         * Returns all default itineraries (created by admin users)
         * 
         */
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Itinerary/Default")]
        public IHttpActionResult GetDefaultItineraries()
        {
            List<Itinerary> ItineraryList = itineraryRepository.GetDefault();

            if (ItineraryList == null)
            {
                return NotFound();
            }

            return Ok(itineraryRepository.ConvertModelListToDTO(ItineraryList));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Itinerary/{id}")]
        public IHttpActionResult GetItinerary(int id)
        {
            Itinerary itinerary = itineraryRepository.GetData(id);
            if (itinerary == null)
            {
                return NotFound();
            }

            string dataJSON = JsonConvert.SerializeObject(itinerary);
            return Ok(dataJSON);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Itinerary/GetJson/{id}")]
        public IHttpActionResult GetJson(int id)
        {
            Itinerary itinerary = itineraryRepository.GetData(id);
            if (itinerary == null)
            {
                return NotFound();
            }
            
            string dataJSON = JsonConvert.SerializeObject(itineraryRepository.ConvertVisitsToSGRAI(itinerary));
            return Ok(dataJSON);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/Itinerary/Request")]
        public async Task<IHttpActionResult> RequestItenerary(ItineraryRequestDTO dto)
        {
            ALGAVItineraryRequest outgoingDTO = new ALGAVItineraryRequest();
            string[] actualTimeOpen = dto.startTime.Split(':');
            int multiplierOpen = int.Parse(actualTimeOpen[1]) / 15;
            double minutesOpen = multiplierOpen * 0.25;
            int blockOpen = Convert.ToInt32(4 * (int.Parse(actualTimeOpen[0]) + minutesOpen - 8));

            string[] actualTimeClose = dto.endTime.Split(':');
            int multiplierClose = int.Parse(actualTimeClose[1]) / 15;
            double minutesClose = multiplierClose * 0.25;
            int blockClose = Convert.ToInt32(4 * (int.Parse(actualTimeClose[0]) + minutesClose - 8));
            List<int> dispo = new List<int>();
            for (int i = blockOpen; i < blockClose; i++) { dispo.Add(i); }
            outgoingDTO.disponibilidadeTurista = dispo;
            int date = new int();
            DateTime convertedDate = DateTime.Parse(dto.startDate);
            
            outgoingDTO.diadasemana = ((int)convertedDate.DayOfWeek)+1;
            outgoingDTO.transporte = dto.wayOfTransportation;
            outgoingDTO.pois = dto.pois;
            Itinerary itinerary=null;
            switch (dto.type)
            {
                case 1:
                case 2:
                    itinerary = await WebApiHttpClientALGAV.sendToAlgavForTimedItinerary(outgoingDTO,convertedDate);
                    break;
                case 3:
                    itinerary = await WebApiHttpClientALGAV.sendToAlgavForRegularItinerary(outgoingDTO,convertedDate);
                    break;
            }
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            if (itinerary == null)
            {
                alerts.Create(new Alert { UserID= HttpContext.Current.User.Identity.GetUserId(),Message= "The Itinerary request you did for " + convertedDate.ToLongDateString() + " was refused due to time constraints." ,AlertTypeID=2});
                return BadRequest();

            }
           
            itinerary.Type = "" + dto.type;
            itinerary.UserID = HttpContext.Current.User.Identity.GetUserId();
            Itinerary it = itineraryRepository.Create(itinerary);
            //inserir na base de dados
            alerts.Create(new Alert { UserID = HttpContext.Current.User.Identity.GetUserId(), Message = "The Itinerary request you did for " + convertedDate.ToLongDateString() + " was accepted. You can view the details on your custom Itinerary list, under \"My Itineraries\".", AlertTypeID = 2 });

            return Ok();
        }

        [ResponseType(typeof(Itinerary))]
        public IHttpActionResult PutItinerary(int id, ItineraryDTO itineraryDTO)
        {
            Itinerary itinerary = itineraryRepository.ConvertDTOToModel(itineraryDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != itinerary.ID)
            {
                return BadRequest();
            }

            itineraryRepository.UpdateItinerary(itinerary);

            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(Itinerary))]
        public IHttpActionResult PostItinerary(ItineraryDTO itineraryDTO)
        {
            Itinerary itinerary = itineraryRepository.ConvertDTOToModel(itineraryDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return CreatedAtRoute("DefaultApi", new { id = itinerary.ID }, itinerary);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/Itinerary/Delete/{id}")]
        public IHttpActionResult DeleteItinerary(int id)
        {
            // check if current user is the itinerary owner
            string userID = HttpContext.Current.User.Identity.GetUserId();

            Itinerary itinerary = itineraryRepository.GetData(id);
            if (itinerary == null)
            {
                return NotFound();
            }

            if(itinerary.UserID != userID)
            {
                return BadRequest();
            }

            itineraryRepository.DeleteItinerary(itinerary);
            return Ok();
        }

        //// GET: Itineraries
        //public async Task<ActionResult> Index()
        //{
        //    var itineraries = itineraryRepository.GetData();
        //    return View(itineraries.ToList());
        //}

        //public List<Itinerary> ReturnItineraryList()
        //{
        //    var itineraries = itineraryRepository.GetData();
        //    return itineraries.ToList();
        //} 

        //// GET: Itineraries/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    //Itinerary itinerary = await db.Itineraries.FindAsync(id);
        //    Itinerary itinerary = itineraryRepository.GetData((int) id);
        //    if (itinerary == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(itinerary);
        //}

        //// GET: Itineraries/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Itineraries/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "ID,StartTime,Type")] Itinerary itinerary)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //db.Itineraries.Add(itinerary);
        //        //await db.SaveChangesAsync();
        //        itineraryRepository.Create(itinerary);
        //        return RedirectToAction("Index");
        //    }

        //    return View(itinerary);
        //}

        //// GET: Itineraries/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    //Itinerary itinerary = await db.Itineraries.FindAsync(id);
        //    Itinerary itinerary = itineraryRepository.GetData((int) id);
        //    if (itinerary == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(itinerary);
        //}

        //// POST: Itineraries/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "ID,StartTime,Type")] Itinerary itinerary)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //db.Entry(itinerary).State = EntityState.Modified;
        //        //await db.SaveChangesAsync();
        //        itineraryRepository.Update(itinerary.ID, itinerary);
        //        return RedirectToAction("Index");
        //    }
        //    return View(itinerary);
        //}

        //// GET: Itineraries/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    //Itinerary itinerary = await db.Itineraries.FindAsync(id);
        //    Itinerary itinerary = itineraryRepository.GetData((int) id);
        //    if (itinerary == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(itinerary);
        //}

        //// POST: Itineraries/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    //Itinerary itinerary = await db.Itineraries.FindAsync(id);
        //    //db.Itineraries.Remove(itinerary);
        //    //await db.SaveChangesAsync();
        //    itineraryRepository.Delete(id);
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
