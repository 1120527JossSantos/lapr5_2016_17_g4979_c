﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.Controllers
{
    public class VisitsController : ApiController
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IVisitRepository visitRepository = new VisitRepository();

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Visit/All")]
        public IHttpActionResult GetAllVisit()
        {
            List<Visit> VisitList = visitRepository.GetData();
            if (VisitList == null)
            {
                return NotFound();
            }

            return Ok(visitRepository.ConvertModelListToDTO(VisitList));
        }

        [ResponseType(typeof(Visit))]
        public IHttpActionResult GetVisit(int id)
        {
            Visit visit = visitRepository.GetData(id);
            if (visit == null)
            {
                return BadRequest();
            }

            return Ok(visit);
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/Visit/PutVisit")]
        [ResponseType(typeof(Visit))]
        public IHttpActionResult PutVisit(int id, VisitDTO visitDTO)
        {
            Visit visit = visitRepository.ConvertDTOToModel(visitDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != visit.ID)

            {
                return BadRequest();
            }

            visitRepository.UpdateVisit(visit);

            return StatusCode(HttpStatusCode.NoContent);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/Visit/PostVisit")]
        [ResponseType(typeof(Visit))]
        public IHttpActionResult PostVisit(VisitDTO visitDTO)
        {
            Visit visit = visitRepository.ConvertDTOToModel(visitDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            visitRepository.InsertVisit(visit);

            return CreatedAtRoute("DefaultApi", new { id = visit.ID }, visit);
        }

        [ResponseType(typeof(Visit))]
        public IHttpActionResult DeleteVisit(int id)
        {
            Visit visit = visitRepository.GetData(id);
            if (visit == null)
            {
                return NotFound();
            }

            visitRepository.DeleteVisit(visit);

            return Ok(visitRepository.ConvertModelToDTO(visit));
        }

        //// GET: Visits
        //public async Task<ActionResult> Index()
        //{
        //    var visits = db.Visits.Include(v => v.POI);
        //    return View(await visits.ToListAsync());
        //}

        //// GET: Visits/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Visit visit = await db.Visits.FindAsync(id);
        //    if (visit == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(visit);
        //}

        //// GET: Visits/Create
        //public ActionResult Create()
        //{
        //    ViewBag.POIID = new SelectList(db.POIs, "ID", "Name");
        //    return View();
        //}

        //// POST: Visits/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "ID,POIID,StartTime")] Visit visit)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Visits.Add(visit);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", visit.POIID);
        //    return View(visit);
        //}

        //// GET: Visits/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Visit visit = await db.Visits.FindAsync(id);
        //    if (visit == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", visit.POIID);
        //    return View(visit);
        //}

        //// POST: Visits/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "ID,POIID,StartTime")] Visit visit)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(visit).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", visit.POIID);
        //    return View(visit);
        //}

        //// GET: Visits/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Visit visit = await db.Visits.FindAsync(id);
        //    if (visit == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(visit);
        //}

        //// POST: Visits/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Visit visit = await db.Visits.FindAsync(id);
        //    db.Visits.Remove(visit);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
