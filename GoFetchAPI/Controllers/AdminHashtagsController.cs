﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.Models;

namespace GoFetchAPI.Controllers
{
    public class AdminHashtagsController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IHashtagRepository hashtagRepository = new HashtagRepository();

        // GET: AdminHashtags
        public async Task<ActionResult> Index()
        {
            return View(hashtagRepository.GetData().ToList());
        }

        // GET: AdminHashtags/IndexTentative
        public async Task<ActionResult> IndexTentative()
        {
            List<Hashtag> TentativeHashtags = hashtagRepository.GetTentative();
            return View("Tentative", TentativeHashtags);
        }

        // GET: AdminHashtags/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Hashtag hashtag = await db.HashTags.FindAsync(id);
            Hashtag hashtag = hashtagRepository.GetData(id);
            if (hashtag == null)
            {
                return HttpNotFound();
            }
            return View(hashtag);
        }

        // GET: AdminHashtags/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminHashtags/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Label,IsTentative")] Hashtag hashtag)
        {
            if (ModelState.IsValid)
            {
                //db.HashTags.Add(hashtag);
                //await db.SaveChangesAsync();
                hashtagRepository.Create(hashtag.Label);
                return RedirectToAction("Index");
            }

            return View(hashtag);
        }

        // GET: AdminHashtags/Accept/5
        public async Task<ActionResult> Accept(int id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            hashtagRepository.Accept(id);
            return RedirectToAction("Index"); ;
        }

        // GET: AdminHashtags/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Hashtag hashtag = await db.HashTags.FindAsync(id);
            Hashtag hashtag = hashtagRepository.GetData(id);
            if (hashtag == null)
            {
                return HttpNotFound();
            }
            return View(hashtag);
        }

        // POST: AdminHashtags/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Label,IsTentative")] Hashtag hashtag)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(hashtag).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                hashtagRepository.UpdateHashtag(hashtag);
                return RedirectToAction("Index");
            }
            return View(hashtag);
        }

        // GET: AdminHashtags/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Hashtag hashtag = await db.HashTags.FindAsync(id);
            Hashtag hashtag = hashtagRepository.GetData(id);
            if (hashtag == null)
            {
                return HttpNotFound();
            }
            return View(hashtag);
        }

        // POST: AdminHashtags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            //Hashtag hashtag = await db.HashTags.FindAsync(id);
            //db.HashTags.Remove(hashtag);
            //await db.SaveChangesAsync();
            Hashtag hashtag = hashtagRepository.GetData(id);
            hashtagRepository.DeleteHashtag(hashtag);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
