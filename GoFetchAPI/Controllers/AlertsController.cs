﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.DAL.Repositories;
using GoFetchAPI.DAL.Repository_Interfaces;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;
using GoFetchAPI.ViewModels;
using Microsoft.AspNet.Identity;

namespace GoFetchAPI.Controllers
{
    public class AlertsController : ApiController
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IAlertRepository alertsRepository = new AlertRepository();
        private IProfileRepository userRepository = new ProfileRepository();
        private IAlertTypeRepository alertTypesRepository = new AlertTypeRepository();
        private IItineraryRepository itineraryRepository = new ItineraryRepository();

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Alert/All")]
        public IHttpActionResult GetAllAlert()
        {
            List<Alert> AlertList = alertsRepository.GetData();
            if (AlertList == null)
            {
                return NotFound();
            }

            return Ok(alertsRepository.ConvertModelListToDTO(AlertList));
        }

        [ResponseType(typeof(Alert))]
        public IHttpActionResult GetAlert(int id)
        {
            Alert alert = alertsRepository.GetData(id);
            if (alert == null)
            {
                return BadRequest();
            }

            return Ok(alert);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Alert/GetAlertsByUser")]
        [System.Web.Http.Authorize(Roles = "Tourist")]
        public IHttpActionResult GetAlertsByUser()
        {
            //verificar
            List<Alert> AlertList = alertsRepository.GetAlertsByUser(User.Identity.GetUserId()).ToList();
            if (AlertList == null)
            {
                return NotFound();
            }

            return Ok(alertsRepository.ConvertModelListToDTO(AlertList));
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/Alert/PutAlert")]
        [ResponseType(typeof(Alert))]
        public IHttpActionResult PutAlert(int id, AlertDTO alertDTO)
        {
            Alert alert = alertsRepository.ConvertDTOToModel(alertDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != alert.ID)
            {
                return BadRequest();
            }

            alertsRepository.UpdateAlert(alert);

            return StatusCode(HttpStatusCode.NoContent);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/Alert/PostAlert")]
        [ResponseType(typeof(Alert))]
        public IHttpActionResult PostAlert(AlertDTO alertDTO)
        {
            Alert alert = alertsRepository.ConvertDTOToModel(alertDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            alertsRepository.InsertAlert(alert);

            return CreatedAtRoute("DefaultApi", new { id = alert.ID }, alert);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/Alert/Delete/{id}")]
        [System.Web.Http.Authorize(Roles = "Tourist")]
        public IHttpActionResult DeleteAlert(int id)
        {
            Alert alert = alertsRepository.GetData(id);
            if (alert == null)
            {
                return NotFound();
            }

            if(alert.User.Id == User.Identity.GetUserId())
            {
                alertsRepository.DeleteAlert(alert);
                return Ok();
            } else
            {
                return BadRequest();
            }            
        }

        [ResponseType(typeof(void))]
        public void AddAlert([Bind(Include = "POIID,message")] int POIID, string message)
        {
            List<Itinerary> itineraries = itineraryRepository.GetData();
                foreach (Itinerary itinerary in itineraries)
                {
                    List<Visit> visits = itinerary.VisitList;
                    foreach (Visit visit in visits)
                    {
                        if (POIID == visit.POIID)
                        {
                            alertsRepository.AddUserMessageAlert(itinerary.User, message,1);
                        }
                    }
                }
        }

        //// GET: Alerts
        //public async Task<ActionResult> Index()
        //{
        //    //var alerts = db.Alerts.Include(a => a.AlertType);
        //    var alerts = alertsRepository.GetData();
        //    return View(alerts.ToList());
        //}

        //// GET: Alerts/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    //Alert alert = await db.Alerts.FindAsync(id);
        //    Alert alert = alertsRepository.GetData((int) id);
        //    if (alert == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(alert);
        //}

        //// GET: Alerts/Create
        //public ActionResult Create()
        //{
        //    //ViewBag.AlertTypeID = new SelectList(db.AlertTypes, "ID", "Description");
        //    ViewBag.AlertTypeID = new SelectList(alertTypesRepository.GetData(), "ID", "Description");
        //    return View();
        //}

        //// POST: Alerts/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "ID,AlertTypeID,Message")] Alert alert)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //db.Alerts.Add(alert);
        //        //await db.SaveChangesAsync();
        //        alertsRepository.Create(alert);
        //        return RedirectToAction("Index");
        //    }

        //    //ViewBag.AlertTypeID = new SelectList(db.AlertTypes, "ID", "Description", alert.AlertTypeID);
        //    ViewBag.AlertTypeID = new SelectList(alertTypesRepository.GetData(), "ID", "Description");
        //    return View(alert);
        //}

        //// GET: Alerts/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    //Alert alert = await db.Alerts.FindAsync(id);
        //    Alert alert = alertsRepository.GetData((int) id);
        //    if (alert == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    //ViewBag.AlertTypeID = new SelectList(db.AlertTypes, "ID", "Description", alert.AlertTypeID);
        //    ViewBag.AlertTypeID = new SelectList(alertTypesRepository.GetData(), "ID", "Description", alert.AlertTypeID);
        //    return View(alert);
        //}

        //// POST: Alerts/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "ID,AlertTypeID,Message")] Alert alert)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //db.Entry(alert).State = EntityState.Modified;
        //        //await db.SaveChangesAsync();
        //        alertsRepository.Update(alert.ID, alert);
        //        return RedirectToAction("Index");
        //    }
        //    //ViewBag.AlertTypeID = new SelectList(db.AlertTypes, "ID", "Description", alert.AlertTypeID);
        //    ViewBag.AlertTypeID = new SelectList(alertTypesRepository.GetData(), "ID", "Description", alert.AlertTypeID);
        //    return View(alert);
        //}

        // POST: Alerts/AddAlert
        //[ValidateAntiForgeryToken]
        //public ActionResult AddAlert([Bind(Include = "POIID,message")] int POIID, string message)
        //{
        //    List<Itinerary> itineraries = itineraryRepository.GetData();
        //    foreach (Itinerary itinerary in itineraries)
        //    {
        //        List<Visit> visits = itinerary.VisitList;
        //        foreach (Visit visit in visits)
        //        {
        //            if (POIID == visit.POIID)
        //            {
        //                alertsRepository.AddUserMessageAlert(itinerary.User, message);
        //            }
        //        }
        //    }

        //    ////Falta implementar
        //    //ApplicationUser profile = userRepository.GetData();
        //    //if (profile == null)
        //    //{
        //    //    HttpNotFound();
        //    //}

        //    //Alert existingAlert = alertsRepository.GetData(Alert.AlertID);
        //    //if (existingAlert == null)
        //    //{
        //    //    Alert newAlert = new Alert();
        //    //    userRepository.AddAlert(profile.ID, newAlert);
        //    //}
        //    return Redirect("/Alert");
        //}

        //// GET: Alerts/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    //Alert alert = await db.Alerts.FindAsync(id);
        //    Alert alert = alertsRepository.GetData((int) id);
        //    if (alert == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(alert);
        //}

        //// POST: Alerts/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    //Alert alert = await db.Alerts.FindAsync(id);
        //    //db.Alerts.Remove(alert);
        //    //await db.SaveChangesAsync();
        //    alertsRepository.Delete(id);
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
