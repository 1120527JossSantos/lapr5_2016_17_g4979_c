﻿using System.Collections.Generic;
using System.Net;
using System.Web;
using GoFetchAPI.Models;
using GoFetchAPI.DAL;
using GoFetchAPI.DAL.Repository_Interfaces;
using GoFetchAPI.DAL.Repositories;
using System.Web.Http;
using System.Web.Http.Description;
using GoFetchAPI.DTO;
using Newtonsoft.Json;
using System.Web.Http.Cors;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace GoFetchAPI.Controllers
{
    [EnableCors(origins: "http://localhost:8000", headers: "*", methods: "*")]
    public class POIController : ApiController
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IPOIRepository repository = new POIRepository();
        private IPOICategoryRepository categories = new POICategoryRepository();
        private IHashtagRepository hashtags = new HashtagRepository();
        private IAlertRepository alertRepository = new AlertRepository();
        private IItineraryRepository itineraryRepository = new ItineraryRepository();

        // GET: POI
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/POI/All")]
        public IHttpActionResult GetAllPOI()
        {
            string userID = HttpContext.Current.User.Identity.GetUserId();
            List<POI> POIList = repository.GetData(userID);

            if (POIList == null)
            {
                return NotFound();
            }

            string dataJSON = JsonConvert.SerializeObject(repository.ConvertModelListToDTO(POIList));
            return Ok(dataJSON);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/POI/Tentative")]
        public IHttpActionResult GetAllTentativePOI()
        {
            List<POI> POIList = repository.GetTentative();
            if (POIList == null)
            {
                return NotFound();
            }
            string dataJSON = JsonConvert.SerializeObject(repository.ConvertModelListToDTO(POIList));
            return Ok(dataJSON);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/POI/ByCategory/{Id}")]
        public IHttpActionResult ByCategory(int Id)
        {
            List<POI> list = repository.GetByCategory(Id);

            if(list == null)
            {
                return NotFound();
            }

            string dataJson = JsonConvert.SerializeObject(repository.ConvertModelListToDTO(list));
            return Ok(dataJson);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/POI/ByHashtag/{Label}")]
        public IHttpActionResult ByHashtag(string Label)
        {
            Hashtag hashtag = hashtags.ByLabel(Label);
            if (hashtag == null) return NotFound();

            List<POI> list = repository.GetByHashtag(hashtag);
            if (list == null)
            {
                return NotFound();
            }

            string dataJson = JsonConvert.SerializeObject(repository.ConvertModelListToDTO(list));
            return Ok(dataJson);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/POI/ByTerm/{Term}")]
        public IHttpActionResult ByTerm(string Term)
        {
            List<POI> list = repository.GetByTerm(Term);
            if (list == null)
            {
                return NotFound();
            }

            string dataJson = JsonConvert.SerializeObject(repository.ConvertModelListToDTO(list));
            return Ok(dataJson);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/POI/Suggest")]
        [System.Web.Http.Authorize(Roles = "Tourist")]
        public IHttpActionResult SuggestPOI(SuggestPOI poiDTO)
        {
            string userID = HttpContext.Current.User.Identity.GetUserId();
            POI poi = repository.ConvertSuggestPOIToModel(poiDTO,userID);

            if (poi == null)
            {
                return BadRequest();
            }

            POI newPOI = repository.Create(poi);
            return CreatedAtRoute("GetPOIById", new { id = newPOI.ID }, newPOI);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/POI/Update/{id}")]
        [System.Web.Http.Authorize(Roles = "Tourist")]
        public IHttpActionResult UpdatePOI(int id, SuggestPOI poiDTO)
        {
            string userID = HttpContext.Current.User.Identity.GetUserId();
            POI poi = repository.ConvertSuggestPOIToModel(poiDTO, userID);
            POI toUpdate = repository.GetData(id);

            if (poi == null || toUpdate == null)
            {
                return NotFound();
            }

            if(toUpdate.ProposerID != userID || !toUpdate.IsTentative)
            {
                return BadRequest();
            }

            repository.UpdatePOI(id, poi);
            return StatusCode(HttpStatusCode.NoContent);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/POI/AddHashtag")]
        //[System.Web.Http.Authorize(Roles = "Tourist")]
        public IHttpActionResult SuggestHashtag(AddHashtagDTO dto)
        {

            POI poi = repository.GetData(dto.ID);

            // if POI doesn't exist or is still tentative, it can't have Hashtags
            if(poi == null || poi.IsTentative)
            {
                return BadRequest();
            }

            //repository.AddHashtag(poi.ID, dto.Label);

            // check if the Hashtag already exists in the POI context
            //bool hasHashtag = repository.HasHashtag(poi.ID, dto.Label);
            bool hasHashtag = hashtags.IsInPOI(dto.Label, poi.ID);

            // add the new Hashtag to the chosen POI
            if (!hasHashtag)
            {
                Hashtag hashtag = hashtags.CreateAndAdd(poi.ID, dto.Label);
            }

            return Ok();
        }

        /**
         * Returns all POI's and their connections in a JSON string formatted for the 3D simulation.
         * 
         */
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/POI/AllJson")]
        public IHttpActionResult GetAllJson()
        {
            var pois = repository.GetAccepted();
            if(pois == null)
            {
                return BadRequest();
            }

            string dataJSON = JsonConvert.SerializeObject(repository.ConvertModelListToDTO_SGRAI(pois));
            return Ok(dataJSON);
        }

        [System.Web.Http.Route("api/POI/{id}", Name = "GetPOIById")]
        [ResponseType(typeof(POI))]
        public IHttpActionResult GetPOI(int id)
        {
            POI poi = repository.GetData(id);
            if (poi == null)
            {
                return BadRequest();
            }

            string dataJSON = JsonConvert.SerializeObject(poi);
            return Ok(dataJSON);
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/POI/PutPOI")]
        [ResponseType(typeof(POI))]
        public IHttpActionResult PutPOI(int id, POIDTO poiDTO)
        {
            POI poi = repository.ConvertDTOToModel(poiDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != poi.ID)

            {
                return BadRequest();
            }

            repository.UpdatePOI(id, poi);

            return StatusCode(HttpStatusCode.NoContent);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/POI/PostPOI")]
        [ResponseType(typeof(POI))]
        public IHttpActionResult PostPOI(POIDTO poiDTO)
        {
            POI poi = repository.ConvertDTOToModel(poiDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            POI newPOI = repository.Create(poi);

            return CreatedAtRoute("DefaultApi", new { id = newPOI.ID }, newPOI);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/POI/Delete/{id}")]
        [System.Web.Http.Authorize(Roles = "Tourist")]
        public IHttpActionResult DeletePOI(int id)
        {
            POI poi = repository.GetData(id);

            if (poi == null)
            {
                return NotFound();
            }

            // check if current user is the proposer
            string userID = HttpContext.Current.User.Identity.GetUserId();

            if(poi.ProposerID == userID)
            {
                repository.DeletePOI(poi);
                return Ok();
            } else
            {
                return BadRequest();
            }
        }

        //public void AcceptRejectPOI(POI poi, bool acceptOrReject)
        //{
        //    string message = null;
        //    if (poi.IsTentative && acceptOrReject)
        //    {
        //        poi.IsTentative = false;
        //        message = "Your POI suggestion has been accepted, congratz!";
        //    }

        //    if (poi.IsTentative && acceptOrReject == false)
        //    {
        //        repository.DeletePOI(poi);
        //        message = "Your POI suggestion hasn't been accepted, sorry mate, try next time!";
        //    }

        //    alertRepository.AddUserMessageAlert(poi.Proponente, message);
        //}

        // POST: POI/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]

        //        public ActionResult Create([Bind(Include = "ID,Name,Description,VisitTime,IsTentative,Latitude,Longitude,CategoryID")] POI pOI)
        //        {
        //            if (ModelState.IsValid)
        //            {
        //                //db.POIs.Add(pOI);
        //                //db.SaveChanges();
        //                repository.Create(pOI);
        //                return RedirectToAction("Index");
        //            }

        //            //ViewBag.CategoryID = new SelectList(db.POICategories, "ID", "Description", pOI.CategoryID);
        //            ViewBag.CategoryID = new SelectList(categories.GetData(), "ID", "Description", pOI.CategoryID);
        //            return View(pOI);
        //        }

        //        // POST: POI/AddHashtag
        //        [ValidateAntiForgeryToken]
        //        public ActionResult AddHashtag([Bind(Include = "POI,Label")] POIHashtagsViewModel Hashtag)
        //        {
        //            POI poi = repository.GetData(Hashtag.POI.ID);
        //            if(poi == null)
        //            {
        //                return HttpNotFound();
        //            }

        //            // Validation rules...
        //            if(Hashtag.Label == "")
        //            {
        //                return HttpNotFound();
        //            }

        //            // Avoids duplicate Hashtags
        //            // Link...
        //            Hashtag existingHashtag = hashtags.GetData(Hashtag.Label);
        //            if(existingHashtag == null)
        //            {
        //                // Create the new Hashtag
        //                Hashtag newHashtag = new Hashtag(Hashtag.Label);

        //                // Add hashtag to POI
        //                repository.AddHashtag(poi.ID, newHashtag);
        //            }

        //            return Redirect("/POI/Details/" + poi.ID);
        //        }

        //        // GET: POI/Edit/5
        //        public ActionResult Edit(int? id)
        //        {
        //            if (id == null)
        //            {
        //                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //            }
        //            //POI pOI = db.POIs.Find(id);
        //            POI pOI = repository.GetData((int) id);
        //            if (pOI == null)
        //            {
        //                return HttpNotFound();
        //            }

        //            //ViewBag.CategoryID = new SelectList(db.POICategories, "ID", "Description", pOI.CategoryID);
        //            ViewBag.CategoryID = new SelectList(categories.GetData(), "ID", "Description", pOI.CategoryID);

        //            return View(pOI);
        //        }

        //        // POST: POI/Edit/5
        //        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //        [HttpPost]
        //        [ValidateAntiForgeryToken]
        //        public ActionResult Edit([Bind(Include = "ID,Name,VisitTime,IsTentative,Latitude,Longitude,CategoryID")] POI pOI)
        //        {
        //            if (ModelState.IsValid)
        //            {
        //                //db.Entry(pOI).State = EntityState.Modified;
        //                //db.SaveChanges();

        //                repository.Update(pOI.ID, pOI);
        //                return RedirectToAction("Index");
        //            }

        //            //ViewBag.CategoryID = new SelectList(db.POICategories, "ID", "Description", pOI.CategoryID);
        //            ViewBag.CategoryID = new SelectList(categories.GetData(), "ID", "Description", pOI.CategoryID);

        //            return View(pOI);
        //        }

        //        // GET: POI/Delete/5
        //        public ActionResult Delete(int? id)
        //        {
        //            if (id == null)
        //            {
        //                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //            }
        //            //POI pOI = db.POIs.Find(id);
        //            POI pOI = repository.GetData((int) id);
        //            if (pOI == null)
        //            {
        //                return HttpNotFound();
        //            }
        //            return View(pOI);
        //        }

        //        // POST: POI/Delete/5
        //        [HttpPost, ActionName("Delete")]
        //        [ValidateAntiForgeryToken]
        //        public ActionResult DeleteConfirmed(int id)
        //        {
        //            //POI pOI = db.POIs.Find(id);
        //            //db.POIs.Remove(pOI);
        //            //db.SaveChanges();
        //            repository.Delete(id);
        //            return RedirectToAction("Index");
        //        }

        //        /*protected override void Dispose(bool disposing)
        //        {
        //            if (disposing)
        //            {
        //                db.Dispose();
        //            }
        //            base.Dispose(disposing);
        //        }*/
        //    }
    }
}
