﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using GoFetchAPI.DAL;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.Controllers
{
    public class SchedulesController : ApiController
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private IScheduleRepository scheduleRepository = new ScheduleRepository();

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Schedule/All")]
        public IHttpActionResult GetAllSchedule()
        {
            List<Schedule> ScheduleList = scheduleRepository.GetData();
            if (ScheduleList == null)
            {
                return NotFound();
            }

            return Ok(scheduleRepository.ConvertModelListToDTO(ScheduleList));
        }

        [ResponseType(typeof(Schedule))]
        public IHttpActionResult GetSchedule(int day)
        {
            Schedule schedule = scheduleRepository.GetData(day);
            if (schedule == null)
            {
                return BadRequest();
            }

            return Ok(schedule);
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/Schedule/PutSchedule")]
        [ResponseType(typeof(Schedule))]
        public IHttpActionResult PutSchedule(int day, ScheduleDTO scheduleDTO)
        {
            Schedule schedule = scheduleRepository.ConvertDTOToModel(scheduleDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (day != schedule.Day)
            {
                return BadRequest();
            }

            scheduleRepository.UpdateSchedule(schedule);

            return StatusCode(HttpStatusCode.NoContent);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/Schedule/PostSchedule")]
        [ResponseType(typeof(Schedule))]
        public IHttpActionResult PostSchedule(ScheduleDTO scheduleDTO)
        {
            Schedule schedule = scheduleRepository.ConvertDTOToModel(scheduleDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            scheduleRepository.InsertSchedule(schedule);

            return CreatedAtRoute("DefaultApi", new { day = schedule.Day }, schedule);
        }

        [ResponseType(typeof(Schedule))]
        public IHttpActionResult DeleteSchedule(int day)
        {
            Schedule schedule = scheduleRepository.GetData(day);
            if (schedule == null)
            {
                return NotFound();
            }

            scheduleRepository.DeleteSchedule(schedule);

            return Ok(scheduleRepository.ConvertModelToDTO(schedule));
        }

        //// GET: Schedules
        //public async Task<ActionResult> Index()
        //{
        //    var schedules = db.Schedules.Include(s => s.POI);
        //    return View(await schedules.ToListAsync());
        //}

        //// GET: Schedules/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Schedule schedule = await db.Schedules.FindAsync(id);
        //    if (schedule == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(schedule);
        //}

        //// GET: Schedules/Create
        //public ActionResult Create()
        //{
        //    ViewBag.POIID = new SelectList(db.POIs, "ID", "Name");
        //    return View();
        //}

        //// POST: Schedules/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "Day,POIID,Open,Close")] Schedule schedule)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Schedules.Add(schedule);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", schedule.POIID);
        //    return View(schedule);
        //}

        //// GET: Schedules/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Schedule schedule = await db.Schedules.FindAsync(id);
        //    if (schedule == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", schedule.POIID);
        //    return View(schedule);
        //}

        //// POST: Schedules/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "Day,POIID,Open,Close")] Schedule schedule)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(schedule).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.POIID = new SelectList(db.POIs, "ID", "Name", schedule.POIID);
        //    return View(schedule);
        //}

        //// GET: Schedules/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Schedule schedule = await db.Schedules.FindAsync(id);
        //    if (schedule == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(schedule);
        //}

        //// POST: Schedules/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Schedule schedule = await db.Schedules.FindAsync(id);
        //    db.Schedules.Remove(schedule);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
