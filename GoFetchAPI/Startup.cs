﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GoFetchAPI.Startup))]
namespace GoFetchAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
