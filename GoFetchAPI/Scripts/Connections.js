
var pois;
var poisToPresent=null;
var poisExclude;
var counterPOIs=0;
var tableRowsId=0;
var connections;
$(document).ready(function() {
  //createWidget();
  var url= 'https://localhost:44301/api/POI/All';
  makeXmlHttpGetCall(url,false,false,start);
});

function start(resp){
    pois=[];
    response = JSON.parse(resp);
    for(var i = 0; i < response.length; i++) {
       pois.push({ name: response[i].Name, id: response[i].ID });
    }

   createWidget();
}
function makeXmlHttpGetCall(url,async,xml,functionRedirect) {
    
    xmlHttpObj = new XMLHttpRequest();
    if (xmlHttpObj) {
        xmlHttpObj.open("GET", url, async);
        xmlHttpObj.onreadystatechange = function () {
            if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
               
                var resp;
                var response;
                if (xml == true) {
                    response = xmlHttpObj.responseXML;
                } else {
                    resp = xmlHttpObj.responseText;
                    response = JSON.parse(resp);
                }
                functionRedirect(response);
            }
        };
       xmlHttpObj.send(null);
    }
}

function createWidget() {
    
   // insertPOIS();
    var divWidget = document.createElement("div");
    divWidget.id = "divWidget";
    divWidget.className = "divGeral";
    divWidget.style.display = "block";
    document.getElementById("widget").appendChild(divWidget);

    createDivPOIS();
    
    createButtonAdd();

}

function insertPOIS(){
    var url= 'https://localhost:44301/api/POI/All';
    makeXmlHttpGetCall(url,false,false,start);
}


function createDivPOIS(){
    var divPOIS = document.createElement("div");
    divPOIS.id = "divPOIS";
    divPOIS.className = "divGeral";
    divPOIS.style.display = "none";
    document.getElementById("widget").appendChild(divPOIS);

    var table = document.createElement("table");
    table.id="tablePOIS"; 
    table.appendChild(createHeadersTable());   
    document.getElementById("divPOIS").appendChild(table);    

}

function createButtonAdd(){
    var divButton = document.createElement("div");
    divButton.id = "divButton";
    divButton.className = "divGeral";
    divButton.style.display = "block";
    document.getElementById("widget").appendChild(divButton);

    var button=document.createElement("input");
    button.setAttribute("id", "add");
    button.setAttribute("type", "button");
    button.setAttribute("class", "btn btn-default");
    button.setAttribute("value", "Add");
    button.setAttribute("onclick","addPOI()");

    document.getElementById("widget").appendChild(button);
}

function addPOI(){
    if(poisToPresent==null){
        
        poisToPresent=pois;
    }
    if(tableRowsId!=0){     
            removeFromList();
    
    }
    if(poisToPresent.length!=0){


    tableRowsId++;
    var divPOIS= document.getElementById("divPOIS");
    if(divPOIS.style.display=="none"){
        document.getElementById("divPOIS").style.display="block";
    }

    var trPOI=document.createElement("tr");
    trPOI.setAttribute("id",tableRowsId);

    var tdSelect=document.createElement("td");


    var select = document.createElement("SELECT");
    var idSelect= "select"+tableRowsId;
    select.setAttribute("id", idSelect);

    tdSelect.appendChild(select);

    document.getElementById("tablePOIS").appendChild(trPOI);
    trPOI.appendChild(tdSelect);
    

    for(var i=0; i< poisToPresent.length;i++){
        var option = document.createElement("option");
        option.setAttribute("value", poisToPresent[i].id);
        var optiontext = document.createTextNode(poisToPresent[i].name);
        option.appendChild(optiontext);
        document.getElementById("select"+tableRowsId).appendChild(option);
    }

    var tdFrom=document.createElement("td");
    var nameCheckboxFrom="from"+tableRowsId;
    var checkboxFrom=createInputPOI(nameCheckboxFrom,nameCheckboxFrom);
    checkboxFrom.setAttribute("onclick","createConnections()");
    tdFrom.appendChild(checkboxFrom);
    trPOI.appendChild(tdFrom);

    var tdTo=document.createElement("td");
    var nameCheckboxTo="to"+tableRowsId;
    var checkboxTo=createInputPOI(nameCheckboxTo,nameCheckboxTo);
    checkboxTo.setAttribute("onclick","createConnections()");


    tdTo.appendChild(checkboxTo);
    trPOI.appendChild(tdTo);

    var tdCost=document.createElement("td");
    
    var inputNumber=document.createElement("input");
    inputNumber.setAttribute("type","number");
    inputNumber.setAttribute("style","width:100px");
    var idInputCost="cost"+tableRowsId;
    inputNumber.setAttribute("id",idInputCost);
    inputNumber.setAttribute("min",0);
    inputNumber.setAttribute("onchange","createConnections()");
    tdCost.appendChild(inputNumber);

    trPOI.appendChild(tdCost);

    var tdDelete=document.createElement("td");
    var deletePOI=document.createElement("a");
    deletePOI.setAttribute("href","#");
    deletePOI.setAttribute("class","btn btn-info btn-sm");

    var span=document.createElement("span");
    span.setAttribute("class","glyphicon glyphicon-trash");
    span.setAttribute("id",tableRowsId);
    
    span.onmouseup= function(event){
        var element = document.getElementById(this.id);
        element.parentNode.removeChild(element);
        counterPOIs--;
        createConnections();
        if(divPOIS.style.display=="block" && document.getElementById("tablePOIS").rows.length==1){
            document.getElementById("divPOIS").style.display="none";
        }
        
    }

    deletePOI.appendChild(span);
    tdDelete.appendChild(deletePOI);


    trPOI.appendChild(tdDelete);
    
    counterPOIs++;
    createConnections();
 
}else{
alert("Nao ha mais POIS!");
   
}
}

function createInputPOI(id,name){
    var checkbox=document.createElement("input");
        checkbox.id=id;
        checkbox.type="checkbox";
        checkbox.name=name;
    return checkbox;
}


function createHeadersTable(){

        var tr = document.createElement("tr");

        var th = document.createElement("th");
        var thText = document.createTextNode("POI");
        th.appendChild(thText);
        tr.appendChild(th);

        var th2 = document.createElement("th");
        var th2Text = document.createTextNode("From");
        th2.appendChild(th2Text);
        tr.appendChild(th2);

        var th3 = document.createElement("th");
        var th3Text = document.createTextNode("To");
        th3.appendChild(th3Text);
        tr.appendChild(th3);

        var th4 = document.createElement("th");
        var th4Text = document.createTextNode("Cost");
        th4.appendChild(th4Text);
        tr.appendChild(th4);


        var th4 = document.createElement("th");
        tr.appendChild(th4);

        return tr;
}

function removeFromList(){
    var l = document.getElementById("tablePOIS").rows.length;
     poisExclude=[];
    var cont=-1;

    for ( var i = 1; i < l; i++ )
    {
        var tr = document.getElementById("tablePOIS").rows[i];
        document.getElementById("select"+ tr.id).disabled=true;
        
        cont++;
        poisExclude[cont]=document.getElementById("select"+ tr.id).value;
    }

    newPOIListToPresent(poisExclude);
}
function newPOIListToPresent(poisExclude){
    var cont=-1;
    poisToPresent=[];
        for(var i = 0; i < pois.length; i++ ){
            if(!isInArray(pois[i].id,poisExclude)){
                cont++;
                poisToPresent[cont]=pois[i];
            }
        }

}
function isInArray(value, array) {
    for(var i = 0; i < array.length; i++ ){
        if(array[i]==value){
            return true;
        }
    }
  return false;
}

//id do poi em questão é X
function createConnections(){
    connections=[];
    var l = document.getElementById("tablePOIS").rows.length;

    for ( var i = 1; i < l; i++ )
    {
        var tr = document.getElementById("tablePOIS").rows[i];
        var idPOI=document.getElementById("select"+tr.id).value;
        var distance=document.getElementById("cost"+tr.id).value;

        if(distance!=0){
            var checkboxFrom=document.getElementById("from"+tr.id);
            var checkboxTo=document.getElementById("to"+tr.id);
            if(checkboxFrom.checked || checkboxTo.checked){
                if(checkboxFrom.checked){
                    connections.push({ POI1ID:idPOI,  POI2ID:'X', Distance: distance });
                }

                if(checkboxTo.checked){
                    connections.push({ POI1ID:'X',  POI2ID: idPOI, Distance: distance});
                }
            }
        }
    }

    console.log(connections);
}
