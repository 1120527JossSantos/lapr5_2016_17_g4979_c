﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GoFetchAPI.Models
{
    public class POI
    {
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "Please give the point of interest a name.")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please give the point of interest a description.")]
        public string Description { get; set; }

        [DisplayName("Estimated visit time (minutes)")]
        [Required(ErrorMessage = "Please indicate the point of interest's estimated visit time.")]
        public int VisitTime { get; set; }

        //[HiddenInput(DisplayValue = false)]
        [DisplayName("Tentative")]
        public bool IsTentative { get; set; }

        [Required(ErrorMessage = "Please indicate the point of interest's latitude.")]
        //[RegularExpression("^[1-9]?[0-9].[0-9]{1,6}$", ErrorMessage = "Please input a valid format (00.000000).")]
        public string Latitude { get; set; }

        [Required(ErrorMessage = "Please indicate the point of interest's longitude.")]
        //[RegularExpression("^[1-9]?[0-9].[0-9]{1,6}$", ErrorMessage = "Please input a valid format (00.000000).")]
        public string Longitude { get; set; }
        public string Altitude { get; set; }

        [DisplayName("Category")]
        public int CategoryID { get; set; }
        public virtual POICategory Category { get; set; }

        public virtual List<Schedule> WorkSchedule { get; set; }

        public virtual List<Hashtag> HashtagList { get; set; }

        public virtual List<Restriction> RestrictionList { get; set; }

        public virtual List<Image> ImageList { get; set; }
        public string ProposerID { get; set; }

        public virtual ApplicationUser Proposer { get; set; }

        public POI()
        {
            this.IsTentative = true;
        }
    }
}
