﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoFetchAPI.Models
{
    public class Profile
    {
        [Key]
        public int ID { get; set; }

        public virtual ApplicationUser User { get; set; }

        public string Name { get; set; }

        public DateTime BirthDate { get; set; }

        public virtual List<Restriction> RestrictionList { get; set; }
    }
}