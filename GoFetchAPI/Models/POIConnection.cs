﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.Models
{
    public class POIConnection
    {
        [Key]
        [Column(Order = 0)]
        public int POI1ID { get; set; }
        public virtual POI POI1 { get; set; }

        [Key]
        [Column(Order = 1)]
        public int POI2ID { get; set; }
        public virtual POI POI2 { get; set; }

        public int Distance { get; set; }

    }
}
