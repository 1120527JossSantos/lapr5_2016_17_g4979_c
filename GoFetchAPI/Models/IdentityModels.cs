﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace GoFetchAPI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [JsonIgnore]
        public virtual ICollection<Alert> Alerts { get; set; }

        public ApplicationUser()
        {
            Alerts = new List<Alert>();
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<POI> POIs { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Restriction> Restrictions { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<Hashtag> HashTags { get; set; }
        public DbSet<Alert> Alerts { get; set; }
        public DbSet<Itinerary> Itineraries { get; set; }
        public DbSet<POICategory> POICategories { get; set; }
        public DbSet<AlertType> AlertTypes { get; set; }
        public DbSet<POIConnection> POIConnections { get; set; }
        public DbSet<POITravel> POITravels { get; set; }
    }
}