﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoFetchAPI.Models
{
    public class Itinerary
    {
        [Key]
        public int ID { get; set; }

        public string UserID { get; set; }
        public virtual ApplicationUser User { get; set; }
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartTime { get; set; }

        public string Type { get; set; }
        
        public virtual List<Visit> VisitList { get; set; }

        public virtual List<POITravel> travels { get; set; }

       
    }
}