﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.Models
{
    public class Hashtag
    {
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "Please introuce the desired hashtag.")]
        [RegularExpression("^([A-Za-z_][A-Za-z0-9_]*)$", ErrorMessage = "Please input a valid hashtag, without numbers or non alphabetical characters.")]
        public string Label { get; set; }

        public bool IsTentative { get; set; }

        public int POIID { get; set; }
        [JsonIgnore]
        public virtual POI POI { get; set; }

        public Hashtag()
        {
            this.IsTentative = true;
        }
    }
}
