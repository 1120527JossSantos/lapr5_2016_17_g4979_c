﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoFetchAPI.Models
{
    public class Alert
    {
        [Key]
        public int ID { get; set; }

        public int AlertTypeID { get; set; }
        public virtual AlertType AlertType { get; set; }

        public string UserID { get; set; }
        public virtual ApplicationUser User { get; set; }

        public string Message { get; set; }
    }
}