﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.Models
{
    public class POITravel
    {
        [Key]
        public int ID { get; set; }
        public int POI1ID { get; set; }

        public virtual POI POI1 { get; set; }

        public int POI2ID { get; set; }

        public virtual POI POI2 { get; set; }

        public int typeOfTravel { get; set; }


        public int ItineraryID { get; set; }

        [JsonIgnore]
        public virtual Itinerary Itinerary { get; set; }
        
    }
}
