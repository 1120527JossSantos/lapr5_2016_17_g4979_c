﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoFetchAPI.Models
{
    public class Visit
    {
        [Key]
        public int ID { get; set; }

        public int POIID { get; set; }
        public virtual POI POI { get; set; }

        public DateTime StartTime { get; set; }

        public int ItineraryID { get; set; }

        [JsonIgnore]
        public virtual Itinerary Itinerary { get; set; }
    }
}