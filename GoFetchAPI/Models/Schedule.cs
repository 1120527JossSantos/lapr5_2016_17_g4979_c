﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoFetchAPI.Models
{
    public class Schedule
    {
        [Key]
        [Column(Order = 0)]
        public int Day { get; set; }

        [Key]
        [Column(Order = 1)]
        public int POIID { get; set; }
        [JsonIgnore]
        public virtual POI POI { get; set; }

        [RegularExpression("^(((0[8|9])|(1[0-9])|(2[0-3])):((00)|([1|4]5)|(30)))|(00:00)$", ErrorMessage = "Please input a valid time (15 minute interval)")]
        public string Open { get; set; }

        [RegularExpression("^(((0[8|9])|(1[0-9])|(2[0-3])):((00)|([1|4]5)|(30)))|(00:00)$", ErrorMessage = "Please input a valid time (15 minute interval)")]
        public string Close { get; set; }
    }
}