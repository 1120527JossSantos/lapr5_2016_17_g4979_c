﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.Models
{
    public class POICategory
    {
        [Key]
        public int ID { get; set; }

        [DisplayName("Category")]
        public string Description { get; set; }

    }
}
