﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoFetchAPI.Models
{
    public class Image
    {
        [Key]
        public int ID { get; set; }

        public string ImageType { get; set; }

        public byte[] Content { get; set; }
    }
}