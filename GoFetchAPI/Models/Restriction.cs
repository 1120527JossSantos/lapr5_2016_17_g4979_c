﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.Models
{
    public class Restriction
    {
        [Key]
        public string Code { get; set; }

        public string Description { get; set; }
        [JsonIgnore] 
        public virtual List<POI> POIList { get; set; }

        public virtual List<Profile> ProfileList { get; set; }
    }
}
