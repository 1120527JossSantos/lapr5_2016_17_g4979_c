﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public class RestrictionRepository : IRestrictionRepository
    {
        public ApplicationDbContext context;

        public RestrictionRepository()
        {
            context = new ApplicationDbContext();
        }

        public Restriction Create(Restriction restriction)
        {
            context.Restrictions.Add(restriction);
            context.SaveChanges();
            return restriction;
        }

        public bool Delete(string id)
        {
            var restriction = context.Restrictions.Find(id);

            if (restriction != null)
            {
                context.Restrictions.Remove(restriction);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Restriction> GetData()
        {
            var restriction = context.Restrictions.ToList();
            return restriction;
        }

        public Restriction GetData(string id)
        {
            return context.Restrictions.Find(id);
        }

        public bool Update(string id, Restriction NewRestriction)
        {
            var restriction = context.Restrictions.Find(id);

            if (restriction != null)
            {
                restriction.Code = NewRestriction.Code;
                restriction.Description = NewRestriction.Description;
                restriction.POIList = NewRestriction.POIList;
                restriction.ProfileList = NewRestriction.ProfileList;
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}