﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoFetchAPI.DAL.Repository_Interfaces;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL.Repositories
{
    public class AlertTypeRepository : IAlertTypeRepository
    {
        public ApplicationDbContext context;

        public AlertTypeRepository()
        {
            context = new ApplicationDbContext();
        }

        public AlertType Create(AlertType alertType)
        {
            context.AlertTypes.Add(alertType);
            context.SaveChanges();
            return alertType;
        }

        public bool Delete(int id)
        {
            var alertType = context.AlertTypes.Find(id);

            if (alertType != null)
            {
                context.AlertTypes.Remove(alertType);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<AlertType> GetData()
        {
            var alertType = context.AlertTypes.ToList();
            return alertType;
        }

        public AlertType GetData(int id)
        {
            return context.AlertTypes.Find(id);
        }

        public bool Update(int id, AlertType NewAlertType)
        {
            var alertType = context.AlertTypes.Find(id);

            if (alertType != null)
            {
                alertType.ID = NewAlertType.ID;
                alertType.Description = NewAlertType.Description;
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}