﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public class AlertRepository : IAlertRepository
    {
        public ApplicationDbContext context;

        public AlertRepository()
        {
            context = new ApplicationDbContext();
        }

        public Alert Create(Alert alert)
        {
            context.Alerts.Add(alert);
            context.SaveChanges();
            return alert;
        }

        public bool Delete(int id)
        {
            var alert = context.Alerts.Find(id);

            if (alert != null)
            {
                context.Alerts.Remove(alert);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Alert> GetData()
        {
            var alert = context.Alerts.ToList();
            return alert;
        }

        public Alert GetData(int id)
        {
            return context.Alerts.Find(id);
        }

        public bool Update(int id, Alert NewAlert)
        {
            var alert = context.Alerts.Find(id);

            if (alert != null)
            {
                alert.ID = NewAlert.ID;
                alert.AlertType = NewAlert.AlertType;
                alert.Message = NewAlert.Message;
                alert.User = NewAlert.User;
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddUserMessageAlert(ApplicationUser user, string message, int alertTypeID)
        {
            if (user != null && message !=null)
            {
                Alert newAlert = new Alert();
                newAlert.UserID = user.Id;
                newAlert.Message = message;
                newAlert.AlertTypeID = alertTypeID;
                context.Alerts.Add(newAlert);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public List<AlertDTO> ConvertModelListToDTO(List<Alert> alertList)
        {
            List<AlertDTO> dtoList = new List<AlertDTO>();
            foreach (Alert alert in alertList)
            {
                AlertDTO alertDTO = ConvertModelToDTO(alert);
                if (alertDTO != null)
                {
                    dtoList.Add(alertDTO);
                }
            }
            return dtoList;
        }

        public AlertDTO ConvertModelToDTO(Alert alert)
        {
            AlertDTO alertDTO = new AlertDTO();

            alertDTO.ID = alert.ID;
            alertDTO.AlertType = alert.AlertType;
            alertDTO.AlertTypeID = alert.AlertTypeID;
            alertDTO.Message = alert.Message;
            alertDTO.User = alert.User;

            return alertDTO;
        }

        public Alert ConvertDTOToModel(AlertDTO alertDTO)
        {
            Alert alert = new Alert();

            alert.ID = alertDTO.ID;
            alert.AlertType = alertDTO.AlertType;
            alert.AlertTypeID = alertDTO.AlertTypeID;
            alert.Message = alertDTO.Message;
            alert.User = alertDTO.User;

            return alert;
        }

        public void UpdateAlert(Alert alert)
        {
            context.Entry(alert).State = EntityState.Modified;
        }

        public void DeleteAlert(Alert alert)
        {
            context.Alerts.Remove(alert);
            context.SaveChanges();
        }

        public void InsertAlert(Alert alert)
        {
            context.Alerts.Add(alert);
        }

        public IEnumerable<Alert> GetAlertsByUser(string userID)
        {
           return context.Alerts.Where(a => a.User.Id == userID).ToList();
        } 
    }
}