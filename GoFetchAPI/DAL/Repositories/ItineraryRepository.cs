﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public class ItineraryRepository : IItineraryRepository
    {
        public ApplicationDbContext context;

        public ItineraryRepository()
        {
            context = new ApplicationDbContext();
        }

        public Itinerary Create(Itinerary itinerary)
        {
            Itinerary it= context.Itineraries.Add(itinerary);
            context.SaveChanges();
            return it;
        }

        public bool Delete(int id)
        {
            var itinerary = context.Itineraries.Find(id);

            if (itinerary != null)
            {
                context.Itineraries.Remove(itinerary);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Itinerary> GetData()
        {
            var itinerary = context.Itineraries.ToList();
            return itinerary;
        }

        public List<Itinerary> ByUser(string userID)
        {
            List<Itinerary> ItineraryList = context.Itineraries.Where(i => i.User.Id == userID).ToList();
            return ItineraryList;
        }

        public List<Itinerary> GetDefault()
        {
            List<Itinerary> ItineraryList = context.Itineraries.Where(
                                            i => i.User.Roles.Any(r => r.RoleId == "1")).ToList();
            return ItineraryList;
        }

        public Itinerary GetData(int id)
        {
            return context.Itineraries.Find(id);
        }

        public bool Update(int id, Itinerary NewItinerary)
        {
            var itinerary = context.Itineraries.Find(id);

            if (itinerary != null)
            {
                itinerary.StartTime = NewItinerary.StartTime;
                itinerary.ID = NewItinerary.ID;
                itinerary.Type = NewItinerary.Type;
                itinerary.User = NewItinerary.User;
                itinerary.VisitList = NewItinerary.VisitList;
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<ItineraryDTO> ConvertModelListToDTO(List<Itinerary> itineraryList)
        {
            List<ItineraryDTO> dtoList = new List<ItineraryDTO>();
            foreach (Itinerary itinerary in itineraryList)
            {
                ItineraryDTO itineraryDTO = ConvertModelToDTO(itinerary);
                if (itineraryDTO != null)
                {
                    dtoList.Add(itineraryDTO);
                }
            }
            return dtoList;
        }

        public ItineraryDTO ConvertModelToDTO(Itinerary itinerary)
        {
            ItineraryDTO itineraryDTO = new ItineraryDTO();

            itineraryDTO.ID = itinerary.ID;
            itineraryDTO.StartTime = itinerary.StartTime;
            itineraryDTO.Type = itinerary.Type;
            itineraryDTO.User = itinerary.User;
            itineraryDTO.VisitList = itinerary.VisitList;

            return itineraryDTO;
        }

        public ItinerarySGRAIDTO ConvertVisitsToSGRAI(Itinerary itinerary)
        {
            ItinerarySGRAIDTO dto = new ItinerarySGRAIDTO();

            foreach(POITravel travel in itinerary.travels)
            {
                VisitSGRAIDTO visit = new VisitSGRAIDTO();

                visit.ID1 = Convert.ToString(travel.POI1ID);
                visit.ID2 = Convert.ToString(travel.POI2ID);
                
                switch(travel.typeOfTravel)
                {
                    case 1:
                        {
                            visit.Type = "Foot";
                            break;
                        }
                    case 2:
                        {
                            visit.Type = "Car";
                            break;
                        }
                }

                dto.POI.Add(visit);
            }

            return dto;
        }

        public Itinerary ConvertDTOToModel(ItineraryDTO itineraryDTO)
        {
            Itinerary itinerary = new Itinerary();

            itinerary.ID = itineraryDTO.ID;
            itinerary.StartTime = itineraryDTO.StartTime;
            itinerary.Type = itineraryDTO.Type;
            itinerary.User = itineraryDTO.User;
            itinerary.VisitList = itineraryDTO.VisitList;

            return itinerary;
        }

        public void UpdateItinerary(Itinerary itinerary)
        {
            context.Entry(itinerary).State = EntityState.Modified;
        }

        public void DeleteItinerary(Itinerary itinerary)
        {
            context.Itineraries.Remove(itinerary);
            context.SaveChanges();
        }
    }
}