﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public class ProfileRepository : IProfileRepository
    {
        public ApplicationDbContext context;

        public ProfileRepository()
        {
            context = new ApplicationDbContext();
        }

        public Profile Create(Profile profile)
        {
            context.Profiles.Add(profile);
            context.SaveChanges();
            return profile;
        }

        public bool Delete(int id)
        {
            var profile = context.Profiles.Find(id);

            if (profile != null)
            {
                context.Profiles.Remove(profile);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Profile> GetData()
        {
            var profiles = context.Profiles.ToList();
            return profiles;
        }

        public Profile GetData(int id)
        {
            return context.Profiles.Find(id);
        }

        public bool Update(int id, Profile NewProfile)
        {
            var profile = context.Profiles.Find(id);

            if (profile != null)
            {
                profile.Name = NewProfile.Name;
                profile.BirthDate = NewProfile.BirthDate;
                profile.ID = NewProfile.ID;
                profile.RestrictionList = NewProfile.RestrictionList;
                profile.User = NewProfile.User;
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddAlert(int id, Alert alert)
        {
            var Profile = context.Profiles.Find(id);

            if (Profile != null)
            {
                //Profile.AlertList.Add(alert);
                context.SaveChanges();
                return true;
            }

            return false;
        }
    }
}