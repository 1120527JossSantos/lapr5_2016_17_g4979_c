﻿using GoFetchAPI.DAL.Repository_Interfaces;
using GoFetchAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using GoFetchAPI.DTO;

namespace GoFetchAPI.DAL.Repositories
{
    public class POICategoryRepository : IPOICategoryRepository 
    {
        public ApplicationDbContext context;

        public POICategoryRepository()
        {
            context = new ApplicationDbContext();
        }

        public POICategory Create(POICategory POICategory)
        {
            context.POICategories.Add(POICategory);
            context.SaveChanges();
            return POICategory;
        }

        public bool Delete(int id)
        {
            var POICategory = context.POICategories.Find(id);

            if (POICategory != null)
            {
                context.POICategories.Remove(POICategory);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<POICategory> GetData()
        {
            var POICategories = context.POICategories.ToList();
            return POICategories;
        }

        public POICategory GetData(int id)
        {
            return context.POICategories.Find(id);
        }

        public bool Update(int id, POICategory NewPOICategory)
        {
            var POICategory = context.POICategories.Find(id);

            if (POICategory != null)
            {
                POICategory.Description = NewPOICategory.Description;
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<POICategoryDTO> ConvertModelListToDTO(List<POICategory> poiCategoryList)
        {
            List<POICategoryDTO> dtoList = new List<POICategoryDTO>();
            foreach (POICategory poiCategory in poiCategoryList)
            {
                POICategoryDTO poiCategoryDTO = ConvertModelToDTO(poiCategory);
                if (poiCategoryDTO != null)
                {
                    dtoList.Add(poiCategoryDTO);
                }
            }
            return dtoList;
        }

        public POICategoryDTO ConvertModelToDTO(POICategory poiCategory)
        {
            POICategoryDTO poiCategoryDTO = new POICategoryDTO();

            poiCategoryDTO.ID = poiCategory.ID;
            poiCategoryDTO.Description = poiCategory.Description;

            return poiCategoryDTO;
        }

        public POICategory ConvertDTOToModel(POICategoryDTO poiCategoryDTO)
        {
            POICategory poiCategory = new POICategory();

            poiCategory.ID = poiCategoryDTO.ID;
            poiCategory.Description = poiCategoryDTO.Description;

            return poiCategory;
        }

        public void UpdatePOICategory(POICategory poiCategory)
        {
            context.Entry(poiCategory).State = EntityState.Modified;
        }

        public void DeletePOICategory(POICategory poiCategory)
        {
            this.context.POICategories.Remove(poiCategory);
        }

        public void InsertPOICategory(POICategory poiCategory)
        {
            context.POICategories.Add(poiCategory);
        }
    }
}