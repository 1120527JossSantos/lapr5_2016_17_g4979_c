﻿using GoFetchAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using GoFetchAPI.DTO;
using GoFetchAPI.ViewModels;
using System.Data.Entity.Validation;
using System.Text;
using System.Globalization;

namespace GoFetchAPI.DAL
{
    public class POIRepository : IPOIRepository
    {
        public ApplicationDbContext context;

        public POIRepository()
        {
            context = new ApplicationDbContext();
        }

        public POI Create(POI POI)
        {
            context.POIs.Add(POI);
            context.SaveChanges();
            return POI;
        }

        public bool Delete(int id)
        {
            var POI = context.POIs.Find(id);

            if(POI != null)
            {
                context.POIs.Remove(POI);
                context.SaveChanges();
                return true;
            } else
            {
                return false;
            }
        }

        public List<POI> GetData(string userID)
        {
            var POIs = context.POIs.Where(p => p.IsTentative == false).ToList();

            if (userID != null) {
                List<POI> POIs2 = context.POIs.Where(p => p.ProposerID==userID).Where(p => p.IsTentative==true).ToList();
                List<POI> POIsAll = POIs2.Concat(POIs).ToList();

                return filterHashtags(POIsAll);
            }

            return filterHashtags(POIs);
        }

        private List<POI> filterHashtags(List<POI> POIs)
        {
            foreach(POI p in POIs)
            {
                p.HashtagList = p.HashtagList.Where(h => h.IsTentative == false).ToList();
            }

            return POIs;

        }

        public List<POI> GetTentative()
        {
            var POIs = context.POIs.Where(p => p.IsTentative == true).ToList();
            return POIs;
        }

        public List<POI> GetAccepted()
        {
            var POIs = context.POIs.Where(p => p.IsTentative == false).ToList();
            return POIs;
        }

        /**
         * Returns a list of POI associated with a given category.
         * 
         */
        public List<POI> GetByCategory(int id)
        {
            var POIs = context.POIs.Where(p => p.IsTentative == false).Where(p => p.CategoryID == id).ToList();
            return POIs;
        }

        /**
         * Returns a list of POI associated with a given Hashtag.
         * 
         */
        public List<POI> GetByHashtag(Hashtag hashtag)
        {
            var POIList = context.POIs.Where(p => p.IsTentative == false).ToList();
            var POIs = POIList.Where(p => p.HashtagList.Any(h => h.Label == hashtag.Label)).ToList();
            //context.HashTags.Where(h => h.Label == hashtag.Label).First()
            return POIs;
        }

        /**
         * Returns a list of POI matching a given search term.
         * 
         */
        public List<POI> GetByTerm(string term)
        {
            var POIList = context.POIs.Where(p => p.IsTentative == false).ToList();

            // case sensitive shenanigans
            var POIs = POIList.Where(f => f.Name.ToLower().Contains(term.ToLower())).ToList();
            return POIs;
        }

        public POI GetData(int id)
        {
            POI p = context.POIs.Find(id);

            if(p != null)
            {
                if (p.HashtagList!=null) { 
                 p.HashtagList = p.HashtagList.Where(h => h.IsTentative == false).ToList();
                }
                return p;
            }

            return null;
        }

        public POI ConvertSuggestPOIToModel(SuggestPOI poiDTO, string userID)
        {
            var POI = new POI();
            POI.Altitude = poiDTO.Altitude;
            POI.CategoryID = poiDTO.CategoryID;
            POI.Description = poiDTO.Description;
            POI.Latitude = poiDTO.Latitude;
            POI.Longitude = poiDTO.Longitude;
            POI.Name = poiDTO.Name;
            POI.IsTentative = true;
            POI.VisitTime = poiDTO.VisitTime;
            POI.ProposerID = userID;
            return POI;
        }

        public bool Update(int id, POI NewPOI)
        {
            var POI = context.POIs.Find(id);
            var j = 0;
            if (POI != null)
            {

                POI.Latitude = NewPOI.Latitude;

                POI.ImageList = NewPOI.ImageList;
                POI.Longitude = NewPOI.Longitude;
                POI.Name = NewPOI.Name;
                foreach(Restriction rest in NewPOI.RestrictionList)
                {
                    var toAdd = context.Restrictions.Find(rest.Code);
                    POI.RestrictionList.Add(toAdd);
                }
              
                POI.VisitTime = NewPOI.VisitTime;
                POI.CategoryID = NewPOI.CategoryID;
                POI.WorkSchedule = NewPOI.WorkSchedule;
                POI.Description = NewPOI.Description;
                SaveChanges(context);
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void SaveChanges(DbContext context)
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                
            }
        }

        public bool AddHashtag(int id, string Label)
        {
            var POI = context.POIs.Find(id);

            if(POI != null)
            {
                int exists = POI.HashtagList.Where(h => h.Label == Label).Count();
                if (exists > 0)
                    return false;

                Hashtag hashtag = new Hashtag();
                //hashtag.ID = context.HashTags.Count() + 1;
                hashtag.Label = Label;
                //hashtag.POIID = POI.ID;

                context.HashTags.Add(hashtag);
                //context.SaveChanges();

                POI.HashtagList.Add(hashtag);
                context.SaveChanges();
                return true;
            }

            return false;
        }

        /**
         * Checks if an Hashtags already associated with a POI
         * 
         */
        public bool HasHashtag(int ID, string Label)
        {
            POI poi = context.POIs.Find(ID);
            if (poi == null)
            {
                return true;
            }

            var asd = poi.HashtagList.ToList();
            int exists = poi.HashtagList.Where(h => h.Label == Label).Count();
            return (exists > 0);
        }

        public bool Accept(int id)
        {
            var POI = context.POIs.Find(id);

            if(POI != null)
            {
                POI.IsTentative = false;
                context.SaveChanges();
                return true;
            }

            return false;
        }

        public bool AddSchedule(int id, List<Schedule> schedule)
        {
            var POI = context.POIs.Find(id);

            if(POI != null)
            {
                POI.WorkSchedule = schedule;
                context.SaveChanges();
                return true;
            }

            return false;
        }

        public List<POIDTO> ConvertModelListToDTO(List<POI> POIList)
        {
            List<POIDTO> dtoList = new List<POIDTO>();
            foreach (POI poi in POIList)
            {
                POIDTO poiDTO = ConvertModelToDTO(poi);
                if (poiDTO != null)
                {
                    dtoList.Add(poiDTO);
                }
            }
            return dtoList;
        }

        public POIDTO ConvertModelToDTO(POI poi)
        {
            POIDTO poiDTO = new POIDTO();

            poiDTO.ID = poi.ID;
            poiDTO.Category = poi.Category;
            poiDTO.CategoryID = poi.CategoryID;
            poiDTO.Description = poi.Description;
            poiDTO.HashtagList = poi.HashtagList;
            poiDTO.ImageList = poi.ImageList;
            poiDTO.IsTentative = poi.IsTentative;
            poiDTO.Latitude = poi.Latitude;
            poiDTO.Longitude = poi.Longitude;
            poiDTO.Name = poi.Name;
            poiDTO.RestrictionList = poi.RestrictionList;
            poiDTO.VisitTime = poi.VisitTime;
            poiDTO.WorkSchedule = poi.WorkSchedule;
            poiDTO.Proponente = poi.Proposer;

            return poiDTO;
        }

        public POIListSGRAIDTO ConvertModelListToDTO_SGRAI(List<POI> pois)
        {
            List<POISGRAIDTO> dtoList = new List<POISGRAIDTO>();

            foreach (POI poi in pois)
            {
                POISGRAIDTO dto = ConvertModelToSGRAI(poi);
                if (dto != null)
                {
                    dtoList.Add(dto);
                }
            }

            List<POIConnectionSGRAIDTO> conList = new List<POIConnectionSGRAIDTO>();
            List<POIConnection> connections = context.POIConnections.ToList();

            foreach(POIConnection con in connections)
            {
                POIConnectionSGRAIDTO dto = ConvertConnectionToSGRAI(con);
                if(dto != null)
                {
                    conList.Add(dto);
                }
            }

            POIListSGRAIDTO result = new POIListSGRAIDTO();
            result.POI = dtoList;
            result.Connection = conList;
            return result;
        }

        public POISGRAIDTO ConvertModelToSGRAI(POI poi)
        {
            POISGRAIDTO dto = new POISGRAIDTO();

            dto.ID = Convert.ToString(poi.ID);
            dto.Name = RemoveDiacritics(poi.Name);
            dto.Description = RemoveDiacritics(poi.Description);
            dto.VisitTime = Convert.ToString(poi.VisitTime);
            dto.Latitude = poi.Latitude;
            dto.Longitude = poi.Longitude;
            dto.Altitude = poi.Altitude;

            return dto;
        }

        private string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public POIConnectionSGRAIDTO ConvertConnectionToSGRAI(POIConnection connection)
        {
            POIConnectionSGRAIDTO dto = new POIConnectionSGRAIDTO();

            dto.POI1ID = Convert.ToString(connection.POI1ID);
            dto.POI2ID = Convert.ToString(connection.POI2ID);
            dto.Distance = Convert.ToString(connection.Distance);

            return dto;
        }

        /**
         * Returns a DTO to send to the ALGAV application.
         * 
         */
        public POIALGAVDTO ConvertModelToALGAVDTO(POIAcceptionViewModel vm)
        {
            POIALGAVDTO dto = new POIALGAVDTO();

            dto.id = vm.POI.ID;
            dto.tempoestimativavisita = vm.POI.VisitTime;
            dto.ligacoes = ConvertPOIConnectionsToDTO(vm.Connections);
            dto.disponibilidade = ConvertPOIScheduleToDTO(vm.WorkSchedule);

            return dto;
        }

        /**
         * Converts connections array to an integer matrix to be sent as DTO
         * 
         */
        private List<List<int>> ConvertPOIConnectionsToDTO(List<POIConnection> connections)
        {
            List<List<int>> returnArray = new List<List<int>>();
            List<POIConnection> addedConnections = new List<POIConnection>();

            foreach(var connection in connections)
            {
                List<int> conn = new List<int>();

                bool connExists = addedConnections.Where(c => c.POI1ID == connection.POI2ID).Where(c => c.POI2ID == connection.POI1ID).Count() > 0;
                if (!connExists)
                {
                    conn.Add(connection.POI1ID);
                    conn.Add(connection.POI2ID);
                    conn.Add(connection.Distance);

                    bool bid = (connections.Where(c => c.POI1ID == connection.POI2ID).Where(c => c.POI2ID == connection.POI1ID).Count() > 0);
                    if (bid)
                    {
                        conn.Add(1);
                    }
                    else
                    {
                        conn.Add(0);
                    }

                    addedConnections.Add(new POIConnection { POI1ID = connection.POI1ID, POI2ID = connection.POI2ID });
                    returnArray.Add(conn);
                }
            }
            return returnArray;
        }

        /**
         * Converts work schedule array to an integer matrix to be sent as DTO 
         * 
         * A Day is made up of blocks, with 15 minutes each
         * Total Blocks per Day: 64 
         */
        private List<List<int>> ConvertPOIScheduleToDTO(List<Schedule> schedule)
        {
            List<List<int>> returnArray = new List<List<int>>();

            foreach(var day in schedule)
            {
                // check for closed days
                if (day.Open == "" || day.Close == "")
                    break;

                // parse Open time
                string[] actualTimeOpen = day.Open.Split(':');
                int multiplierOpen = int.Parse( actualTimeOpen[1] ) / 15;
                double minutesOpen = multiplierOpen * 0.25;
                int blockOpen = Convert.ToInt32( 4 * (int.Parse(actualTimeOpen[0]) + minutesOpen - 8) );

                // parse Close tim
                string[] actualTimeClose = day.Close.Split(':');
                int multiplierClose= int.Parse(actualTimeClose[1]) / 15;
                double minutesClose = multiplierClose * 0.25;
                int blockClose = Convert.ToInt32( 4 * (int.Parse(actualTimeClose[0]) + minutesClose - 8) );

                // store Blocks
                List<int> actualDay = new List<int>();
                for(int j = blockOpen; j <= blockClose; j++)
                {
                    actualDay.Add(j);
                }

                // add to result array
                returnArray.Add(actualDay);
            }

            return returnArray;
        }

        public POI ConvertDTOToModel(POIDTO poiDTO)
        {
            POI poi = new POI();

            poi.ID = poiDTO.ID;
            poi.Category = poiDTO.Category;
            poi.CategoryID = poiDTO.CategoryID;
            poi.Description = poiDTO.Description;
            poi.HashtagList = poiDTO.HashtagList;
            poi.ImageList = poiDTO.ImageList;
            poi.IsTentative = poiDTO.IsTentative;
            poi.Latitude = poiDTO.Latitude;
            poi.Longitude = poiDTO.Longitude;
            poi.Name = poiDTO.Name;
            poi.RestrictionList = poiDTO.RestrictionList;
            poi.VisitTime = poiDTO.VisitTime;
            poi.WorkSchedule = poiDTO.WorkSchedule;
            poi.Proposer = poiDTO.Proponente;
            poi.ProposerID = poiDTO.Proponente.Id;
            return poi;
        }

        public void UpdatePOI(int id, POI poi)
        {
            POI toUpdate = context.POIs.Find(id);

            toUpdate.Name = poi.Name;
            toUpdate.Description = poi.Description;
            toUpdate.VisitTime = poi.VisitTime;
            toUpdate.Latitude = poi.Latitude;
            toUpdate.Longitude = poi.Longitude;
            toUpdate.Altitude = poi.Altitude;
            toUpdate.CategoryID = poi.CategoryID;

            context.SaveChanges();
        }

        public void DeletePOI(POI poi)
        {
            context.POIs.Remove(poi);
            context.SaveChanges();
        }
    }
}