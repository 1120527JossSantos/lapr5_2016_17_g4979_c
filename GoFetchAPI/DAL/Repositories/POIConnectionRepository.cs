﻿using GoFetchAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using GoFetchAPI.DTO;
using GoFetchAPI.ViewModels;

namespace GoFetchAPI.DAL.Repositories
{
    public class POIConnectionRepository : IPOIConnectionRepository
    {
        public ApplicationDbContext context;

        public POIConnectionRepository()
        {
            context = new ApplicationDbContext();
        }

        public POIConnection Create(POIConnection connection)
        {
            context.POIConnections.Add(connection);
            context.SaveChanges();
            return connection;
        }

        public List<POIConnection> CreateMultipleConnections(List<POIConnection> connections)
        {
            foreach (POIConnection con in connections)
            {
                context.POIConnections.Add(con);
               
            }
            context.SaveChanges();
            return connections;
        }

        public bool Delete(int id)
        {
            var connection = context.POIConnections.Find(id);

            if (connection != null)
            {
                context.POIConnections.Remove(connection);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<POIConnection> GetData()
        {
            var connectionsList = context.POIConnections.ToList();
            return connectionsList;
        }

        public List<POIConnection> GetDataFrom(int id)
        {
            return context.POIConnections.Where(p => p.POI1ID == id).ToList();

        }
        public List<POIConnection> GetDataTo(int id)
        {
            return context.POIConnections.Where(p => p.POI2ID == id).ToList();

        }

        public bool Update(int id, POIConnection newConnection)
        {
            var connection = context.POIConnections.Find(id);

            if (connection != null)
            {
                POI from = context.POIs.Find(newConnection.POI1ID);
                POI to = context.POIs.Find(newConnection.POI2ID);

                connection.POI1ID = newConnection.POI1ID;
                connection.POI2ID = newConnection.POI2ID;

                connection.POI1 = from;
                connection.POI2 = to;

                connection.Distance = newConnection.Distance;
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}