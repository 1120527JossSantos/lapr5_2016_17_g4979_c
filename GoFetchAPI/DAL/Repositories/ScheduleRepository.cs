﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public class ScheduleRepository : IScheduleRepository
    {
        public ApplicationDbContext context;

        public ScheduleRepository()
        {
            context = new ApplicationDbContext();
        }

        public Schedule Create(Schedule schedule)
        {
            context.Schedules.Add(schedule);
            context.SaveChanges();
            return schedule;
        }

        public bool Delete(int id)
        {
            var schedule = context.Schedules.Find(id);

            if (schedule != null)
            {
                context.Schedules.Remove(schedule);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Schedule> GetData()
        {
            var schedule = context.Schedules.ToList();
            return schedule;
        }

        public Schedule GetData(int id)
        {
            return context.Schedules.Find(id);
        }

        public bool Update(int id, Schedule NewSchedule)
        {
            var schedule = context.Schedules.Find(id);

            if (schedule != null)
            {
                schedule.Day = NewSchedule.Day;
                schedule.Open = NewSchedule.Open;
                schedule.Close = NewSchedule.Close;
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<ScheduleDTO> ConvertModelListToDTO(List<Schedule> ScheduleList)
        {
            List<ScheduleDTO> dtoList = new List<ScheduleDTO>();
            foreach (Schedule schedule in ScheduleList)
            {
                ScheduleDTO scheduleDTO = ConvertModelToDTO(schedule);
                if (scheduleDTO != null)
                {
                    dtoList.Add(scheduleDTO);
                }
            }
            return dtoList;
        }

        public ScheduleDTO ConvertModelToDTO(Schedule schedule)
        {
            ScheduleDTO scheduleDTO = new ScheduleDTO();

            scheduleDTO.POI = schedule.POI;
            scheduleDTO.POIID = schedule.POIID;
            scheduleDTO.Open = schedule.Open;
            scheduleDTO.Close = schedule.Close;
            scheduleDTO.Day = schedule.Day;

            return scheduleDTO;
        }

        public Schedule ConvertDTOToModel(ScheduleDTO scheduleDTO)
        {
            Schedule schedule = new Schedule();

            schedule.POI = scheduleDTO.POI;
            schedule.POIID = scheduleDTO.POIID;
            schedule.Open = scheduleDTO.Open;
            schedule.Close = scheduleDTO.Close;
            schedule.Day = scheduleDTO.Day;

            return schedule;
        }

        public void UpdateSchedule(Schedule schedule)
        {
            context.Entry(schedule).State = EntityState.Modified;
        }

        public void DeleteSchedule(Schedule schedule)
        {
            this.context.Schedules.Remove(schedule);
        }

        public void InsertSchedule(Schedule schedule)
        {
            context.Schedules.Add(schedule);
        }
    }
}