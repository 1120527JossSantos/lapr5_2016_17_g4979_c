﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public class VisitRepository : IVisitRepository
    {
        public ApplicationDbContext context;

        public VisitRepository()
        {
            context = new ApplicationDbContext();
        }

        public Visit Create(Visit visit)
        {
            context.Visits.Add(visit);
            context.SaveChanges();
            return visit;
        }

        public bool Delete(int id)
        {
            var visit = context.Visits.Find(id);

            if (visit != null)
            {
                context.Visits.Remove(visit);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Visit> GetData()
        {
            var visit = context.Visits.ToList();
            return visit;
        }

        public Visit GetData(int id)
        {
            return context.Visits.Find(id);
        }

        public bool Update(int id, Visit NewVisit)
        {
            var visit = context.Visits.Find(id);

            if (visit != null)
            {
                visit.ID = NewVisit.ID;
                visit.POI = NewVisit.POI;
                visit.StartTime = NewVisit.StartTime;
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<VisitDTO> ConvertModelListToDTO(List<Visit> VisitList)
        {
            List<VisitDTO> dtoList = new List<VisitDTO>();
            foreach (Visit visit in VisitList)
            {
                VisitDTO visitDTO = ConvertModelToDTO(visit);
                if (visitDTO != null)
                {
                    dtoList.Add(visitDTO);
                }
            }
            return dtoList;
        }

        public VisitDTO ConvertModelToDTO(Visit visit)
        {
            VisitDTO visitDTO = new VisitDTO();

            visitDTO.ID = visit.ID;
            visitDTO.POI = visit.POI;
            visitDTO.POIID = visit.POIID;
            visitDTO.StartTime = visit.StartTime;

            return visitDTO;
        }

        public Visit ConvertDTOToModel(VisitDTO visitDTO)
        {
            Visit visit = new Visit();

            visit.ID = visitDTO.ID;
            visit.POI = visitDTO.POI;
            visit.POIID = visitDTO.POIID;
            visit.StartTime = visitDTO.StartTime;

            return visit;
        }

        public void UpdateVisit(Visit visit)
        {
            context.Entry(visit).State = EntityState.Modified;
        }

        public void DeleteVisit(Visit visit)
        {
            this.context.Visits.Remove(visit);
        }

        public void InsertVisit(Visit visit)
        {
            context.Visits.Add(visit);
        }
    }
}