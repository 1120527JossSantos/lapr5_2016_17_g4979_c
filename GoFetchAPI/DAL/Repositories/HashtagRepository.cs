﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public class HashtagRepository : IHashtagRepository
    {
        public ApplicationDbContext context;

        public HashtagRepository()
        {
            context = new ApplicationDbContext();
        }

        public Hashtag Create(string Label)
        {
            Hashtag hashtag = new Hashtag();
            hashtag.Label = Label;
            context.HashTags.Add(hashtag);
         
            return hashtag;
        }

        public Hashtag CreateAndAdd(int ID, string Label)
        {
            Hashtag hashtag = new Hashtag();
            hashtag.POIID = ID;
            hashtag.Label = Label;
            context.HashTags.Add(hashtag);
            context.SaveChanges();

            return hashtag;
        }

        public bool IsInPOI(string Label, int ID)
        {
            int exists = context.HashTags.Where(h => h.Label == Label).Where(h => h.POIID == ID).Count();
            return (exists > 0);
        }

        public bool Delete(int id)
        {
            var hashtag = context.HashTags.Find(id);

            if (hashtag != null)
            {
                context.HashTags.Remove(hashtag);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Hashtag> GetData()
        {
            var hashtag = context.HashTags.Where(h => h.IsTentative == false).ToList();
            return hashtag;
        }

        public List<Hashtag> GetTentative()
        {
            var hashtags = context.HashTags.Where(h => h.IsTentative == true).ToList();
            return hashtags;
        }

        public bool Accept(int id)
        {
            var hashtag = context.HashTags.Find(id);
            if(hashtag == null)
            {
                return false;
            }

            hashtag.IsTentative = false;
            context.SaveChanges();
            return true;
        }

        public Hashtag GetData(int ID)
        {
            return context.HashTags.Find(ID);
        }

        public Hashtag ByLabel(string Label)
        {
            return context.HashTags.Where(h => h.Label == Label).FirstOrDefault();
        }

        public bool Update(int id, Hashtag NewHashtag)
        {
            var hashtag = context.HashTags.Find(id);

            if (hashtag != null)
            {
                hashtag.Label = NewHashtag.Label;
                hashtag.IsTentative = NewHashtag.IsTentative;
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<HashtagDTO> ConvertModelListToDTO(List<Hashtag> hashtagList)
        {
            List<HashtagDTO> dtoList = new List<HashtagDTO>();
            foreach (Hashtag hashtag in hashtagList)
            {
                HashtagDTO hashtagDTO = ConvertModelToDTO(hashtag);
                if (hashtagDTO != null)
                {
                    dtoList.Add(hashtagDTO);
                }
            }
            return dtoList;
        }

        public HashtagDTO ConvertModelToDTO(Hashtag hashtag)
        {
            if (hashtag.Label == null)
            {
                return null;
            }

            HashtagDTO hashtagDTO = new HashtagDTO();

            hashtagDTO.Label = hashtag.Label;
            hashtagDTO.isTentative = hashtag.IsTentative;

            return hashtagDTO;
        }

        public Hashtag ConvertDTOToModel(HashtagDTO hashtagDTO)
        {
            Hashtag hashtag = new Hashtag();

            hashtag.Label = hashtagDTO.Label;
            hashtag.IsTentative = hashtagDTO.isTentative;

            return hashtag;
        }

        public void UpdateHashtag(Hashtag hashtag)
        {
            context.Entry(hashtag).State = EntityState.Modified;
        }

        public void DeleteHashtag(Hashtag hashtag)
        {
            context.HashTags.Remove(hashtag);
            context.SaveChanges();
        }

        public void InsertHashtag(Hashtag hashtag)
        {
            context.HashTags.Add(hashtag);
        }
    }
}