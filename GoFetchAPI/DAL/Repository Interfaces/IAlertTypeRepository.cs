﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL.Repository_Interfaces
{
    public interface IAlertTypeRepository
    {
        List<AlertType> GetData();
        AlertType GetData(int id);
        AlertType Create(AlertType alertType);
        bool Update(int id, AlertType alertType);
        bool Delete(int id);
    }
}
