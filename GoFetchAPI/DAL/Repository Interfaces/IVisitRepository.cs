﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public interface IVisitRepository
    {
        List<Visit> GetData();
        Visit GetData(int id);
        Visit Create(Visit visit);
        bool Update(int id, Visit visit);
        bool Delete(int id);
        List<VisitDTO> ConvertModelListToDTO(List<Visit> visitList);
        VisitDTO ConvertModelToDTO(Visit visit);
        Visit ConvertDTOToModel(VisitDTO visitDTO);
        void UpdateVisit(Visit visit);
        void DeleteVisit(Visit visit);
        void InsertVisit(Visit visit);
    }
}
