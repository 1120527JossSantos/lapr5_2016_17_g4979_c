﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public interface IHashtagRepository
    {
        List<Hashtag> GetData();
        List<Hashtag> GetTentative();
        Hashtag GetData(int ID);
        Hashtag ByLabel(string Label);
        Hashtag Create(string Label);
        Hashtag CreateAndAdd(int ID, string Label);
        bool IsInPOI(string Label, int ID);
        bool Update(int id, Hashtag hashtag);
        bool Accept(int id);
        bool Delete(int id);
        List<HashtagDTO> ConvertModelListToDTO(List<Hashtag> hashtagList);
        HashtagDTO ConvertModelToDTO(Hashtag hashtag);
        Hashtag ConvertDTOToModel(HashtagDTO hashtagDTO);
        void UpdateHashtag(Hashtag hashtag);
        void DeleteHashtag(Hashtag hashtag);
        void InsertHashtag(Hashtag hashtag);
    }
}
