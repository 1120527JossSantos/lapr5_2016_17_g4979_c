﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public interface IItineraryRepository
    {
        List<Itinerary> GetData();
        ItinerarySGRAIDTO ConvertVisitsToSGRAI(Itinerary itinerary);
        List<Itinerary> ByUser(string userID);
        List<Itinerary> GetDefault();
        Itinerary GetData(int id);
        Itinerary Create(Itinerary itinerary);
        bool Update(int id, Itinerary itinerary);
        bool Delete(int id);
        List<ItineraryDTO> ConvertModelListToDTO(List<Itinerary> itineraryList);
        ItineraryDTO ConvertModelToDTO(Itinerary itinerary);
        Itinerary ConvertDTOToModel(ItineraryDTO itineraryDTO);
        void UpdateItinerary(Itinerary itinerary);
        void DeleteItinerary(Itinerary itinerary);
    }
}
