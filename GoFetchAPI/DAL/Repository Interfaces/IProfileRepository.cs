﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public interface IProfileRepository
    {
        List<Profile> GetData();
        Profile GetData(int id);
        Profile Create(Profile profile);
        bool Update(int id, Profile profile);
        bool Delete(int id);
        bool AddAlert(int id, Alert alert);
    }
}
