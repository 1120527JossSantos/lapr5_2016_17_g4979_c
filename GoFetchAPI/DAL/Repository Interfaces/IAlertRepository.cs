﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public interface IAlertRepository
    {
        List<Alert> GetData();
        Alert GetData(int id);
        Alert Create(Alert alert);
        bool Update(int id, Alert alert);
        bool Delete(int id);
        bool AddUserMessageAlert(ApplicationUser user, string message,int alertTypeID);
        List<AlertDTO> ConvertModelListToDTO(List<Alert> alertList);
        AlertDTO ConvertModelToDTO(Alert alert);
        Alert ConvertDTOToModel(AlertDTO alertDTO);
        void UpdateAlert(Alert alert);
        void DeleteAlert(Alert alert);
        void InsertAlert(Alert alert);
        IEnumerable<Alert> GetAlertsByUser(string token);
    }
}
