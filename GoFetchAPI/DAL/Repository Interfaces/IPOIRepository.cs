﻿using GoFetchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.DTO;

namespace GoFetchAPI.DAL
{
    public interface IPOIRepository
    {
       
        List<POI> GetTentative();
        List<POI> GetAccepted();
        POIListSGRAIDTO ConvertModelListToDTO_SGRAI(List<POI> pois);
        POISGRAIDTO ConvertModelToSGRAI(POI poi);
        POIConnectionSGRAIDTO ConvertConnectionToSGRAI(POIConnection connection);
        POI GetData(int id);

        List<POI> GetData(string userID);
        POI Create(POI POI);
        bool Update(int id, POI POI);
        bool Delete(int id);


        // Add new Hashtag to a POI
        bool AddHashtag(int id, string Label);
        bool HasHashtag(int ID, string Label);
        bool Accept(int id);

        // POI filtering methods
        List<POI> GetByCategory(int id);
        List<POI> GetByHashtag(Hashtag hashtag);
        List<POI> GetByTerm(string term);

        List<POIDTO> ConvertModelListToDTO(List<POI> poiList);
        POIDTO ConvertModelToDTO(POI poi);
        POI ConvertDTOToModel(POIDTO poiDTO);

        POI ConvertSuggestPOIToModel(SuggestPOI poiDTO,string userID);
        void UpdatePOI(int id, POI poi);
        void DeletePOI(POI poi);
    }
}
