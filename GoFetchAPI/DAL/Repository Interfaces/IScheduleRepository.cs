﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.DTO;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public interface IScheduleRepository
    {
        List<Schedule> GetData();
        Schedule GetData(int id);
        Schedule Create(Schedule schedule);
        bool Update(int id, Schedule schedule);
        bool Delete(int id);
        List<ScheduleDTO> ConvertModelListToDTO(List<Schedule> scheduleList);
        ScheduleDTO ConvertModelToDTO(Schedule schedule);
        Schedule ConvertDTOToModel(ScheduleDTO scheduleDTO);
        void UpdateSchedule(Schedule schedule);
        void DeleteSchedule(Schedule schedule);
        void InsertSchedule(Schedule schedule);
    }
}
