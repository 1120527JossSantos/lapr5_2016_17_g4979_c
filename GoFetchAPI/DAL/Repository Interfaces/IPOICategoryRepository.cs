﻿using GoFetchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.DTO;

namespace GoFetchAPI.DAL.Repository_Interfaces
{
    interface IPOICategoryRepository
    {
        List<POICategory> GetData();
        POICategory GetData(int id);
        POICategory Create(POICategory Category);
        bool Update(int id, POICategory Category);
        bool Delete(int id);

        List<POICategoryDTO> ConvertModelListToDTO(List<POICategory> poiCategoryList);
        POICategoryDTO ConvertModelToDTO(POICategory poiCategory);
        POICategory ConvertDTOToModel(POICategoryDTO poiCategoryDTO);
        void UpdatePOICategory(POICategory poiCategory);
        void DeletePOICategory(POICategory poiCategory);
        void InsertPOICategory(POICategory poiCategory);
    }
}
