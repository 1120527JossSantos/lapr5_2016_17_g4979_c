﻿using System;
using System.Collections.Generic;
using GoFetchAPI.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.DAL
{
    public interface IPOIConnectionRepository
    {

        List<POIConnection> GetData();
        List<POIConnection> GetDataFrom(int id);
        List<POIConnection> GetDataTo(int id);
        POIConnection Create(POIConnection connection);
        bool Update(int id, POIConnection connection);
        bool Delete(int id);
    }
}
