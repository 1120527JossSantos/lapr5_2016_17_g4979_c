﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL
{
    public interface IRestrictionRepository
    {
        List<Restriction> GetData();
        Restriction GetData(string id);
        Restriction Create(Restriction restriction);
        bool Update(string id, Restriction restriction);
        bool Delete(string id);
    }
}
