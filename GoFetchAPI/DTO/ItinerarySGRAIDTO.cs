﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoFetchAPI.DTO
{
    public class ItinerarySGRAIDTO
    {
        public List<VisitSGRAIDTO> POI { get; set; } 

        public ItinerarySGRAIDTO()
        {
            this.POI = new List<VisitSGRAIDTO>();
        }
    }
}