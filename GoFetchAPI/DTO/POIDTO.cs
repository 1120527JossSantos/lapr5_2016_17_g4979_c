﻿using GoFetchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.DTO
{
    public class POIDTO
    {
        public int ID { get; set; }

       
        public string Name { get; set; }

        
        public string Description { get; set; }


        public int VisitTime { get; set; }

     
        public bool IsTentative { get; set; }

        
        public string Latitude { get; set; }

       
        public string Longitude { get; set; }

    
        public int CategoryID { get; set; }
        public virtual POICategory Category { get; set; }

        public virtual List<Schedule> WorkSchedule { get; set; }

        public virtual List<Hashtag> HashtagList { get; set; }

        public virtual List<Restriction> RestrictionList { get; set; }

        public virtual List<Image> ImageList { get; set; }

        public ApplicationUser Proponente { get; set; }

    }
}

