﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoFetchAPI.DTO
{
    public class POIALGAVDTO
    {
        public int id { get; set; }

        public int tempoestimativavisita { get; set; }

        public List<List<int>> ligacoes { get; set; }

        public List<List<int>> disponibilidade { get; set; }

    }
}