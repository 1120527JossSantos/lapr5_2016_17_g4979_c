﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoFetchAPI.Models;

namespace GoFetchAPI.DTO
{
    public class VisitDTO
    {
        public int ID { get; set; }

        public int POIID { get; set; }

        public virtual POI POI { get; set; }

        public DateTime StartTime { get; set; }
    }
}