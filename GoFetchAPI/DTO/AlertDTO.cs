﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoFetchAPI.Models;

namespace GoFetchAPI.DTO
{
    public class AlertDTO
    {
        public int ID { get; set; }

        public int AlertTypeID { get; set; }
        
        public virtual AlertType AlertType { get; set; }

        public virtual ApplicationUser User { get; set; }

        public string Message { get; set; }
    }
}