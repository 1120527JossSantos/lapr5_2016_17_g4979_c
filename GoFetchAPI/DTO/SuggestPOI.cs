﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.DTO
{
    public class SuggestPOI
    {
        public string Name { get; set; }

        public string Description { get; set; }
        public int VisitTime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Altitude { get; set; }
        public int CategoryID { get; set; }
    }
}
