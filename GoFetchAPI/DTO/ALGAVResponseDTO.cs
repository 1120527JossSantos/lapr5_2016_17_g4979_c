﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.DTO
{
    public class ALGAVResponseDTO
    {
        public List<int> route { get; set; }

        public List<int> dispo { get; set; }

        public List<List<int>>  typeOfTravel { get; set; }
    }
}
