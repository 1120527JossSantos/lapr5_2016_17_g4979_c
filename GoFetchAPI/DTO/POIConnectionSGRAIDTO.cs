﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoFetchAPI.DTO
{
    public class POIConnectionSGRAIDTO
    {
        public string POI1ID { get; set; }
        public string POI2ID { get; set; }
        public string Distance { get; set; }
    }
}