﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoFetchAPI.Models;

namespace GoFetchAPI.DTO
{
    public class HashtagDTO
    {
        public string Label { get; set; }

        public bool isTentative { get; set; }

        public virtual List<POI> POIList { get; set; } 
    }
}