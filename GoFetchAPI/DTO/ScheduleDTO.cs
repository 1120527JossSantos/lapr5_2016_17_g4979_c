﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoFetchAPI.Models;

namespace GoFetchAPI.DTO
{
    public class ScheduleDTO
    {
        public int Day { get; set; }

        public int POIID { get; set; }

        public virtual POI POI { get; set; }

        public string Open { get; set; }

        public string Close { get; set; }
    }
}