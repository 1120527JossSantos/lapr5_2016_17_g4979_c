﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.DTO
{
    public class ALGAVItineraryRequestWithDuration : ALGAVItineraryRequest
    {
        public int duracao { get; set; }
    }
}
