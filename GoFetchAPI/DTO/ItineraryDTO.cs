﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoFetchAPI.Models;

namespace GoFetchAPI.DTO
{
    public class ItineraryDTO
    {
        public int ID { get; set; }

        public virtual ApplicationUser User { get; set; }

        public DateTime StartTime { get; set; }

        public string Type { get; set; }

        public virtual List<Visit> VisitList { get; set; }
    }
}