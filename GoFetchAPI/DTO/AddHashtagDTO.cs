﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoFetchAPI.DTO
{
    public class AddHashtagDTO
    {
        public int ID { get; set; }

        public string Label { get; set; }
    }
}