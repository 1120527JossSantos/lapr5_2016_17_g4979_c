﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoFetchAPI.DTO
{
    public class POISGRAIDTO
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string VisitTime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Altitude { get; set; }
    }
}