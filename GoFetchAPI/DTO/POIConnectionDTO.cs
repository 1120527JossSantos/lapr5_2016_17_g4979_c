﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoFetchAPI.Models;

namespace GoFetchAPI.DTO
{
    public class POIConnectionDTO
    {

        public int POI1ID { get; set; }
        public virtual POI POI1 { get; set; }

        public int POI2ID { get; set; }
        public virtual POI POI2 { get; set; }

        public int Distance { get; set; }
    }
}