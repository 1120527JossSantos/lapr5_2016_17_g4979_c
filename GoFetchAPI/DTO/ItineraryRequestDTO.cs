﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.DTO
{
    public class ItineraryRequestDTO
    {
        public string startTime { get; set; }

        public string startDate { get; set; }

        public string  endTime { get; set; }

        public int type { get; set; }

        public int wayOfTransportation { get; set; }

        public List<int> pois { get; set; }
    }
}
