﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoFetchAPI.DTO
{
    public class POIListSGRAIDTO
    {
        public List<POISGRAIDTO> POI { get; set; }

        public List<POIConnectionSGRAIDTO> Connection { get; set; }
    }
}