﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFetchAPI.DTO
{
    public class ALGAVItineraryRequest
    {
        public List<int> pois { get; set; }

        public List<int> disponibilidadeTurista { get; set; }

        public int transporte { get; set; }

        public int diadasemana { get; set; } 
    }
}
