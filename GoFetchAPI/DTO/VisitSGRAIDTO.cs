﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoFetchAPI.DTO
{
    public class VisitSGRAIDTO
    {
        public string ID1 { get; set; }
        public string ID2 { get; set; }
        public string Type { get; set; }
    }
}