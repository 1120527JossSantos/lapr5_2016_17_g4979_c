namespace GoFetchAPI.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<GoFetchAPI.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "GoFetchAPI.Models.ApplicationDbContext";
            SetSqlGenerator("MySql.Data.MySqlClient", new MySql.Data.Entity.MySqlMigrationSqlGenerator());
            SetHistoryContextFactory("MySql.Data.MySqlClient", (conn, schema) => new MySqlHistoryContext(conn, schema)); //here s the thing.
        }

        protected override void Seed(GoFetchAPI.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            
            // Categories seeding
            context.POICategories.AddOrUpdate(
                c => c.ID,
                new POICategory { ID = 1, Description = "Historic" },
                new POICategory { ID = 2, Description = "Scientific" },
                new POICategory { ID = 3, Description = "Cultural" },
                new POICategory { ID = 4, Description = "Sports" },
                new POICategory { ID = 5, Description = "Commerce" },
                new POICategory { ID = 6, Description = "Entertainment" },
                new POICategory { ID = 7, Description = "Fitness & Health" }
            );

            // POIs seeding
            context.POIs.AddOrUpdate(
                p => p.ID,
                new POI { ID = 1, Name = "S�o Bento", Description = "S�o Bento - Train Station", VisitTime = 30, Latitude = "41.14561", Longitude = "-8.610535", Altitude = "57", CategoryID = 1,IsTentative=false },
                new POI { ID = 2, Name = "Torre dos Cl�rigos", Description = "The famous Clerigos Tower, watching over the whole city!", VisitTime = 30, Latitude = "41.14783", Longitude = "-8.61464", Altitude = "83", CategoryID = 1, IsTentative = false },
                new POI { ID = 3, Name = "Trindade", Description = "Central subway station of Porto", VisitTime = 30, Latitude = "41.152283", Longitude = "-8.609301", Altitude = "94", CategoryID = 1, IsTentative = false },
                new POI { ID = 4, Name = "Pra�a da Rep�blica", Description = "Luxious garden in the center of the city", VisitTime = 30, Latitude = "41.154565", Longitude = "-8.612494", Altitude = "115", CategoryID = 1, IsTentative = false },
                new POI { ID = 5, Name = "Rotunda da Boavista", Description = "Roundabout with the famous spire representing the Portuguese victory", VisitTime = 30, Latitude = "41.157978", Longitude = "-8.6229117", Altitude = "87", CategoryID = 1, IsTentative = false },
                new POI { ID = 6, Name = "Planet�rio do Porto", Description = "Museum of astronomy", VisitTime = 30, Latitude = "41.150716", Longitude = "-8.638462", Altitude = "63", CategoryID = 2, IsTentative = false },
                new POI { ID = 7, Name = "ISEP", Description = "Porto's magnificent engineering institute", VisitTime = 50, Latitude = "41.177846", Longitude = "-8.608105", Altitude = "107", CategoryID = 2, IsTentative = false },
                new POI { ID = 8, Name = "Pavilh�o da �gua", Description = "Museum depicting the history and properties of water", VisitTime = 75, Latitude = "41.171402", Longitude = "-8.676538", Altitude = "28", CategoryID = 2, IsTentative = false },
                new POI { ID = 9, Name = "Museu Nacional de Soares dos Reis", Description = "National museum of Art", VisitTime = 40, Latitude = "41.147715", Longitude = "-8.621639", Altitude = "84", CategoryID = 3, IsTentative = false },
                new POI { ID = 10, Name = "Coliseu do Porto", Description = "Biggest showcase stage of Porto", VisitTime = 90, Latitude = "41.146992", Longitude = "-8.605417", Altitude = "92", CategoryID = 3, IsTentative = false },
                new POI { ID = 11, Name = "Museu do Carro El�ctrico", Description = "Museum for the tramway of Porto", VisitTime = 45, Latitude = "41.147581", Longitude = "-8.632936", Altitude = "7", CategoryID = 3, IsTentative = false },
                new POI { ID = 12, Name = "Museu do Vinho do Porto", Description = "Museum for the typical Port Wine", VisitTime = 30, Latitude = "41.144923", Longitude = "-8.626746", Altitude = "14", CategoryID = 3, IsTentative = false },
                new POI { ID = 13, Name = "Pal�cio da Bolsa", Description = "Built by Portugal's commercial association", VisitTime = 35, Latitude = "41.141385", Longitude = "-8.615674", Altitude = "24", CategoryID = 3, IsTentative = false },
                new POI { ID = 14, Name = "Est�dio do Drag�o", Description = "National stadium for FC Porto", VisitTime = 105, Latitude = "41.161775", Longitude = "-8.583595", Altitude = "101", CategoryID = 4, IsTentative = false },
                new POI { ID = 15, Name = "Est�dio do Bessa", Description = "National stadium for Boavista FC", VisitTime = 105, Latitude = "41.162195", Longitude = "-8.642582", Altitude = "72", CategoryID = 4, IsTentative = false },
                new POI { ID = 16, Name = "Parque da Cidade", Description = "Biggest natural park of Porto", VisitTime = 45, Latitude = "41.168175", Longitude = "-8.675868", Altitude = "23", CategoryID = 4, IsTentative = false },
                new POI { ID = 17, Name = "Via Catarina", Description = "Shopping mall on the famous Sta. Catarina Street", VisitTime = 60, Latitude = "41.148931", Longitude = "-8.605953", Altitude = "91", CategoryID = 5, IsTentative = false },
                new POI { ID = 18, Name = "Rua de Cedofeita", Description = "Commercial hotspot for cultural enrichment", VisitTime = 45, Latitude = "41.150267", Longitude = "-8.616882", Altitude = "90", CategoryID = 5, IsTentative = false },
                new POI { ID = 19, Name = "Mercado Bom Sucesso", Description = "Refurbished marketplace now filled with exciting new store", VisitTime = 30, Latitude = "41.155534", Longitude = "-8.629048", Altitude = "81", CategoryID = 5, IsTentative = false },
                new POI { ID = 20, Name = "Funda��o Gomes Teixeira", Description = "Academic federation of Porto head office", VisitTime = 45, Latitude = "41.146555", Longitude = "-8.615706", Altitude = "85", CategoryID = 2, IsTentative = false },
                new POI { ID = 21, Name = "Active Virgin Porto", Description = "One of the most famous fitness Gym franchise in the porto", VisitTime = 45, Latitude = "41.150021", Longitude = "-8.607082", Altitude = "92",/*ProposerID= "134f1f5a-7a89-49e7-b26c-d2cc681bbbd7",*/ CategoryID = 7}
            );

            context.POIConnections.AddOrUpdate(
                p=> new { p.POI1ID , p.POI2ID},
                new POIConnection { POI1ID = 1, POI2ID = 3, Distance = 850 },
                new POIConnection { POI1ID = 2, POI2ID = 13, Distance = 750 },
                new POIConnection { POI1ID = 3, POI2ID = 1, Distance = 850 },
                new POIConnection { POI1ID = 3, POI2ID = 2, Distance = 1100 },
                new POIConnection { POI1ID = 3, POI2ID = 4, Distance = 550 },
                new POIConnection { POI1ID = 3, POI2ID = 10, Distance = 1000 },
                new POIConnection { POI1ID = 3, POI2ID = 14, Distance = 3600 },
                new POIConnection { POI1ID = 4, POI2ID = 5, Distance = 1300 },
                new POIConnection { POI1ID = 4, POI2ID = 18, Distance = 600 },
                new POIConnection { POI1ID = 5, POI2ID = 4, Distance = 1300 },
                new POIConnection { POI1ID = 5, POI2ID = 15, Distance = 1500 },
                new POIConnection { POI1ID = 5, POI2ID = 19, Distance = 450 },
                new POIConnection { POI1ID = 6, POI2ID = 11, Distance = 1000 },
                new POIConnection { POI1ID = 6, POI2ID = 19, Distance = 1300 },
                new POIConnection { POI1ID = 7, POI2ID = 3, Distance = 3800 },
                new POIConnection { POI1ID = 7, POI2ID = 14, Distance = 4100 },
                new POIConnection { POI1ID = 8, POI2ID = 15, Distance = 4800 },
                new POIConnection { POI1ID = 8, POI2ID = 16, Distance = 350 },
                new POIConnection { POI1ID = 9, POI2ID = 18, Distance = 1000 },
                new POIConnection { POI1ID = 10, POI2ID = 17, Distance = 350 },
                new POIConnection { POI1ID = 11, POI2ID = 6, Distance = 1000 },
                new POIConnection { POI1ID = 11, POI2ID = 12, Distance = 600 },
                new POIConnection { POI1ID = 12, POI2ID = 2, Distance = 2000 },
                new POIConnection { POI1ID = 12, POI2ID = 11, Distance = 600 },
                new POIConnection { POI1ID = 13, POI2ID = 1, Distance = 650 },
                new POIConnection { POI1ID = 14, POI2ID = 7, Distance = 4100 },
                new POIConnection { POI1ID = 15, POI2ID = 16, Distance = 5800 },
                new POIConnection { POI1ID = 15, POI2ID = 19, Distance = 2200 },
                new POIConnection { POI1ID = 16, POI2ID = 8, Distance = 350},
                new POIConnection { POI1ID = 17, POI2ID = 3, Distance = 650 },
                new POIConnection { POI1ID = 17, POI2ID = 10, Distance = 350},
                new POIConnection { POI1ID = 18, POI2ID = 4, Distance = 600},
                new POIConnection { POI1ID = 18, POI2ID = 20, Distance = 700 },
                new POIConnection { POI1ID = 19, POI2ID = 5, Distance = 450 },
                new POIConnection { POI1ID = 19, POI2ID = 6, Distance = 1300},
                new POIConnection { POI1ID = 20, POI2ID = 9, Distance = 750 },
                new POIConnection { POI1ID = 20, POI2ID = 11, Distance = 800}
            );

            context.Schedules.AddOrUpdate(
                p => new { p.POIID , p.Day },
                new Schedule { POIID = 1, Day = 1, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 1, Day = 2, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 1, Day = 3, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 1, Day = 4, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 1, Day = 5, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 1, Day = 6, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 1, Day = 7, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 2, Day = 1, Open = "09:00", Close = "19:00" },
                new Schedule { POIID = 2, Day = 2, Open = "09:00", Close = "19:00" },
                new Schedule { POIID = 2, Day = 3, Open = "09:00", Close = "19:00" },
                new Schedule { POIID = 2, Day = 4, Open = "09:00", Close = "19:00" },
                new Schedule { POIID = 2, Day = 5, Open = "09:00", Close = "19:00" },
                new Schedule { POIID = 2, Day = 6, Open = "09:00", Close = "19:00" },
                new Schedule { POIID = 2, Day = 7, Open = "09:00", Close = "19:00" },
                new Schedule { POIID = 3, Day = 1, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 3, Day = 2, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 3, Day = 3, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 3, Day = 4, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 3, Day = 5, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 3, Day = 6, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 3, Day = 7, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 4, Day = 1, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 4, Day = 2, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 4, Day = 3, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 4, Day = 4, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 4, Day = 5, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 4, Day = 6, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 4, Day = 7, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 5, Day = 1, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 5, Day = 2, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 5, Day = 3, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 5, Day = 4, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 5, Day = 5, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 5, Day = 6, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 5, Day = 7, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 6, Day = 1, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 6, Day = 2, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 6, Day = 3, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 6, Day = 4, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 6, Day = 5, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 6, Day = 6, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 7, Day = 1, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 7, Day = 2, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 7, Day = 3, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 7, Day = 4, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 7, Day = 5, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 7, Day = 6, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 7, Day = 7, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 8, Day = 1, Open = "09:00", Close = "17:30" },
                new Schedule { POIID = 8, Day = 2, Open = "09:00", Close = "17:30" },
                new Schedule { POIID = 8, Day = 3, Open = "09:00", Close = "17:30" },
                new Schedule { POIID = 8, Day = 4, Open = "09:00", Close = "17:30" },
                new Schedule { POIID = 8, Day = 5, Open = "09:00", Close = "17:30" },
                new Schedule { POIID = 8, Day = 6, Open = "10:00", Close = "17:30" },
                new Schedule { POIID = 9, Day = 2, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 9, Day = 3, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 9, Day = 4, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 9, Day = 5, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 9, Day = 6, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 9, Day = 7, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 10, Day = 1, Open = "13:00", Close = "20:30" },
                new Schedule { POIID = 10, Day = 2, Open = "13:00", Close = "20:30" },
                new Schedule { POIID = 10, Day = 3, Open = "13:00", Close = "20:30" },
                new Schedule { POIID = 10, Day = 4, Open = "13:00", Close = "20:30" },
                new Schedule { POIID = 10, Day = 5, Open = "13:00", Close = "20:30" },
                new Schedule { POIID = 10, Day = 6, Open = "13:00", Close = "20:30" },
                new Schedule { POIID = 11, Day = 1, Open = "14:00", Close = "18:00" },
                new Schedule { POIID = 11, Day = 2, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 11, Day = 3, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 11, Day = 4, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 11, Day = 5, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 11, Day = 6, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 11, Day = 7, Open = "10:00", Close = "18:00" },
                new Schedule { POIID = 12, Day = 2, Open = "10:00", Close = "17:30" },
                new Schedule { POIID = 12, Day = 3, Open = "10:00", Close = "17:30" },
                new Schedule { POIID = 12, Day = 4, Open = "10:00", Close = "17:30" },
                new Schedule { POIID = 12, Day = 5, Open = "10:00", Close = "17:30" },
                new Schedule { POIID = 12, Day = 6, Open = "10:00", Close = "17:30" },
                new Schedule { POIID = 12, Day = 7, Open = "10:00", Close = "17:30" },
                new Schedule { POIID = 13, Day = 1, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 13, Day = 2, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 13, Day = 3, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 13, Day = 4, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 13, Day = 5, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 13, Day = 6, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 13, Day = 7, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 14, Day = 1, Open = "13:00", Close = "16:45" },
                new Schedule { POIID = 14, Day = 2, Open = "11:00", Close = "16:45" },
                new Schedule { POIID = 14, Day = 3, Open = "11:00", Close = "16:45" },
                new Schedule { POIID = 14, Day = 4, Open = "11:00", Close = "16:45" },
                new Schedule { POIID = 14, Day = 5, Open = "11:00", Close = "16:45" },
                new Schedule { POIID = 14, Day = 6, Open = "11:00", Close = "16:45" },
                new Schedule { POIID = 14, Day = 7, Open = "11:00", Close = "16:45" },
                new Schedule { POIID = 15, Day = 1, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 15, Day = 2, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 15, Day = 3, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 15, Day = 4, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 15, Day = 5, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 15, Day = 6, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 15, Day = 7, Open = "09:30", Close = "17:30" },
                new Schedule { POIID = 16, Day = 1, Open = "08:00", Close = "22:00" },
                new Schedule { POIID = 16, Day = 2, Open = "08:00", Close = "22:00" },
                new Schedule { POIID = 16, Day = 3, Open = "08:00", Close = "22:00" },
                new Schedule { POIID = 16, Day = 4, Open = "08:00", Close = "22:00" },
                new Schedule { POIID = 16, Day = 5, Open = "08:00", Close = "22:00" },
                new Schedule { POIID = 16, Day = 6, Open = "08:00", Close = "22:00" },
                new Schedule { POIID = 16, Day = 7, Open = "08:00", Close = "22:00" },
                new Schedule { POIID = 17, Day = 1, Open = "09:00", Close = "21:00" },
                new Schedule { POIID = 17, Day = 2, Open = "09:00", Close = "21:00" },
                new Schedule { POIID = 17, Day = 3, Open = "09:00", Close = "21:00" },
                new Schedule { POIID = 17, Day = 4, Open = "09:00", Close = "21:00" },
                new Schedule { POIID = 17, Day = 5, Open = "09:00", Close = "21:00" },
                new Schedule { POIID = 17, Day = 6, Open = "09:00", Close = "21:00" },
                new Schedule { POIID = 17, Day = 7, Open = "09:00", Close = "21:00" },
                new Schedule { POIID = 18, Day = 1, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 18, Day = 2, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 18, Day = 3, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 18, Day = 4, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 18, Day = 5, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 18, Day = 6, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 18, Day = 7, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 19, Day = 1, Open = "10:00", Close = "23:00" },
                new Schedule { POIID = 19, Day = 2, Open = "10:00", Close = "23:00" },
                new Schedule { POIID = 19, Day = 3, Open = "10:00", Close = "23:00" },
                new Schedule { POIID = 19, Day = 4, Open = "10:00", Close = "23:00" },
                new Schedule { POIID = 19, Day = 5, Open = "10:00", Close = "00:00" },
                new Schedule { POIID = 19, Day = 6, Open = "10:00", Close = "00:00" },
                new Schedule { POIID = 19, Day = 7, Open = "10:00", Close = "00:00" },
                new Schedule { POIID = 20, Day = 1, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 20, Day = 2, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 20, Day = 3, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 20, Day = 4, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 20, Day = 5, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 20, Day = 6, Open = "08:00", Close = "00:00" },
                new Schedule { POIID = 20, Day = 7, Open = "08:00", Close = "00:00" }
            );

            context.Restrictions.AddOrUpdate(
                p=> p.Code,
                new Restriction { Code = "M5", Description="For Ages 5 and Up" },
                new Restriction { Code = "M12", Description = "For Ages 12 and Up" },
                new Restriction { Code = "M16", Description = "For Ages 16 and Up" },
                new Restriction { Code = "M18", Description = "For Ages 18 and Up" },
                new Restriction { Code = "Mob", Description = "Not suited for people with restricted mobility" },
                new Restriction { Code = "Epi", Description = "Epilepsy risk" },
                new Restriction { Code = "Prg", Description = "Not suited for pregnant women" },
                new Restriction { Code = "Crd", Description = "Risk for prople with heart conditions" },
                new Restriction { Code = "H10", Description = "Minimum height 1.0m" },
                new Restriction { Code = "H15", Description = "Minimum height 1.5m" }
            );

            context.HashTags.AddOrUpdate(
                p => p.ID,
                new Hashtag { ID = 1, Label= "Iliketrains", IsTentative = false, POIID = 1 },
                new Hashtag { ID = 2, Label = "MetroDoPorto", IsTentative = false, POIID = 1 },
                new Hashtag { ID = 3, Label = "OldAsHell", IsTentative = false, POIID = 1 },
                new Hashtag { ID = 4, Label = "HighAsHell", IsTentative = false, POIID = 2 },
                new Hashtag { ID = 5, Label = "SoManySteps", IsTentative = false, POIID = 2 },
                new Hashtag { ID = 6, Label = "AwesomeView", IsTentative = false, POIID = 2 },
                new Hashtag { ID = 7, Label = "WorthIt", IsTentative = false, POIID = 2 },
                new Hashtag { ID = 8, Label = "Confusing", IsTentative = false, POIID = 3 },
                new Hashtag { ID = 9, Label = "GetLost", IsTentative = false, POIID = 3 },
                new Hashtag { ID = 10, Label = "SoManyQueues", IsTentative = false, POIID = 3 },
                new Hashtag { ID = 11, Label = "WowMuchGuards", IsTentative = false, POIID = 4 },
                new Hashtag { ID = 12, Label = "SuchRepublic", IsTentative = false, POIID = 4 },
                new Hashtag { ID = 13, Label = "BigGarden", IsTentative = false, POIID = 5 },
                new Hashtag { ID = 14, Label = "VictoryOverTheFrench", IsTentative = false, POIID = 5 },
                new Hashtag { ID = 15, Label = "BigAsHell", IsTentative = false, POIID = 5 },
                new Hashtag { ID = 16, Label = "CasaDaMusica", IsTentative = false, POIID = 5 },
                new Hashtag { ID = 17, Label = "Suffering", IsTentative = false, POIID = 7 },
                new Hashtag { ID = 18, Label = "ManyEngineering", IsTentative = false, POIID = 7 },
                new Hashtag { ID = 19, Label = "MuchPrograming", IsTentative = false, POIID = 7 },
                new Hashtag { ID = 20, Label = "CISTER", IsTentative = false, POIID = 7 },
                new Hashtag { ID = 21, Label = "SoMuchWater", IsTentative = false, POIID = 8 },
                new Hashtag { ID = 22, Label = "VeryInteresting", IsTentative = false, POIID = 8 },
                new Hashtag { ID = 23, Label = "Moist", IsTentative = false, POIID = 8 },
                new Hashtag { ID = 24, Label = "Museum", IsTentative = false, POIID = 9 },
                new Hashtag { ID = 25, Label = "VeryArts", IsTentative = false, POIID = 9 },
                new Hashtag { ID = 26, Label = "MikaelCarreira4Ever", IsTentative = false, POIID = 10 },
                new Hashtag { ID = 27, Label = "LoudAsHell", IsTentative = false, POIID = 10 },
                new Hashtag { ID = 28, Label = "Transports", IsTentative = false, POIID = 11 },
                new Hashtag { ID = 29, Label = "PoPoHonk", IsTentative = false, POIID = 11 },
                new Hashtag { ID = 30, Label = "DrunkAsHell", IsTentative = false, POIID = 12 },
                new Hashtag { ID = 31, Label = "CantRememberYesterday", IsTentative = false, POIID = 12 },
                new Hashtag { ID = 32, Label = "VeryFancy", IsTentative = false, POIID = 13 },
                new Hashtag { ID = 33, Label = "NotARealPalace", IsTentative = false, POIID = 13 },
                new Hashtag { ID = 34, Label = "SomosPorto", IsTentative = false, POIID = 14 },
                new Hashtag { ID = 35, Label = "AzulEBrancoECoracao", IsTentative = false, POIID = 14 },
                new Hashtag { ID = 36, Label = "Mecos", IsTentative = false, POIID = 15 },
                new Hashtag { ID = 37, Label = "Boavista", IsTentative = false, POIID = 15 },
                new Hashtag { ID = 38, Label = "BessaXXI", IsTentative = false, POIID = 15 },
                new Hashtag { ID = 39, Label = "Nature", IsTentative = false, POIID = 16 },
                new Hashtag { ID = 40, Label = "HugeAsHell", IsTentative = false, POIID = 16 },
                new Hashtag { ID = 41, Label = "StreetShopping", IsTentative = false, POIID = 17 },
                new Hashtag { ID = 42, Label = "GotInWithMoney", IsTentative = false, POIID = 17 },
                new Hashtag { ID = 43, Label = "LeftBroke", IsTentative = false, POIID = 17 },
                new Hashtag { ID = 44, Label = "CommercialStreet", IsTentative = false, POIID = 18 },
                new Hashtag { ID = 45, Label = "SoManyShops", IsTentative = false, POIID = 18 },
                new Hashtag { ID = 46, Label = "OldButNowNew", IsTentative = false, POIID = 19 },
                new Hashtag { ID = 47, Label = "FancyDinning", IsTentative = false, POIID = 19 },
                new Hashtag { ID = 48, Label = "Praxis", IsTentative = false, POIID = 20 },
                new Hashtag { ID = 49, Label = "Academic", IsTentative = false, POIID = 20 }
            );

            context.AlertTypes.AddOrUpdate(
                p => p.ID,
                new AlertType { ID = 1, Description = "Point of Interest Decision" },
                new AlertType { ID = 2, Description = "Itinerary Decision" }
            );

            SaveChanges(context);
        }

        private static void SaveChanges(DbContext context)
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();
                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }
                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                );
            }
        }
    }
}
