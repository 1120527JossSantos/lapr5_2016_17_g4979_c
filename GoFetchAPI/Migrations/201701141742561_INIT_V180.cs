namespace GoFetchAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class INIT_V180 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alerts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AlertTypeID = c.Int(nullable: false),
                        UserID = c.String(maxLength: 128, storeType: "nvarchar"),
                        Message = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AlertTypes", t => t.AlertTypeID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.AlertTypeID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.AlertTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Email = c.String(maxLength: 250, storeType: "nvarchar"),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(unicode: false),
                        SecurityStamp = c.String(unicode: false),
                        PhoneNumber = c.String(unicode: false),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(precision: 0),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 250, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        ClaimType = c.String(unicode: false),
                        ClaimValue = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        ProviderKey = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        RoleId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Hashtags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Label = c.String(nullable: false, unicode: false),
                        IsTentative = c.Boolean(nullable: false),
                        POIID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.POIs", t => t.POIID, cascadeDelete: true)
                .Index(t => t.POIID);
            
            CreateTable(
                "dbo.POIs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, unicode: false),
                        Description = c.String(nullable: false, unicode: false),
                        VisitTime = c.Int(nullable: false),
                        IsTentative = c.Boolean(nullable: false),
                        Latitude = c.String(nullable: false, unicode: false),
                        Longitude = c.String(nullable: false, unicode: false),
                        Altitude = c.String(unicode: false),
                        CategoryID = c.Int(nullable: false),
                        ProposerID = c.String(maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.POICategories", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ProposerID)
                .Index(t => t.CategoryID)
                .Index(t => t.ProposerID);
            
            CreateTable(
                "dbo.POICategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ImageType = c.String(unicode: false),
                        Content = c.Binary(),
                        POI_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.POIs", t => t.POI_ID)
                .Index(t => t.POI_ID);
            
            CreateTable(
                "dbo.Restrictions",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Description = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.Profiles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        BirthDate = c.DateTime(nullable: false, precision: 0),
                        User_Id = c.String(maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Schedules",
                c => new
                    {
                        Day = c.Int(nullable: false),
                        POIID = c.Int(nullable: false),
                        Open = c.String(unicode: false),
                        Close = c.String(unicode: false),
                    })
                .PrimaryKey(t => new { t.Day, t.POIID })
                .ForeignKey("dbo.POIs", t => t.POIID, cascadeDelete: true)
                .Index(t => t.POIID);
            
            CreateTable(
                "dbo.Itineraries",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.String(maxLength: 128, storeType: "nvarchar"),
                        StartTime = c.DateTime(nullable: false, precision: 0),
                        Type = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.POITravels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        POI1ID = c.Int(nullable: false),
                        POI2ID = c.Int(nullable: false),
                        typeOfTravel = c.Int(nullable: false),
                        ItineraryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Itineraries", t => t.ItineraryID, cascadeDelete: true)
                .ForeignKey("dbo.POIs", t => t.POI1ID, cascadeDelete: true)
                .ForeignKey("dbo.POIs", t => t.POI2ID, cascadeDelete: true)
                .Index(t => t.POI1ID)
                .Index(t => t.POI2ID)
                .Index(t => t.ItineraryID);
            
            CreateTable(
                "dbo.Visits",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        POIID = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false, precision: 0),
                        ItineraryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Itineraries", t => t.ItineraryID, cascadeDelete: true)
                .ForeignKey("dbo.POIs", t => t.POIID, cascadeDelete: true)
                .Index(t => t.POIID)
                .Index(t => t.ItineraryID);
            
            CreateTable(
                "dbo.POIConnections",
                c => new
                    {
                        POI1ID = c.Int(nullable: false),
                        POI2ID = c.Int(nullable: false),
                        Distance = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.POI1ID, t.POI2ID })
                .ForeignKey("dbo.POIs", t => t.POI1ID, cascadeDelete: true)
                .ForeignKey("dbo.POIs", t => t.POI2ID, cascadeDelete: true)
                .Index(t => t.POI1ID)
                .Index(t => t.POI2ID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Name = c.String(nullable: false, maxLength: 250, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.RestrictionPOIs",
                c => new
                    {
                        Restriction_Code = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        POI_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Restriction_Code, t.POI_ID })
                .ForeignKey("dbo.Restrictions", t => t.Restriction_Code, cascadeDelete: true)
                .ForeignKey("dbo.POIs", t => t.POI_ID, cascadeDelete: true)
                .Index(t => t.Restriction_Code)
                .Index(t => t.POI_ID);
            
            CreateTable(
                "dbo.ProfileRestrictions",
                c => new
                    {
                        Profile_ID = c.Int(nullable: false),
                        Restriction_Code = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => new { t.Profile_ID, t.Restriction_Code })
                .ForeignKey("dbo.Profiles", t => t.Profile_ID, cascadeDelete: true)
                .ForeignKey("dbo.Restrictions", t => t.Restriction_Code, cascadeDelete: true)
                .Index(t => t.Profile_ID)
                .Index(t => t.Restriction_Code);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.POIConnections", "POI2ID", "dbo.POIs");
            DropForeignKey("dbo.POIConnections", "POI1ID", "dbo.POIs");
            DropForeignKey("dbo.Visits", "POIID", "dbo.POIs");
            DropForeignKey("dbo.Visits", "ItineraryID", "dbo.Itineraries");
            DropForeignKey("dbo.Itineraries", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.POITravels", "POI2ID", "dbo.POIs");
            DropForeignKey("dbo.POITravels", "POI1ID", "dbo.POIs");
            DropForeignKey("dbo.POITravels", "ItineraryID", "dbo.Itineraries");
            DropForeignKey("dbo.Schedules", "POIID", "dbo.POIs");
            DropForeignKey("dbo.Profiles", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ProfileRestrictions", "Restriction_Code", "dbo.Restrictions");
            DropForeignKey("dbo.ProfileRestrictions", "Profile_ID", "dbo.Profiles");
            DropForeignKey("dbo.RestrictionPOIs", "POI_ID", "dbo.POIs");
            DropForeignKey("dbo.RestrictionPOIs", "Restriction_Code", "dbo.Restrictions");
            DropForeignKey("dbo.POIs", "ProposerID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Images", "POI_ID", "dbo.POIs");
            DropForeignKey("dbo.Hashtags", "POIID", "dbo.POIs");
            DropForeignKey("dbo.POIs", "CategoryID", "dbo.POICategories");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Alerts", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Alerts", "AlertTypeID", "dbo.AlertTypes");
            DropIndex("dbo.ProfileRestrictions", new[] { "Restriction_Code" });
            DropIndex("dbo.ProfileRestrictions", new[] { "Profile_ID" });
            DropIndex("dbo.RestrictionPOIs", new[] { "POI_ID" });
            DropIndex("dbo.RestrictionPOIs", new[] { "Restriction_Code" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.POIConnections", new[] { "POI2ID" });
            DropIndex("dbo.POIConnections", new[] { "POI1ID" });
            DropIndex("dbo.Visits", new[] { "ItineraryID" });
            DropIndex("dbo.Visits", new[] { "POIID" });
            DropIndex("dbo.POITravels", new[] { "ItineraryID" });
            DropIndex("dbo.POITravels", new[] { "POI2ID" });
            DropIndex("dbo.POITravels", new[] { "POI1ID" });
            DropIndex("dbo.Itineraries", new[] { "UserID" });
            DropIndex("dbo.Schedules", new[] { "POIID" });
            DropIndex("dbo.Profiles", new[] { "User_Id" });
            DropIndex("dbo.Images", new[] { "POI_ID" });
            DropIndex("dbo.POIs", new[] { "ProposerID" });
            DropIndex("dbo.POIs", new[] { "CategoryID" });
            DropIndex("dbo.Hashtags", new[] { "POIID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Alerts", new[] { "UserID" });
            DropIndex("dbo.Alerts", new[] { "AlertTypeID" });
            DropTable("dbo.ProfileRestrictions");
            DropTable("dbo.RestrictionPOIs");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.POIConnections");
            DropTable("dbo.Visits");
            DropTable("dbo.POITravels");
            DropTable("dbo.Itineraries");
            DropTable("dbo.Schedules");
            DropTable("dbo.Profiles");
            DropTable("dbo.Restrictions");
            DropTable("dbo.Images");
            DropTable("dbo.POICategories");
            DropTable("dbo.POIs");
            DropTable("dbo.Hashtags");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AlertTypes");
            DropTable("dbo.Alerts");
        }
    }
}
