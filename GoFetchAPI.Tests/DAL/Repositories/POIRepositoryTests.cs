﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFetchAPI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;
using GoFetchAPI.DTO;
using GoFetchAPI.ViewModels;

namespace GoFetchAPI.DAL.Tests
{
    [TestClass()]
    public class POIRepositoryTests
    {
        [TestMethod()]
        public void POIRepositoryTest()
        {
            POIRepository repo = new POIRepository();
            Assert.IsNotNull(repo.context);
        }

        [TestMethod()]
        public void CreateTest()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);

            POI fromRepo = repo.GetData(p.ID);
            repo.Delete(p.ID);
            Assert.IsNotNull(fromRepo);
            
        }

        [TestMethod()]
        public void DeleteTest()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);
           bool b= repo.Delete(p.ID);
            Assert.IsTrue(b);
            
        }

        [TestMethod()]
        public void GetDataTest()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);

            POI fromRepo = repo.GetData(p.ID);
            repo.Delete(p.ID);
            Assert.IsNotNull(fromRepo);
            
        }

        [TestMethod()]
        public void GetByCategoryTest()
        {
            POIRepository repo = new POIRepository();
            Assert.IsNotNull(repo.GetByCategory(1));
        }

        [TestMethod()]
        public void GetByHashtagTest()
        {
            POIRepository repo = new POIRepository();
            Assert.IsNotNull(repo.GetByCategory(1));
        }

        [TestMethod()]
        public void GetDataTest1()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);

            POI fromRepo = repo.GetData(p.ID);
            repo.Delete(p.ID);
            int count = repo.GetData("1").Count();
            Assert.IsTrue(count!=0);
        }

        [TestMethod()]
        public void ConvertSuggestPOIToModelTest()
        {
            POIRepository repo = new POIRepository();
            SuggestPOI poi = new SuggestPOI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.Name = "Test";
            poi.VisitTime = 120;
            POI p = repo.ConvertSuggestPOIToModel(poi,"1");
            Assert.AreEqual(poi.CategoryID,p.CategoryID);
        }

        [TestMethod()]
        public void UpdateTest()
        {
            POIRepository repo = new POIRepository();
            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);
            POI newpoi = new POI();
            newpoi.ID = p.ID;
            newpoi.Altitude = "123";
            newpoi.Longitude = "123";
            newpoi.Latitude = "123";
            newpoi.CategoryID = 3;
            newpoi.Description = "Unit Test";
            newpoi.IsTentative = true;
            newpoi.Name = "UpdateTest";
            newpoi.VisitTime = 120;
            List<Restriction> restrictions = new List<Restriction>();
            newpoi.RestrictionList = restrictions;
            bool b=repo.Update(p.ID,newpoi);
            repo.Delete(p.ID);
            Assert.IsTrue(b);
            
        }

        [TestMethod()]
        public void HasHashtagTest()
        {
            POIRepository repo = new POIRepository();
            Assert.IsFalse(repo.HasHashtag(1,"Unit Test"));
        }

        [TestMethod()]
        public void AcceptTest()
        {
            POIRepository repo = new POIRepository();
            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;
            POI p = repo.Create(poi);

            bool b = repo.Accept(p.ID);
            repo.Delete(p.ID);
            Assert.IsTrue(b);
        }

        [TestMethod()]
        public void AddScheduleTest()
        {
            POIRepository repo = new POIRepository();
            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;
            POI p = repo.Create(poi);
            ScheduleRepository shrepo = new ScheduleRepository();
            List<Schedule> schedule = new List<Schedule>();
            Schedule sh = new Schedule();
            sh.Day = 1;
            schedule.Add(sh);
            repo.AddSchedule(p.ID,schedule);

            bool b = repo.AddSchedule(p.ID, schedule);
            repo.Delete(p.ID);
            Assert.IsTrue(b);
        }

        [TestMethod()]
        public void ConvertModelListToDTOTest()
        {
            POIRepository repo = new POIRepository();
            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.Name = "Test";
            poi.VisitTime = 120;

            List<POI> list = new List<POI>();
            list.Add(poi); list.Add(poi);
            List<POIDTO> dtoList = repo.ConvertModelListToDTO(list);
            Assert.AreEqual(2, dtoList.Count());
        }

        [TestMethod()]
        public void ConvertModelToDTOTest()
        {
            POIRepository repo = new POIRepository();
            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.Name = "Test";
            poi.VisitTime = 120;
            POIDTO p = repo.ConvertModelToDTO(poi);
            Assert.AreEqual(poi.CategoryID, p.CategoryID);
        }

        [TestMethod()]
        public void ConvertModelToALGAVDTOTest()
        {
            POIRepository repo = new POIRepository();
            POIAcceptionViewModel poiAccep = new POIAcceptionViewModel();
            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.Name = "Test";
            poi.VisitTime = 120;
            poi.ID = 1;


            List<POIConnection> con = new List<POIConnection>();
            List<Schedule> sch = new List<Schedule>();
            poiAccep.POI = poi;
            poiAccep.WorkSchedule=sch;
            poiAccep.Connections = con;

            POIALGAVDTO dto = repo.ConvertModelToALGAVDTO(poiAccep);
            Assert.AreEqual(dto.id,poi.ID);

        }

        [TestMethod()]
        public void ConvertDTOToModelTest()
        {
            POIRepository repo = new POIRepository();
            POIDTO poi = new POIDTO();
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.Name = "Test";
            poi.VisitTime = 120;
            ApplicationUser prop = new ApplicationUser();
            prop.Id = "2";
            poi.Proponente = prop; 
            POI p = repo.ConvertDTOToModel(poi);
            Assert.AreEqual(poi.Name, p.Name);
        }

    }
}