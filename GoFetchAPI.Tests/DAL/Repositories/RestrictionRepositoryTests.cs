﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFetchAPI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL.Tests
{
    [TestClass()]
    public class RestrictionRepositoryTests
    {
        [TestMethod()]
        public void RestrictionRepositoryTest()
        {
            RestrictionRepository repo = new RestrictionRepository();
            Assert.IsNotNull(repo.context);
        }

        [TestMethod()]
        public void CreateTest()
        {
            RestrictionRepository repo = new RestrictionRepository();
            Restriction restriction = new Restriction();
            restriction.Code = "1";
            restriction.Description = "Description Unit Test";

            Restriction cat = repo.Create(restriction);
            repo.Delete(cat.Code);
            Assert.IsNotNull(cat.Description);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            RestrictionRepository repo = new RestrictionRepository();
            Restriction restriction = new Restriction();
            restriction.Code = "1";
            restriction.Description = "Description Unit Test";

            Restriction cat = repo.Create(restriction);
            bool b=repo.Delete(cat.Code);
            Assert.IsTrue(b);
        }

        [TestMethod()]
        public void GetDataTest()
        {
            RestrictionRepository repo = new RestrictionRepository();
            Restriction restriction = new Restriction();
            restriction.Code = "1";
            restriction.Description = "Description Unit Test";

            Restriction cat = repo.Create(restriction);
            Restriction fromRepo = repo.GetData(cat.Code);
            repo.Delete(cat.Code);
            Assert.IsNotNull(fromRepo.Description);
        }

        [TestMethod()]
        public void GetDataTest1()
        {
            RestrictionRepository repo = new RestrictionRepository();
            Restriction restriction = new Restriction();
            restriction.Code = "1";
            restriction.Description = "Description Unit Test";

            Restriction cat = repo.Create(restriction);
            List<Restriction> fromRepo = repo.GetData();
            repo.Delete(cat.Code);
            Assert.AreNotEqual(0,fromRepo.ToList().Count());
        }

        [TestMethod()]
        public void UpdateTest()
        {
            RestrictionRepository repo = new RestrictionRepository();
            Restriction restriction = new Restriction();
            restriction.Code = "1";
            restriction.Description = "Description Unit Test";

            Restriction cat = repo.Create(restriction);
            Restriction fromRepo = repo.GetData(cat.Code);

            Restriction newP = new Restriction();
            newP.Code = fromRepo.Code;
            newP.Description = "Message: Unit Test";
            repo.Update(cat.Code, newP);
            Restriction aupdate = repo.GetData(cat.Code);
            repo.Delete(cat.Code);
            Assert.AreEqual("Message: Unit Test", aupdate.Description);
        }
    }
}