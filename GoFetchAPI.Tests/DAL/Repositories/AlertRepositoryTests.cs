﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFetchAPI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;
using GoFetchAPI.DTO;
using GoFetchAPI.DAL.Repositories;

namespace GoFetchAPI.DAL.Tests
{
    [TestClass()]
    public class AlertRepositoryTests
    {
        [TestMethod()]
        public void AlertRepositoryTest()
        {
            AlertRepository repo = new AlertRepository();
            Assert.IsNotNull(repo.context);
        }

        [TestMethod()]
        public void CreateTest()
        {
            AlertRepository repo = new AlertRepository();
            AlertTypeRepository repoType = new AlertTypeRepository();
            AlertType type = new AlertType();
            type.Description = "Unit Tests";
            Alert alert = new Alert();
            alert.Message = "Unit Test Message";
            alert.User = null;

            AlertType at = repoType.Create(type);
            alert.AlertTypeID = at.ID;

            Alert a = repo.Create(alert);
            repo.Delete(a.ID);
          
            Assert.IsNotNull(a);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            AlertRepository repo = new AlertRepository();
            AlertTypeRepository repoType = new AlertTypeRepository();
            AlertType type = new AlertType();
            type.Description = "Unit Tests";
            Alert alert = new Alert();
            alert.Message = "Unit Test Message";
            alert.User = null;

            AlertType at = repoType.Create(type);
            alert.AlertTypeID = at.ID;
            Alert a = repo.Create(alert);
            Alert fromRepoAlert = repo.GetData(a.ID);

           bool del= repo.Delete(a.ID);
            bool b = repoType.Delete(at.ID);
            
            Assert.IsTrue(del);
        }

        [TestMethod()]
        public void GetDataTest()
        {
            AlertRepository repo = new AlertRepository();
            AlertTypeRepository repoType = new AlertTypeRepository();
            AlertType type = new AlertType();
            type.Description = "Unit Tests";
            Alert alert = new Alert();
            alert.Message = "Unit Test Message";
            alert.User = null;

            AlertType at = repoType.Create(type);
            alert.AlertTypeID = at.ID;
            Alert a = repo.Create(alert);
            Alert fromRepoAlert = repo.GetData(a.ID);

            bool del = repo.Delete(a.ID);
            bool b = repoType.Delete(at.ID);

            Assert.IsNotNull(fromRepoAlert);
        }

        [TestMethod()]
        public void GetDataTest1()
        {
            AlertRepository repo = new AlertRepository();
            AlertTypeRepository repoType = new AlertTypeRepository();
            AlertType type = new AlertType();
            type.Description = "Unit Tests";
            Alert alert = new Alert();
            alert.Message = "Unit Test Message";
            alert.User = null;

            AlertType at = repoType.Create(type);
            alert.AlertTypeID = at.ID;

            int count = repo.GetData().Count();
            Assert.IsTrue(count!=0);
        }

        [TestMethod()]
        public void UpdateTest()
        {
            AlertRepository repo = new AlertRepository();
            AlertTypeRepository repoType = new AlertTypeRepository();
            AlertType type = new AlertType();
            type.Description = "Unit Tests";
            Alert alert = new Alert();
            alert.Message = "Unit Test Message";
            alert.User = null;
            
            AlertType at = repoType.Create(type);
            alert.AlertTypeID = at.ID;
            Alert a = repo.Create(alert);
            Alert fromRepoAlert = repo.GetData(a.ID);

            Alert newAlert = fromRepoAlert;
            newAlert.Message = "Message: Unit Test";
            repo.Update(fromRepoAlert.ID, newAlert);
            Alert aupdate = repo.GetData(fromRepoAlert.ID);
            repo.Delete(a.ID);
            repoType.Delete(at.ID);
            Assert.AreNotEqual("Unit Test Message",aupdate.Message);

        }

        [TestMethod()]
        public void AddUserMessageAlertTest()
        {
            AlertRepository repo = new AlertRepository();
            AlertTypeRepository repoType = new AlertTypeRepository();
            AlertType type = new AlertType();
            type.Description = "Unit Tests for Alert Repository";
            AlertType at = repoType.Create(type);
            ApplicationUser user = null;
            bool b = repo.AddUserMessageAlert(user, "Message Test", at.ID);
            repoType.Delete(at.ID);
            Assert.IsFalse(b);
            
        }

        [TestMethod()]
        public void ConvertModelListToDTOTest()
        {
            AlertRepository repo = new AlertRepository();
            List<AlertDTO> dtoList = new List<AlertDTO>();
            List<Alert> alertList = new List<Alert>();
            Alert alert = new Alert();
            alert.Message = "Test";
            alert.AlertTypeID = 3;
            alert.User = null;
            alertList.Add(alert);
            alertList.Add(alert);
            alertList.Add(alert);
            dtoList = repo.ConvertModelListToDTO(alertList);

            Assert.AreEqual(3,dtoList.Count);
        }

        [TestMethod()]
        public void ConvertModelToDTOTest()
        {
            AlertRepository repo = new AlertRepository();
            Alert alert = new Alert();
            AlertDTO alertDTO = new AlertDTO();
            alert.Message = "Test";
            alert.AlertTypeID = 3;
            alert.User = null;
            alertDTO=repo.ConvertModelToDTO(alert);
            Assert.IsTrue(alertDTO.Message=="Test");
        }

        [TestMethod()]
        public void ConvertDTOToModelTest()
        {
            AlertRepository repo = new AlertRepository();
            Alert alert = new Alert();
            AlertDTO alertDTO = new AlertDTO();
            alertDTO.Message = "Test";
            alertDTO.AlertTypeID = 3;
            alertDTO.User = null;
            alert = repo.ConvertDTOToModel(alertDTO);
            Assert.IsTrue(alertDTO.Message == "Test");
        }
    }
}