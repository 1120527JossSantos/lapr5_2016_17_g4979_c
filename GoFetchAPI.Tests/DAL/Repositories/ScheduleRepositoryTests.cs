﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFetchAPI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;
using GoFetchAPI.DTO;

namespace GoFetchAPI.DAL.Tests
{
    [TestClass()]
    public class ScheduleRepositoryTests
    {
        [TestMethod()]
        public void ScheduleRepositoryTest()
        {
            ScheduleRepository repo = new ScheduleRepository();
            Assert.IsNotNull(repo.context);
        }

        [TestMethod()]
        public void CreateTest()
        {
            ScheduleRepository repo = new ScheduleRepository();
            Schedule schedule = new Schedule();
            schedule.Day = 1;
            schedule.Open = "10:00";
            schedule.Close = "11:00";

            POIRepository repoPOI = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repoPOI.Create(poi);


            schedule.POIID = p.ID;
            Schedule cat = repo.Create(schedule);
            int day = cat.Day;
            repo.context.Schedules.Remove(cat);
            Assert.IsNotNull(day);
        }

        [TestMethod()]
        public void GetDataTest1()
        {
            ScheduleRepository repo = new ScheduleRepository();
            Schedule schedule = new Schedule();
            schedule.Day = 1;
            schedule.Open = "10:00";
            schedule.Close = "11:00";

            POIRepository repoPOI = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repoPOI.Create(poi);


            schedule.POIID = p.ID;
            Schedule cat = repo.Create(schedule);
            int day = cat.Day;
            repo.context.Schedules.Remove(cat);

            List<Schedule> list = new List<Schedule>();
            list = repo.GetData();
            Assert.AreNotEqual(0,list.Count());
        }

        [TestMethod()]
        public void ConvertModelListToDTOTest()
        {

            ScheduleRepository repo = new ScheduleRepository();
            Schedule schedule = new Schedule();
            schedule.Day = 1;
            schedule.Open = "10:00";
            schedule.Close = "11:00";

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            schedule.POIID = 2;


            List<Schedule> list = new List<Schedule>();
            list.Add(schedule);
            list.Add(schedule);

            List<ScheduleDTO> listdto = new List<ScheduleDTO>();
            listdto = repo.ConvertModelListToDTO(list);
            Assert.AreEqual(listdto.Count(), list.Count());
        }

        [TestMethod()]
        public void ConvertModelToDTOTest()
        {
            ScheduleRepository repo = new ScheduleRepository();
            Schedule schedule = new Schedule();
            schedule.Day = 1;
            schedule.Open = "10:00";
            schedule.Close = "11:00";

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            schedule.POIID = 2;

            ScheduleDTO dto = new ScheduleDTO();
            dto = repo.ConvertModelToDTO(schedule);
            Assert.AreEqual(dto.Day,schedule.Day);
        }

        [TestMethod()]
        public void ConvertDTOToModelTest()
        {
            ScheduleRepository repo = new ScheduleRepository();
            ScheduleDTO dto = new ScheduleDTO();

            dto.Day = 1;
            dto.Open = "10:00";
            dto.Close = "11:00";

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            dto.POIID = 2;

            Schedule schedule = new Schedule();
            schedule = repo.ConvertDTOToModel(dto);
            Assert.AreEqual(dto.Day, schedule.Day);
        }
    }
}