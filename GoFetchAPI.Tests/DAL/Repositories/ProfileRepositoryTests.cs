﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFetchAPI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL.Tests
{
    [TestClass()]
    public class ProfileRepositoryTests
    {
        [TestMethod()]
        public void ProfileRepositoryTest()
        {
            ProfileRepository repo = new ProfileRepository();
            Assert.IsNotNull(repo.context);
        }

        [TestMethod()]
        public void CreateTest()
        {
            ProfileRepository repo = new ProfileRepository();
            Profile profile = new Profile();
            profile.Name = "Unit Test";
            profile.BirthDate = new DateTime(1995, 12, 12);

            Profile p = repo.Create(profile);
            Profile fromRepo = repo.GetData(p.ID);
            repo.Delete(p.ID);
            Assert.IsNotNull(fromRepo);
           
        }

        [TestMethod()]
        public void DeleteTest()
        {
            ProfileRepository repo = new ProfileRepository();
            Profile profile = new Profile();
            profile.Name = "Unit Test";
            profile.BirthDate = new DateTime(1995, 12, 12);

            Profile p = repo.Create(profile);
            Profile fromRepo = repo.GetData(p.ID);
            Assert.IsNotNull(fromRepo);
            bool b=repo.Delete(p.ID);
            Assert.IsTrue(b);
            
        }

        [TestMethod()]
        public void GetDataTest()
        {
            ProfileRepository repo = new ProfileRepository();
            Profile profile = new Profile();
            profile.Name = "Unit Test";
            profile.BirthDate = new DateTime(1995, 12, 12);

            Profile p = repo.Create(profile);
            Profile fromRepo = repo.GetData(p.ID);
            repo.Delete(p.ID);
            Assert.IsNotNull(fromRepo);
            
        }

        [TestMethod()]
        public void GetDataTest1()
        {
            ProfileRepository repo = new ProfileRepository();
            Profile profile = new Profile();
            profile.Name = "Unit Test";
            profile.BirthDate = new DateTime(1995, 12, 12);

            Profile p = repo.Create(profile);
            Profile fromRepo = repo.GetData(p.ID);
            repo.Delete(p.ID);
            Assert.IsNotNull(repo.GetData().ToList().Count());
            
        }

        [TestMethod()]
        public void UpdateTest()
        {
            ProfileRepository repo = new ProfileRepository();
            Profile profile = new Profile();
            profile.Name = "Unit Test";
            profile.BirthDate = new DateTime(1995, 12, 12);

            Profile p = repo.Create(profile);
            Profile fromRepo = repo.GetData(p.ID);
            Profile newProfile = new Profile();
            newProfile.Name = "Unit Test Message";
            newProfile.BirthDate = new DateTime(1995, 12, 3);
            newProfile.ID = fromRepo.ID;
            repo.Update(fromRepo.ID, newProfile);
            Assert.AreEqual("Unit Test Message", newProfile.Name);
            repo.Delete(p.ID);
            Assert.AreEqual(new DateTime(1995, 12, 3), newProfile.BirthDate);
            
        }
    }
}