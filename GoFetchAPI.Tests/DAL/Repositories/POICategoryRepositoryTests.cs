﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFetchAPI.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;
using GoFetchAPI.DTO;

namespace GoFetchAPI.DAL.Repositories.Tests
{
    [TestClass()]
    public class POICategoryRepositoryTests
    {
        [TestMethod()]
        public void POICategoryRepositoryTest()
        {
            POICategoryRepository repo = new POICategoryRepository();
            Assert.IsNotNull(repo.context);
        }

        [TestMethod()]
        public void CreateTest()
        {
            POICategoryRepository repo = new POICategoryRepository();
            POICategory category = new POICategory();
            category.Description = "Description Unit Test";

            POICategory cat = repo.Create(category);
            POICategory fromRepo = repo.GetData(cat.ID);
            repo.Delete(cat.ID);
            Assert.IsNotNull(fromRepo.ID);
            
        }

        [TestMethod()]
        public void DeleteTest()
        {
            POICategoryRepository repo = new POICategoryRepository();
            POICategory category = new POICategory();
            category.Description = "Description Test";
            POICategory cat = repo.Create(category);
            POICategory fromRepo = repo.GetData(cat.ID);
            repo.Delete(cat.ID);
            Assert.IsNull(repo.GetData(cat.ID));
        }

        [TestMethod()]
        public void GetDataTest()
        {
            POICategoryRepository repo = new POICategoryRepository();
            POICategory category = new POICategory();
            category.Description = "Description Test";
            POICategory a = repo.Create(category);
            POICategory fromRepo = repo.GetData(a.ID);
            repo.Delete(a.ID);
            Assert.AreEqual(a, fromRepo);
        }

        [TestMethod()]
        public void GetDataTest1()
        {
            POICategoryRepository repo = new POICategoryRepository();
            Assert.IsNotNull(repo.GetData());
        }

        [TestMethod()]
        public void UpdateTest()
        {
            POICategoryRepository repo = new POICategoryRepository();
            POICategory category = new POICategory();
            category.Description = "Description Test";
            POICategory a = repo.Create(category);
            POICategory fromRepo = repo.GetData(a.ID);

            POICategory newP = fromRepo;
            newP.Description = "Message: Unit Test";
            repo.Update(fromRepo.ID, newP);
            POICategory aupdate = repo.GetData(fromRepo.ID);
            repo.Delete(a.ID);
            Assert.AreNotEqual("Unit Test Message", aupdate.Description);
            
        }

        [TestMethod()]
        public void ConvertModelListToDTOTest()
        {
            POICategoryRepository repo = new POICategoryRepository();
            List<POICategoryDTO> dtoList = new List<POICategoryDTO>();
            List<POICategory> list = new List<POICategory>();
            POICategory c = new POICategory();
            c.Description = "Test";

            list.Add(c);
            list.Add(c);
            list.Add(c);
            dtoList = repo.ConvertModelListToDTO(list);

            Assert.AreEqual(3, dtoList.Count);
        }

        [TestMethod()]
        public void ConvertModelToDTOTest()
        {
            POICategoryRepository repo = new POICategoryRepository();
            POICategory c = new POICategory();
            POICategoryDTO dto = new POICategoryDTO();
            c.Description = "Test";
            dto = repo.ConvertModelToDTO(c);
            Assert.IsTrue(dto.Description == "Test");
        }

        [TestMethod()]
        public void ConvertDTOToModelTest()
        {
            POICategoryRepository repo = new POICategoryRepository();
            POICategory p = new POICategory();
            POICategoryDTO dto = new POICategoryDTO();
            dto.Description = "Test";
            p = repo.ConvertDTOToModel(dto);
            Assert.IsTrue(dto.Description == "Test");
        }

    }
}