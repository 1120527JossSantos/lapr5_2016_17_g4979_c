﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFetchAPI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;
using GoFetchAPI.DTO;

namespace GoFetchAPI.DAL.Tests
{
    [TestClass()]
    public class VisitRepositoryTests
    {
        [TestMethod()]
        public void VisitRepositoryTest()
        {
            VisitRepository repo = new VisitRepository();
            Assert.IsNotNull(repo.context);
        }

        [TestMethod()]
        public void CreateTest()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);
            /**************************/
            ItineraryRepository repoIt = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary it = repoIt.Create(itinerary);
            /******************************/
            VisitRepository repoVisit = new VisitRepository();
            Visit visit = new Visit();

            visit.POIID = p.ID;

            visit.ItineraryID = it.ID;
            visit.StartTime= new DateTime(2017, 12, 12); 

            Visit cat = repoVisit.Create(visit);


            repoVisit.Delete(cat.ID);
            repoIt.Delete(it.ID);
            repo.Delete(p.ID);
            Assert.IsNotNull(cat.StartTime);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);
            /**************************/
            ItineraryRepository repoIt = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary it = repoIt.Create(itinerary);
            /******************************/
            VisitRepository repoVisit = new VisitRepository();
            Visit visit = new Visit();

            visit.POIID = p.ID;

            visit.ItineraryID = it.ID;
            visit.StartTime = new DateTime(2017, 12, 12);

            Visit cat = repoVisit.Create(visit);


           bool b= repoVisit.Delete(cat.ID);
            repoIt.Delete(it.ID);
            repo.Delete(p.ID);
            Assert.IsTrue(b);
        }

        [TestMethod()]
        public void GetDataTest()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);
            /**************************/
            ItineraryRepository repoIt = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary it = repoIt.Create(itinerary);
            /******************************/
            VisitRepository repoVisit = new VisitRepository();
            Visit visit = new Visit();

            visit.POIID = p.ID;

            visit.ItineraryID = it.ID;
            visit.StartTime = new DateTime(2017, 12, 12);

            Visit cat = repoVisit.Create(visit);


            Visit fromRepo = repoVisit.GetData(cat.ID);
            repoIt.Delete(it.ID);
            repo.Delete(p.ID);
            Assert.IsNotNull(fromRepo);
        }

        [TestMethod()]
        public void GetDataTest1()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);
            /**************************/
            ItineraryRepository repoIt = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary it = repoIt.Create(itinerary);
            /******************************/
            VisitRepository repoVisit = new VisitRepository();
            Visit visit = new Visit();

            visit.POIID = p.ID;

            visit.ItineraryID = it.ID;
            visit.StartTime = new DateTime(2017, 12, 12);

            Visit cat = repoVisit.Create(visit);

            List<Visit> fromRepo = repoVisit.GetData();
            repoVisit.Delete(cat.ID);
            repoIt.Delete(it.ID);
            repo.Delete(p.ID);

            Assert.AreNotEqual(0,fromRepo.Count);
        }

        [TestMethod()]
        public void UpdateTest()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);
            /**************************/
            ItineraryRepository repoIt = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary it = repoIt.Create(itinerary);
            /******************************/
            VisitRepository repoVisit = new VisitRepository();
            Visit visit = new Visit();

            visit.POIID = p.ID;

            visit.ItineraryID = it.ID;
            visit.StartTime = new DateTime(2017, 12, 12);

            Visit cat = repoVisit.Create(visit);

            Visit newV = new Visit();

            newV.POIID = p.ID;

            newV.ItineraryID = it.ID;
            newV.StartTime = new DateTime(2017, 12, 15);
            newV.ID = cat.ID;

            repoVisit.Update(cat.ID,newV);

            Visit fromRepo = repoVisit.GetData(cat.ID);
            repoVisit.Delete(cat.ID);
            repoIt.Delete(it.ID);
            repo.Delete(p.ID);
            Assert.AreEqual(new DateTime(2017, 12, 15), fromRepo.StartTime);
        }

        [TestMethod()]
        public void ConvertModelListToDTOTest()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);
            /**************************/
            ItineraryRepository repoIt = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary it = repoIt.Create(itinerary);
            /******************************/
            VisitRepository repoVisit = new VisitRepository();
            Visit visit = new Visit();

            visit.POIID = p.ID;
            visit.POI = p;
            visit.StartTime = new DateTime(2017, 12, 12);
            List<Visit> list = new List<Visit>();
            list.Add(visit);
            list.Add(visit);
            List<VisitDTO> cat = repoVisit.ConvertModelListToDTO(list);

            Assert.AreEqual(list.Count,cat.Count);
        }

        [TestMethod()]
        public void ConvertModelToDTOTest()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);
            /**************************/
            ItineraryRepository repoIt = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary it = repoIt.Create(itinerary);
            /******************************/
            VisitRepository repoVisit = new VisitRepository();
            Visit visi = new Visit();

            visi.POIID = p.ID;
            visi.POI = p;
            visi.StartTime = new DateTime(2017, 12, 12);

            VisitDTO cat = repoVisit.ConvertModelToDTO(visi);

            Assert.IsNotNull(cat.StartTime);
        }

        [TestMethod()]
        public void ConvertDTOToModelTest()
        {
            POIRepository repo = new POIRepository();

            POI poi = new POI();
            poi.Altitude = "123";
            poi.Longitude = "123";
            poi.Latitude = "123";
            poi.CategoryID = 3;
            poi.Description = "Unit Test";
            poi.IsTentative = true;
            poi.Name = "Test";
            poi.VisitTime = 120;

            POI p = repo.Create(poi);
            /**************************/
            ItineraryRepository repoIt = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary it = repoIt.Create(itinerary);
            /******************************/
            VisitRepository repoVisit = new VisitRepository();
            VisitDTO dto = new VisitDTO();

            dto.POIID = p.ID;
            dto.POI = p;
            dto.StartTime = new DateTime(2017, 12, 12);

            Visit cat = repoVisit.ConvertDTOToModel(dto);

            Assert.IsNotNull(cat.StartTime);
        }
    }
}