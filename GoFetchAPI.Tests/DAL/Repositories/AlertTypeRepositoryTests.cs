﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFetchAPI.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;

namespace GoFetchAPI.DAL.Repositories.Tests
{
    [TestClass()]
    public class AlertTypeRepositoryTests
    {
        [TestMethod()]
        public void AlertTypeRepositoryTest()
        {
            AlertRepository repo = new AlertRepository();
            Assert.IsNotNull(repo);
        }

        [TestMethod()]
        public void CreateTest()
        {
            AlertTypeRepository repo = new AlertTypeRepository();
            AlertType alert = new AlertType();
            alert.ID = 3;
            alert.Description = "AlertType TestUnit";
            AlertType alert2 = repo.Create(alert);
            Assert.IsNotNull(alert);
            repo.Delete(alert2.ID);

        }

        [TestMethod()]
        public void DeleteTest()
        {
            AlertTypeRepository repo = new AlertTypeRepository();
            AlertType alert = new AlertType();
            alert.ID = 4;
            alert.Description = "AlertType Delete TestUnit";
            AlertType alert2 = repo.Create(alert);
            repo.Delete(alert2.ID);
            int id = alert.ID;
            Assert.IsNull(repo.GetData(id));
        }

        [TestMethod()]
        public void GetDataTest()
        {
            AlertTypeRepository repo = new AlertTypeRepository();
            AlertType alert = new AlertType();
            alert.Description = "AlertType GetData TestUnit";
            AlertType p = repo.Create(alert);
            int id = p.ID;
            Assert.AreEqual(p, repo.GetData(id));
            repo.Delete(id);
        }

        [TestMethod()]
        public void GetDataTest1()
        {
            AlertTypeRepository repo = new AlertTypeRepository();
            Assert.IsNotNull(repo);
        }

        [TestMethod()]
        public void UpdateTest()
        {
            AlertTypeRepository repo = new AlertTypeRepository();
            AlertType alertType = new AlertType();
            alertType.Description = "Unit Test Message";

            AlertType a = repo.Create(alertType);
            AlertType fromRepo = repo.GetData(a.ID);

            AlertType newAlertType = fromRepo;
            newAlertType.Description = "Message: Unit Test";
            repo.Update(fromRepo.ID, newAlertType);
            AlertType aupdate = repo.GetData(fromRepo.ID);
            Assert.AreNotEqual("Unit Test Message", aupdate.Description);
            repo.Delete(a.ID);
        }
    }
}