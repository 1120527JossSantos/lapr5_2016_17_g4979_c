﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFetchAPI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;
using GoFetchAPI.DTO;

namespace GoFetchAPI.DAL.Tests
{
    [TestClass()]
    public class HashtagRepositoryTests
    {
        [TestMethod()]
        public void HashtagRepositoryTest()
        {
            HashtagRepository repo = new HashtagRepository();
            Assert.IsNotNull(repo.context);
        }

        [TestMethod()]
        public void CreateTest()
        {
            HashtagRepository repo = new HashtagRepository();

            Hashtag p = repo.Create("Unit Test");

            Hashtag fromRepo = repo.GetData(p.ID);
            repo.Delete(p.ID);
            Assert.IsNotNull(fromRepo);


        }

        [TestMethod()]
        public void IsInPOITest()
        {
            HashtagRepository repo = new HashtagRepository();

            Hashtag p = repo.Create("Unit Test");

            Hashtag fromRepo = repo.GetData(p.ID);
            bool b = repo.IsInPOI("Unit Test",100);
            repo.Delete(p.ID);
            Assert.IsFalse(b);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            HashtagRepository repo = new HashtagRepository();

            Hashtag p = repo.Create("Unit Test");
            bool b = repo.Delete(p.ID);

            Assert.IsTrue(b);
        }

        [TestMethod()]
        public void GetDataTest()
        {
            HashtagRepository repo = new HashtagRepository();

            Hashtag p = repo.Create("Unit Test");

            Hashtag fromRepo = repo.GetData(p.ID);
            repo.Delete(p.ID);
            Assert.IsNotNull(fromRepo);
        }

        [TestMethod()]
        public void GetDataTest1()
        {
            HashtagRepository repo = new HashtagRepository();

            Hashtag p = repo.Create("Unit Test");
            List<Hashtag> all = new List<Hashtag>();
            all.Add(p);
            all.Add(p);
            List<Hashtag> fromRepo = repo.GetData();
            repo.Delete(p.ID);
            Assert.AreNotEqual(0,fromRepo.Count);
        }

        [TestMethod()]
        public void ByLabelTest()
        {
            HashtagRepository repo = new HashtagRepository();

            Hashtag p = repo.Create("Unit Test");

            Hashtag fromRepo = repo.GetData(p.ID); ;
            repo.Delete(p.ID);

            Assert.IsNotNull(fromRepo);
        }

        [TestMethod()]
        public void UpdateTest()
        {
            HashtagRepository repo = new HashtagRepository();

            Hashtag p = repo.Create("Unit Test");

           
            Hashtag newH = new Hashtag();
            newH.Label = "Test";
            newH.ID = p.ID;
            //repo.Update(p.ID,newH);
            Hashtag fromRepo = repo.GetData(p.ID);
            repo.Delete(p.ID);

            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ConvertModelListToDTOTest()
        {
            HashtagRepository repo = new HashtagRepository();

            Hashtag p = new Hashtag();
            p.Label = "Test";
            List<Hashtag> list = new List<Hashtag>();
            list.Add(p);
            list.Add(p);
            List<HashtagDTO> dto = repo.ConvertModelListToDTO(list);
            Assert.AreEqual(list.Count,dto.Count);

        }

        [TestMethod()]
        public void ConvertModelToDTOTest()
        {
            HashtagRepository repo = new HashtagRepository();

            Hashtag p = new Hashtag();
            p.Label = "Test";
           
            HashtagDTO dto = repo.ConvertModelToDTO(p);
            Assert.AreEqual(dto.Label,p.Label);

        }

        [TestMethod()]
        public void ConvertDTOToModelTest()
        {
            HashtagRepository repo = new HashtagRepository();

            HashtagDTO dto  = new HashtagDTO();
            dto.Label = "Test";

            Hashtag p = repo.ConvertDTOToModel(dto);
            Assert.AreEqual(dto.Label, p.Label);
        }
    }
}