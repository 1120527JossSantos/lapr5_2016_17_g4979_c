﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFetchAPI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoFetchAPI.Models;
using GoFetchAPI.DTO;

namespace GoFetchAPI.DAL.Tests
{
    [TestClass()]
    public class ItineraryRepositoryTests
    {
        [TestMethod()]
        public void ItineraryRepositoryTest()
        {
            ItineraryRepository repo = new ItineraryRepository();
            Assert.IsNotNull(repo.context);
        }

        [TestMethod()]
        public void CreateTest()
        {
            ItineraryRepository repo = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017,12,12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary cat = repo.Create(itinerary);
            repo.Delete(cat.ID);
            Assert.IsNotNull(cat.Type);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            ItineraryRepository repo = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary cat = repo.Create(itinerary);
           bool b= repo.Delete(cat.ID);
            Assert.IsTrue(b);
        }

        [TestMethod()]
        public void GetDataTest()
        {
            ItineraryRepository repo = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary cat = repo.Create(itinerary);
            Itinerary fromRepo = repo.GetData(cat.ID);
            repo.Delete(cat.ID);
            Assert.IsNotNull(fromRepo.Type);
        }

        [TestMethod()]
        public void ByUserTest()
        {
            ItineraryRepository repo = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary cat = repo.Create(itinerary);
            List<Itinerary> fromRepo = repo.ByUser("unittest1");
            List<Itinerary> fromRepoAll = repo.GetData();
            repo.Delete(cat.ID);
            Assert.AreNotEqual(fromRepo.ToList().Count(),fromRepoAll.ToList().Count());
        }

        [TestMethod()]
        public void GetDataTest1()
        {
            ItineraryRepository repo = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary cat = repo.Create(itinerary);
            List<Itinerary> fromRepoAll = repo.GetData();
            repo.Delete(cat.ID);
            Assert.AreNotEqual(0, fromRepoAll.ToList().Count());

        }

        [TestMethod()]
        public void UpdateTest()
        {
            ItineraryRepository repo = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            Itinerary cat = repo.Create(itinerary);
            Itinerary newitinerary = new Itinerary();
            newitinerary.StartTime = new DateTime(2017, 12, 11);
            newitinerary.Type = "Sports";
            newitinerary.ID = cat.ID;
            newitinerary.UserID = null;
            newitinerary.User = null;
            repo.Update(cat.ID,newitinerary);

            Itinerary fromRepo = repo.GetData(cat.ID);
            repo.Delete(cat.ID);
            Assert.AreEqual("Sports",fromRepo.Type);
        }

        [TestMethod()]
        public void ConvertModelListToDTOTest()
        {
            ItineraryRepository repo = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;
            List<Itinerary> list = new List<Itinerary>();
            list.Add(itinerary);
            list.Add(itinerary);
            List<ItineraryDTO> listdto = repo.ConvertModelListToDTO(list);

            Assert.AreEqual(list.Count, listdto.Count);
        }

        [TestMethod()]
        public void ConvertModelToDTOTest()
        {
            ItineraryRepository repo = new ItineraryRepository();

            Itinerary itinerary = new Itinerary();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.UserID = null;
            itinerary.User = null;

            ItineraryDTO cat = repo.ConvertModelToDTO(itinerary);

            Assert.AreEqual(cat.Type,itinerary.Type);
        }

        [TestMethod()]
        public void ConvertDTOToModelTest()
        {
            ItineraryRepository repo = new ItineraryRepository();

            ItineraryDTO itinerary = new ItineraryDTO();
            itinerary.StartTime = new DateTime(2017, 12, 12);
            itinerary.Type = "Historyc";
            itinerary.User = null;

            Itinerary cat = repo.ConvertDTOToModel(itinerary);

            Assert.AreEqual(cat.Type, itinerary.Type);
        }
    }
}